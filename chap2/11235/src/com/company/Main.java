package com.company;

        import java.util.ArrayList;
        import java.util.Scanner;

public class Main {

    static class SegTree {
        static class TreeNode {
            int freq;
            int leftFreq, leftNum, rightFreq, rightNum;
        }
        ArrayList<TreeNode> tree;
        int treeSize, arrSize;

        SegTree(int[] arr, int sizeOfArr) {
            arrSize = sizeOfArr;
            int treeHeight = (int) ( Math.floor(Math.log(sizeOfArr) / Math.log(2.0)) + 1);
            treeSize = (int) Math.pow(2, treeHeight + 1);

            tree = new ArrayList<>(treeSize);
            for (int i = 0 ; i < treeSize; ++i) tree.add(new TreeNode());

            buildTree(arr, 1, 0,  sizeOfArr - 1);
        }

        void updateLeafNode (int id, int newValue) {
            TreeNode node = tree.get(id);
            node.freq = 1;
            node.leftFreq = 1;
            node.rightFreq = 1;
            node.leftNum = newValue;
            node.rightNum = newValue;
        }

        TreeNode operation (TreeNode left, TreeNode right) {
            TreeNode combinationNode = new TreeNode();
            int joinFreq = left.rightNum == right.leftNum ? left.rightFreq + right.leftFreq : 0;
            combinationNode.rightNum = right.rightNum;
            combinationNode.leftNum = left.leftNum;
            combinationNode.leftFreq = left.leftNum == right.leftNum ? left.leftFreq + right.leftFreq: left.leftFreq;
            combinationNode.rightFreq = right.rightNum == left.rightNum ?
                    right.rightFreq + left.rightFreq : right.rightFreq;
            combinationNode.freq = Math.max(Math.max(joinFreq, left.freq), right.freq);
            return combinationNode;
        }

        void buildTree (int[] arr, int id, int st, int ed) {
            if (st > ed) return;

            if (st == ed) {
                updateLeafNode(id, arr[st]);
                return;
            }

            int mid = (st + ed) >> 1, twiceId = id << 1;
            buildTree(arr, twiceId, st, mid);
            buildTree(arr, twiceId + 1, mid + 1, ed);
            tree.set(id, operation(tree.get(twiceId), tree.get(twiceId + 1)));
        }

        void update (int id, int st, int ed, int index, int newVal) {
            if (st > ed || index < st || index > ed) return;

            if (st == ed) {
                updateLeafNode(id, newVal);
                return;
            }

            int mid = (st + ed) >> 1, twiceId = id << 1;
            update(twiceId, st, mid, index, newVal);
            update(twiceId + 1, mid + 1, ed, index, newVal);
            tree.set(id, operation(tree.get(twiceId), tree.get(twiceId + 1)));
        }

        TreeNode query (int id, int nodeStart, int nodeEnd, int toFindStart, int toFindEnd) {
            if (nodeStart > nodeEnd || toFindStart > toFindEnd || toFindStart > nodeEnd || toFindEnd < nodeStart) {
                return null;
            }
            if (nodeStart >= toFindStart && nodeEnd <= toFindEnd) {
                return tree.get(id);
            }

            int mid = (nodeStart + nodeEnd) >> 1, twiceId = id << 1;
            TreeNode left = query(twiceId, nodeStart, mid, toFindStart, toFindEnd);
            TreeNode right = query(twiceId + 1, mid + 1, nodeEnd, toFindStart, toFindEnd);
            if (left == null) return right;
            if (right == null) return left;
            return operation(left, right);
        }

        int doQuery (int st, int ed) {
            return query(1, 0, arrSize - 1, st, ed).freq;
        }

        void doUpdate(int index, int newVal) {
            update(1, 0, arrSize - 1, index, newVal);
        }
    }

    public static void main(String[] args) {
        // write your code here
        int[] arr = new int[100010];
        Scanner sc = new Scanner(System.in);
        while (true) {
            String[] inp = sc.nextLine().split(" ");
            int n = Integer.parseInt(inp[0]);
            if (n == 0) break;
            int q = Integer.parseInt(inp[1]);

            for (int i = 0 ; i < n; ++i) arr[i] = sc.nextInt();
            SegTree tree = new SegTree(arr, n);

            for (int i = 0 ; i < q; ++i) {
                int st = sc.nextInt(), ed = sc.nextInt();
                System.out.println(tree.doQuery(st - 1, ed - 1));
            }
            sc.nextLine();
        }
    }
}
