package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = sc.nextInt();
        while (tc-- > 0) {
            int l, m;
            l = sc.nextInt();
            m = sc.nextInt();
            sc.nextLine();
            Queue<Integer> left = new LinkedList<>(), right = new LinkedList<>();

            for (int i = 0 ; i < m; ++i) {
                String [] inp = sc.nextLine().split(" ");
                if (inp[1].equals("left")) {
                    left.add(Integer.parseInt(inp[0]));
                } else right.add(Integer.parseInt(inp[0]));
            }

            char shore = 'L';
            int carsLeft = m;
            int crossed = 0;

            HashMap<Character, Queue<Integer>> queues = new HashMap<>();
            queues.put('L', left);
            queues.put('R', right);
            while (carsLeft > 0) {
                Queue<Integer> q = queues.get(shore);
                int carsTravelling = 0;
                if (!q.isEmpty()) {
                    int spaceLeft = l * 100;
                    while (!q.isEmpty() && spaceLeft - q.peek() >= 0) {
                        int carLen = q.poll();
                        spaceLeft -= carLen;
                        carsTravelling++;
                    }
                }
                shore = shore == 'L' ? 'R' : 'L';
                crossed ++;
                carsLeft -= carsTravelling;
            }
            System.out.println(crossed);
        }
    }
}
