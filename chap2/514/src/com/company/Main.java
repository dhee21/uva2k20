package com.company;

        import java.util.Scanner;
        import java.util.Stack;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Scanner sc = new Scanner(System.in);
        boolean firstCase = true;
        while (true) {
            int numOfBlocks = Integer.parseInt(sc.nextLine());
            if (numOfBlocks == 0) break;



            while (true) {
                String inp = sc.nextLine();
                if (inp.charAt(0) == '0') break;

                String[] required = inp.split(" ");
                boolean isPossible = true;
                Stack<Integer> station = new Stack<>();
                int toEnter = 1;
                for (int i = 0; i < required.length; ++i) {
                    int wanted = Integer.parseInt(required[i]);
                    boolean isInStation = wanted < toEnter;
                    if (isInStation) {
                        boolean isWantedOnTop = station.peek() == wanted;
                        if (!isWantedOnTop) {
                            isPossible = false; break;
                        } else {
                            station.pop();
                        }
                    } else {
                        while (toEnter <= wanted) station.add(toEnter++);
                        station.pop();
                    }
                }
                if(isPossible) System.out.println("Yes");
                else System.out.println("No");
            }
            System.out.println();

        }
    }
}
