package com.company;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        ArrayList<ArrayList<Integer>> adjList = new ArrayList<>();
        for (int i = 0 ; i < 26; ++i) adjList.add(new ArrayList<>());

        while (sc.hasNextLine()) {
            int numOfNodes = Integer.parseInt(sc.nextLine());
            int connections = Integer.parseInt(sc.nextLine());

            boolean [] waken = new boolean[26];
            int [] connectionWithAwakened = new int[26];
            int [] yearOfAwake = new int[26];
            for (int i = 0; i < 26; ++i) adjList.set(i, new ArrayList<>());
            String awakened = sc.nextLine();
            for (int i = 0 ; i < connections; ++i) {
                String inp = sc.nextLine();
                char v1 = inp.charAt(0), v2 = inp.charAt(1);
                adjList.get(v1 - 'A').add(v2 - 'A');
                adjList.get(v2 - 'A').add(v1 - 'A');
            }
            waken[awakened.charAt(0) - 'A'] = true;
            waken[awakened.charAt(1) - 'A'] = true;
            waken[awakened.charAt(2) - 'A'] = true;

            Queue<Integer> q = new LinkedList<>();
            q.add(awakened.charAt(0) - 'A');
            q.add(awakened.charAt(1) - 'A');
            q.add(awakened.charAt(2) - 'A');
            numOfNodes -= 3;
            int years = 0;
            while (!q.isEmpty() && numOfNodes > 0) {
                    int node = q.poll();
                    ArrayList<Integer> neightbours = adjList.get(node);
                    for (int i = 0 ; i < neightbours.size(); ++i) {
                        int neighbourNode = neightbours.get(i);
                        if (!waken[neighbourNode]) {
                            connectionWithAwakened[neighbourNode] ++;
                            if (connectionWithAwakened[neighbourNode] == 3) {
                                waken[neighbourNode] = true;
                                numOfNodes--;
                                yearOfAwake[neighbourNode] = yearOfAwake[node] + 1;
                                q.add(neighbourNode);
                                years = Math.max(years, yearOfAwake[neighbourNode]);
                            }
                        }
                    }
            }

            if (numOfNodes == 0) {
                System.out.println("WAKE UP IN, " + years + ", YEARS");
            } else {
                System.out.println("THIS BRAIN NEVER WAKES UP");
            }

            if (sc.hasNextLine()) sc.nextLine();
        }
    }
}
