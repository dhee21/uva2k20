package com.company;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class Main {

    static  int unloadTime = 1, loadTime = 1, time, timeToMove = 2;
    static int unload (Stack<Integer> stack, Queue<Integer> q, int qSize, int stationNum) {
        int totalReached = 0;
        while (!stack.isEmpty()) {
            int cargoId = stack.peek();
            if (cargoId == stationNum) {
                totalReached++;
                stack.pop();
                time += unloadTime;
            } else if (q.size() < qSize) {
                q.add(stack.pop());
                time += unloadTime;
            } else {
                break;
            }
        }
        return totalReached;
    }

    static void load (Stack<Integer> stack, Queue<Integer> q, int stackSize) {
        while (stack.size() < stackSize && !q.isEmpty()) {
            stack.add(q.poll());
            time += loadTime;
        }
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);

        int tc = sc.nextInt();

        while (tc-- > 0) {
            int n = sc.nextInt(), s = sc.nextInt(), q = sc.nextInt();
            Queue<Integer>[] platB = new Queue[n + 5];
            for (int i = 0 ; i < n + 5; ++i) platB[i] = new LinkedList<Integer>();

            int totalCargoes = 0;
            for (int i = 0 ; i < n; ++i) {
                int numOfCargoes = sc.nextInt();
                while (numOfCargoes-- > 0) {
                    platB[i].add(sc.nextInt() - 1); totalCargoes++;
                }
            }

            time = 0;
            Stack<Integer> bus = new Stack<>();
            int station = 0;
            while (totalCargoes > 0) {
                int cargoesReached = unload(bus, platB[station], q, station);
                totalCargoes -= cargoesReached;
                if (totalCargoes == 0) break;
                load(bus, platB[station], s);
                station = (station + 1) % n;
                time += timeToMove;
            }
            System.out.println(time);
        }
    }
}
