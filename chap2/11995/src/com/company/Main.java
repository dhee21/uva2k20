package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int lines = sc.nextInt();
            Stack<Integer> stack = new Stack<>();
            Queue<Integer> q = new LinkedList<>();
            PriorityQueue<Integer> pq = new PriorityQueue<>();
            int  isStack = 1, isQ = 1, isPQ = 1;
            while (lines -- > 0) {
                int type = sc.nextInt(), value = sc.nextInt();
                if (type == 1) {
                    stack.add(value);
                    q.add(value);
                    pq.add(-value);
                } else {
                    if (stack.isEmpty()) {
                        isStack = 0; isPQ = 0; isQ = 0;
                    }
                    if (!stack.isEmpty() && stack.pop() != value) isStack = 0;
                    if (!q.isEmpty() && q.poll() != value) isQ = 0;
                    if (!pq.isEmpty() && -pq.poll() != value) isPQ = 0;
                }
            }
            if (isStack + isPQ + isQ == 0) System.out.println("impossible");
            else if (isStack + isPQ + isQ > 1) System.out.println("not sure");
            else {
                if (isStack != 0) System.out.println("stack");
                else if (isQ != 0) System.out.println("queue");
                else System.out.println("priority queue");
            }
        }
    }
}
