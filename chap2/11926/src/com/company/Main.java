package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        boolean arr[] = new boolean[1000010];
        Scanner sc = new Scanner(System.in);
        while (true) {
            int n = sc.nextInt(), m = sc.nextInt();
            if (n == 0 && m == 0) break;

            for (int i = 0 ; i < 1000001; ++i) arr[i] = false;

            boolean conflict = false;
            for (int i = 0 ; i < n ; ++i) {
                int st = sc.nextInt(), ed = sc.nextInt();
                if (!conflict) for (int j = st; j < ed; ++j) {
                    if (arr[j]) {
                        conflict = true; break;
                    } else arr[j] = true;
                }
            }

            for (int i = 0 ; i < m ; ++i) {
                int st = sc.nextInt(), ed = sc.nextInt(), interval = sc.nextInt();
                if (!conflict) {
                    boolean first = true;
                    while (!conflict && st < 1000001) {
                        if (!first) {
                            st += interval; ed += interval;
                        }
                        first = false;
                        for (int j = st; j < ed && j < 1000001; ++j) {
                            if (arr[j]) {
                                conflict = true; break;
                            } else arr[j] = true;
                        }
                    }
                }
            }

            if (conflict) System.out.println("CONFLICT");
            else System.out.println("NO CONFLICT");

        }
    }
}
