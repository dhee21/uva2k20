package com.company;

import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int p, g;
        Scanner sc = new Scanner(System.in);
        String[] inp = sc.nextLine().split(" ");
        p = Integer.parseInt(inp[0]);
        g = Integer.parseInt(inp[1]);

        HashMap<String , Integer> map = new HashMap<>();
        while (p-- > 0) {
            String[] input = sc.nextLine().split(" ");
            String party = input[0];
            double percentage = Double.parseDouble(input[1]);
            map.put(party, (int)(percentage * 10));
        }
        for (int index = 0 ; index < g; ++index) {
            String[] toCheck = sc.nextLine().split(" ");
            int total = 0;
            for (int i = 0 ; i < toCheck.length - 2 ; i += 2) {
                String partyVal = toCheck[i];
                total += map.get(partyVal);
            }

            boolean isRight;
            int guessVal = Integer.parseInt(toCheck[toCheck.length - 1]);
            guessVal *= 10;
            String operator = toCheck[toCheck.length - 2];
            switch (operator) {
                case "=" :
                    isRight =  total == guessVal;
                    break;
                case ">=":
                    isRight = total >= guessVal ; break;
                case "<=":
                    isRight = total <= guessVal; break;
                case ">":
                    isRight = total > guessVal; break;
                default:
                    isRight = total < guessVal; break;
            }
            System.out.println("Guess #" + (index + 1) + " was " + (isRight ? "correct." : "incorrect."));
         }
    }
}
