package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {


    static class UnionFind {

        private ArrayList<Integer> pset, nodes;
        public int numOfSets;

        UnionFind (int n) {
            pset = new ArrayList<>(n);
            nodes = new ArrayList<>(n);
            for (int i = 0 ; i < n; ++i) {
                pset.add(i);
                nodes.add(1);
            }
            numOfSets = n;
        }

        void unionSet (int vertex1, int vertex2) {
            int set1 = findSet(vertex1), set2 = findSet(vertex2);
            if (set1 == -1 || set2 == -1 || set1 == set2) return;
            if (nodes.get(set1) > nodes.get(set2)) {
                pset.set(set2, set1);
                nodes.set(set1, nodes.get(set1) + nodes.get(set2));
            } else {
                pset.set(set1, set2);
                nodes.set(set2, nodes.get(set1) + nodes.get(set2));
            }
            numOfSets--;
        }

        void compressThePath (int vertex, int set) {
            while (vertex != set) {
                int parent = pset.get(vertex);
                pset.set(vertex, set);
                vertex = parent;
            }
        }

        int findSet (int vertex) {
            if (vertex < 0 || vertex >= pset.size()) return -1;

            int ptr = vertex;
            while (pset.get(ptr) != ptr) ptr = pset.get(ptr);
            compressThePath(vertex, ptr);
            return ptr;
        }

        boolean isSameSet (int vertex1, int vertex2) {
            int set1 = findSet(vertex1), set2 = findSet(vertex2);
            if (set1 == -1 || set2 == -1) return false;
            return set1 == set2;
        }

        int numDisjointSets() {
            return  numOfSets;
        }

        int sizeOfSet (int vertex) {
            int set = findSet(vertex);
            if (set == -1) return -1;
            return nodes.get(set);
        }

    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = Integer.parseInt(sc.nextLine());
        while (tc -- > 0) {
            int nodeNum = 0;
            HashMap<String, Integer> nameToNode = new HashMap<>();
            int num = Integer.parseInt(sc.nextLine());
            ArrayList<String> inputsToStore = new ArrayList<>();
            while (num -- > 0) {
                String inputString = sc.nextLine();
                inputsToStore.add(inputString);
                String[] inp = inputString.split(" ");
                if (!nameToNode.containsKey(inp[0])) {
                    nameToNode.put(inp[0], nodeNum++);
                }
                if (!nameToNode.containsKey(inp[1])) {
                    nameToNode.put(inp[1], nodeNum++);
                }
            }
            UnionFind sets = new UnionFind(nodeNum);
            for (int i = 0 ;  i < inputsToStore.size(); ++i) {
                String inputVal = inputsToStore.get(i);
                String[] friends = inputVal.split(" ");
                int node1 = nameToNode.get(friends[0]);
                int node2 = nameToNode.get(friends[1]);
                sets.unionSet(node1, node2);
                System.out.println(sets.sizeOfSet(node1));
            }
        }

    }
}
