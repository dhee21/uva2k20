package com.company;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    static class UnionFind {

        private ArrayList<Integer> pset, nodes;
        public int numOfSets;

        UnionFind (int n) {
            pset = new ArrayList<>(n);
            nodes = new ArrayList<>(n);
            for (int i = 0 ; i < n; ++i) {
                pset.add(i);
                nodes.add(1);
            }
            numOfSets = n;
        }

        void unionSet (int vertex1, int vertex2) {
            int set1 = findSet(vertex1), set2 = findSet(vertex2);
            if (set1 == -1 || set2 == -1 || set1 == set2) return;
            if (nodes.get(set1) > nodes.get(set2)) {
                pset.set(set2, set1);
                nodes.set(set1, nodes.get(set1) + nodes.get(set2));
            } else {
                pset.set(set1, set2);
                nodes.set(set2, nodes.get(set1) + nodes.get(set2));
            }
            numOfSets--;
        }

        void compressThePath (int vertex, int set) {
            while (vertex != set) {
                int parent = pset.get(vertex);
                pset.set(vertex, set);
                vertex = parent;
            }
        }

        int findSet (int vertex) {
            if (vertex < 0 || vertex >= pset.size()) return -1;

            int ptr = vertex;
            while (pset.get(ptr) != ptr) ptr = pset.get(ptr);
            compressThePath(vertex, ptr);
            return ptr;
        }

        boolean isSameSet (int vertex1, int vertex2) {
            int set1 = findSet(vertex1), set2 = findSet(vertex2);
            if (set1 == -1 || set2 == -1) return false;
            return set1 == set2;
        }

        int numDisjointSets() {
            return  numOfSets;
        }

        int sizeOfSet (int vertex) {
            int set = findSet(vertex);
            if (set == -1) return -1;
            return nodes.get(set);
        }

    }


    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = Integer.parseInt(sc.nextLine());
        sc.nextLine();

        boolean first = true;
        while (tc -- > 0) {
            if (!first) System.out.println();
            first = false;
            int computers = Integer.parseInt(sc.nextLine());
//            System.out.println(computers);
            UnionFind sets = new UnionFind(computers);
            int wrong = 0, right = 0;
            while (sc.hasNextLine()) {
                String inp = sc.nextLine();
                if (inp.length() == 0) break;
                String[] input = inp.split(" ");
                char type = input[0].charAt(0);
                int v1 = Integer.parseInt(input[1]), v2 = Integer.parseInt(input[2]);
                if (type == 'c') {
                    sets.unionSet(v1 - 1, v2 - 1);
                } else {
                    boolean isSameSet = sets.isSameSet(v1 - 1, v2 - 1);
                    if (isSameSet) right ++;
                    else wrong++;
                }
            }
            System.out.println(right + "," + wrong);
        }
    }
}
