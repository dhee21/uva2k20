package com.company;

import java.util.Scanner;

public class Main {

    static long invCount = 0;
    static void mergeSort (long[] arr, int st, int ed) {
//        System.out.println(st + " " + ed);
        if (st >= ed) return;
        int mid = (st + ed) >> 1;
        mergeSort(arr, st, mid);
        mergeSort(arr, mid + 1, ed);

        int left = st, right = mid + 1;
        long[] mergeArr = new long[ed - st + 1];

        int indexToPut = 0;
        while (left <=  mid && right <= ed) {
            if (arr[left] <= arr[right]) {
                mergeArr[indexToPut++] = arr[left];
                left++;
            } else {
                mergeArr[indexToPut++] = arr[right];
                invCount += mid - left + 1;
                right++;
            }
        }
        while (left <= mid) mergeArr[indexToPut++] = arr[left++];
        while (right <= ed) mergeArr[indexToPut++] = arr[right++];

        for(int i = st ; i <= ed; i++) {
            arr[i] = mergeArr[i - st];
        }
    }
    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);

        while (sc.hasNextInt()) {
            int numToFollow = sc.nextInt();
            long[] arr = new long[numToFollow];
            for (int i = 0 ; i < numToFollow; ++i) {
                arr[i] = sc.nextLong();
            }

            invCount = 0;
            mergeSort(arr, 0, numToFollow - 1);
//            for(int i = 0 ; i < numToFollow; ++i) {
//                System.out.println(arr[i]);
//            }
            System.out.println(invCount);
        }
    }
}
