package com.company;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Main {

    static class Car {
        int order, time;
        Car(int o, int t) {
            order = o;
            time = t;
        }
    }

    static char getOpposite (char c) {
        if (c == 'L') return 'R';
        return 'L';
    }

    static char getShore(char shore ,HashMap<Character, Queue<Car>> queues, int timeVal) {
        if (queues.get('L').isEmpty()) return 'R';
        if (queues.get('R').isEmpty()) return 'L';

        if (queues.get(shore).peek().time <= timeVal) return  shore;

        Car leftCar = queues.get('L').peek();
        Car rightCar = queues.get('R').peek();
        if (leftCar.time < rightCar.time) return 'L';
        if (rightCar.time < leftCar.time) return 'R';
        return shore;
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);

        int tc = sc.nextInt();
        boolean firstCase = true;
        while (tc-- > 0) {
            if (!firstCase) System.out.println();
            firstCase = false;

            int n = sc.nextInt(), t = sc.nextInt(), m = sc.nextInt(); sc.nextLine();
            //System.out.println(n + " " + t + " " + m);
            Queue<Car> left = new LinkedList<>(), right = new LinkedList<>();
            int[] ans = new int[m];

            for (int i = 0 ; i < m ; ++i) {
                String[] input = sc.nextLine().split(" ");
                if (input[1].equals("left")) {
                    left.add(new Car(i, Integer.parseInt(input[0])));
                } else right.add(new Car(i, Integer.parseInt(input[0])));
            }

            int time = 0;
            int cars = m;
            char shore = 'L';
            HashMap<Character, Queue<Car>> queues = new HashMap<>();
            queues.put('L', left); queues.put('R', right);

            while (cars > 0) {
                char shoreForNextCar = getShore(shore, queues, time);

                int timeOfNextCar = queues.get(shoreForNextCar).peek().time;
                if (timeOfNextCar > time) time = timeOfNextCar;

                if (shoreForNextCar != shore) {
                    time += t;
                    shore = getOpposite(shore);
                }

                Queue<Car> carsLine =  queues.get(shore);
                Car car = carsLine.peek();
                time = Math.max(time, car.time);

                Queue<Car> ferry = new LinkedList<>();
                for (int i = 0 ; i < n; ++i) {
                    if (carsLine.isEmpty()) break;
                    if (carsLine.peek().time > time) break;
                    ferry.add(carsLine.poll());
                }

                time += t;
                shore = getOpposite(shore);

                while (!ferry.isEmpty()) {
                    Car unloadedCar = ferry.poll();
                    cars--;
                    ans[unloadedCar.order] = time;
                }
            }

            for (int i = 0 ; i < m; ++i) {
                System.out.println(ans[i]);
            }

        }
    }
}
