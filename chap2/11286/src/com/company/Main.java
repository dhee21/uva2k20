package com.company;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (true) {
            int val = Integer.parseInt(sc.nextLine());
            if (val == 0) break;

            HashMap<String, Integer> map = new HashMap<>();
            while (val -- > 0) {
                String[] inp = sc.nextLine().split(" ");
                int[] valArr = new int[inp.length];
                for (int i = 0 ; i < inp.length; ++i) {
                    valArr[i] = Integer.parseInt(inp[i]);
                }
                Arrays.sort(valArr, 0, inp.length);
                StringBuffer s = new StringBuffer();
                for (int i = 0 ; i < inp.length; ++i) {
                    s.append(valArr[i]);
                }
                int freq = map.getOrDefault(s.toString(), 0);
                map.put(s.toString(), ++freq);
            }
            int maxVal = 0;
            for (String s : map.keySet()) {
                if (map.get(s) > maxVal) maxVal = map.get(s);
            }
            int ans = 0;
            for (String s : map.keySet()) {
                if (map.get(s) == maxVal) ans += maxVal;
            }
            System.out.println(ans);
        }
    }
}
