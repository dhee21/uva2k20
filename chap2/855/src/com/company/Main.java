package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);

        int tc = sc.nextInt();

        while (tc-- > 0) {
            int streets = sc.nextInt(), avenues = sc.nextInt(), friends = sc.nextInt();
            int[] st = new int[friends];
            int[] av = new int[friends];

            for (int i = 0 ; i < friends; ++i) {
                st[i] = sc.nextInt();
                av[i] = sc.nextInt();
            }

            Arrays.sort(st, 0, friends);
            Arrays.sort(av, 0, friends);
            int index = (friends % 2 == 0) ? (friends / 2) - 1 : friends / 2;
            System.out.println("(Street: "+ st[index] + ", Avenue: " + av[index] + ")");
        }
    }
}
