package com.company;

import java.util.Scanner;
import java.util.TreeMap;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = Integer.parseInt(sc.nextLine());
        sc.nextLine();

        boolean isFirst = true;
        while (tc-- > 0) {
            if (!isFirst) System.out.println();
            isFirst = false;

            TreeMap<String, Integer> map = new TreeMap<>();
            int total = 0;
            while (true) {
                if (!sc.hasNextLine()) break;
                String inp = sc.nextLine();
                if (inp.length() == 0) break;
                int val = map.getOrDefault(inp, 0);
                val++;
                total++;
                map.put(inp, val);
            }
            for (String s : map.keySet()) {
                System.out.printf("%s %.4f\n", s , (double)map.get(s) * 100 / (double)total);
            }
        }
    }
}
