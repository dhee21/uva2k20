package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static class Question {
        boolean isSolved;
        int incorrectSub;
    }

    public static class Person {
        int id;
        int penalty;
        int probSolved;
        Question[] questions = new Question[12];
        Person (int pid) {
            id = pid;
            for (int i = 0 ; i < 12; ++i) questions[i] = new Question();
        }
    }

    public static int comparePerson (Person left, Person right) {
        if (left.probSolved > right.probSolved) return -1;
        if (right.probSolved > left.probSolved) return 1;

        if (left.penalty > right.penalty) return 1;
        if (right.penalty > left.penalty) return -1;

        return left.id - right.id;
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = Integer.parseInt(sc.nextLine());
        sc.nextLine();

        boolean firstCase = true;
        while (tc -- > 0) {
            HashMap<Integer, Person> pidToPErson = new HashMap<>();

            if (!firstCase) System.out.println();

            firstCase = false;

            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                if (line.length() == 0) break;

                String[] input = line.split(" ");
                int contestantId = Integer.parseInt(input[0]);
                int problemId = Integer.parseInt(input[1]);
                int time = Integer.parseInt(input[2]);
                char L = input[3].charAt(0);

                Person p = pidToPErson.containsKey(contestantId) ? pidToPErson.get(contestantId) : new Person(contestantId);

                if (!p.questions[problemId].isSolved && (L == 'C' || L == 'I')) {

                    if (L == 'C') {
                        p.questions[problemId].isSolved = true;
                        p.penalty += time + (20 * p.questions[problemId].incorrectSub);
                        p.probSolved++;
                    } else {
                        p.questions[problemId].incorrectSub++;
                    }
                }
                pidToPErson.put(contestantId, p);
            }

            ArrayList<Person> arr = new ArrayList<>();
            for (int pid: pidToPErson.keySet()) {
                arr.add(pidToPErson.get(pid));
            }

            arr.sort(Main::comparePerson);

            for (Person p: arr) {
                System.out.println(p.id + " " + p.probSolved + " " + p.penalty);
            }
        }
    }
}
