package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Stack;

public class Main {

    static HashMap<String, Integer> operatorToPrio = new HashMap<>();
    static {
        operatorToPrio.put("+", 0);
        operatorToPrio.put("-", 0);
        operatorToPrio.put("*", 1);
        operatorToPrio.put("/", 1);
    }

    static boolean isOperator (String val) {
        return operatorToPrio.containsKey(val);
    }

    static boolean isPriorityGreater(String of, String from) {
        if (from.equals("(")) return true;
        return operatorToPrio.get(of) > operatorToPrio.get(from);
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = Integer.parseInt(sc.nextLine());
        sc.nextLine();

        boolean firstCase = true;
        while (tc-- > 0) {
            if (!firstCase) System.out.println();
            firstCase = false;
            ArrayList<String> ans = new ArrayList<>();
            ArrayList<String> input = new ArrayList<>();
            Stack<String> operators = new Stack<>();
            while (sc.hasNextLine()) {
                String charStr = sc.nextLine();
                if (charStr.length() == 0) break;
                input.add(charStr);
            }

            for (int i = 0 ; i < input.size(); ++i) {
                String value = input.get(i);
//                System.out.println(value);
                if (isOperator(value)) {
                    if (operators.isEmpty() || isPriorityGreater(value, operators.peek())) operators.add(value);
                    else {
                        while (!operators.isEmpty() && !isPriorityGreater(value, operators.peek())) ans.add(operators.pop());
                        operators.add(value);
                    }
                } else if (value.equals("(")) {
                    operators.add(value);
                } else if (value.equals(")")) {
                    while (!operators.peek().equals("(")) ans.add(operators.pop());
                    operators.pop();
                } else {
                    ans.add(value);
                }
            }
            while (!operators.isEmpty()) ans.add(operators.pop());

            StringBuffer ansString = new StringBuffer();
            for (int i = 0 ; i < ans.size(); ++i) {
                ansString.append(ans.get(i));
            }
            System.out.println(ansString);
        }
    }
}
