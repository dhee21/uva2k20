package com.company;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeMap;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            int row = sc.nextInt(), col = sc.nextInt(); sc.nextLine();
            ArrayList<TreeMap<Integer, Integer>> sparseMatrix = new ArrayList<>(col);
            for (int i = 0; i < col; ++i) sparseMatrix.add(new TreeMap<>());

            for (int rowNum = 0; rowNum < row; ++rowNum) {
                int numValuesInRow = sc.nextInt();
                int[] indexes = new int[numValuesInRow], values = new int[numValuesInRow];
                for (int i = 0 ; i < numValuesInRow; ++i) indexes[i] = sc.nextInt() - 1;
                sc.nextLine();
                for (int i = 0 ; i < numValuesInRow; ++i) values[i] = sc.nextInt();
                sc.nextLine();

                for (int i = 0 ; i < numValuesInRow; ++i) {
                    int transposeRow = indexes[i];
                    int transposeColumn = rowNum;
                    int valueToPut = values[i];
                    TreeMap<Integer, Integer> map = sparseMatrix.get(transposeRow);
                    map.put(transposeColumn, valueToPut);
                    sparseMatrix.set(transposeRow, map);
                }
            }

            System.out.println(col + " " + row);
            for (int r = 0 ; r < col; ++r) {
                TreeMap<Integer, Integer> valuesMap = sparseMatrix.get(r);
                System.out.print(valuesMap.size());
                valuesMap.forEach((k, v) -> {
                    System.out.print(" " + (k + 1));
                });
                System.out.printf("\n");
                boolean first = true;
                for (Integer key: valuesMap.keySet()) {
                    if (first) {
                        System.out.print(valuesMap.get(key));
                    } else {
                        System.out.print(" " + valuesMap.get(key));
                    }
                    first = false;
                }
                System.out.printf("\n");
            }

        }
    }
}
