package com.company;

import java.util.PriorityQueue;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (true) {
            int num = sc.nextInt();
            if (num == 0) break;
            PriorityQueue<Long> pq = new PriorityQueue<>();
            for (int i = 0 ; i < num; ++i) {
                pq.add((long)sc.nextInt());
            }
            long cost = 0;
            while (!pq.isEmpty()) {
                long first = pq.poll();
                if (pq.isEmpty()) {
                    System.out.println(cost);
                } else {
                    long second = pq.poll();
                    cost += first + second;
                    pq.add(first + second);
                }
            }
        }
    }
}
