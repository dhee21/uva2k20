package com.company;

import java.util.Scanner;
import java.util.Stack;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (sc.hasNextLine()) {
            String[] input = sc.nextLine().split(" ");
            Stack<Long> stack = new Stack<>();

            boolean isPossible = true;
            for (int i = 0 ; i < input.length; ++i) {
                Long val = Long.parseLong(input[i]);
                if (val < 0) stack.add(val);
                else {
                    long sizeInside = 0;
                    while (!stack.isEmpty() && stack.peek() > 0) {
                        sizeInside += stack.pop();
                    }
                    if (stack.isEmpty() || sizeInside >= val || stack.peek() != -val) {
                        isPossible = false; break;
                    }
                    stack.pop();
                    stack.add(val);
                }
            }

            if (!isPossible) {
                System.out.println(":-( Try again."); continue;
            }
            while (!stack.isEmpty() && stack.peek() > 0) stack.pop();

            if (stack.isEmpty()) System.out.println(":-) Matrioshka!");
            else System.out.println(":-( Try again.");
        }
    }
}
