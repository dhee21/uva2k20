package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (true) {
           int num = sc.nextInt();
           if (num == 0) break;

           Integer[] degrees = new Integer[num];
           int total = 0;
           for (int i = 0; i < num; ++i) {
               degrees[i] = sc.nextInt();
               total += degrees[i];
           }
           if (total % 2 != 0) {
               System.out.println("Not possible");
               continue;
           }
           Arrays.sort(degrees, 0, degrees.length, (left, right) -> {
               return right - left;
           });

           boolean possible = true;
           int sumTillK = 0;
           for (int k = 0; k < num; ++k) {
               sumTillK += degrees[k];
               int sumAfterK = 0;
               for (int i = k + 1; i < num; ++i) sumAfterK += Math.min(degrees[i], k + 1);
               int rightSide = ((k + 1) * k) + sumAfterK;
//               System.out.println(sumTillK + " " + rightSide);
               boolean isOkay = sumTillK <= rightSide;
               if (!isOkay) {
                   possible = false; break;
               }
           }

           if (possible) System.out.println("Possible");
           else System.out.println("Not possible");
        }

    }
}
