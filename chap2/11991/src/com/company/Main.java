package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
        StringBuffer buff = new StringBuffer();
        while (true) {
            String line = sc.readLine();
            if (line == null) break;

            HashMap<Integer, ArrayList<Integer>> map = new HashMap<>();
            int n, m ;
            String [] inp = line.split(" ");
            n = Integer.parseInt(inp[0]);
            m = Integer.parseInt(inp[1]);

            String[] arr = sc.readLine().split(" ");
            for (int i = 0 ; i < n; ++i) {
                int num = Integer.parseInt(arr[i]);
                ArrayList<Integer> list = map.getOrDefault(num, new ArrayList<>());
                list.add(i + 1);
                map.put(num, list);
            }
            for (int i = 0 ; i < m; ++i) {
                String [] input = sc.readLine().split(" ");
                int occ = Integer.parseInt(input[0]), num = Integer.parseInt(input[1]);
                int index = occ - 1;

                boolean printed = false;
                if (map.containsKey(num)) {
                    ArrayList<Integer> list = map.get(num);
                    if (index < list.size()) {
                        buff.append(list.get(index) + "\n");
                        printed = true;
                    }
                }
                if (!printed) buff.append(0 + "\n");
            }
        }
        System.out.print(buff.toString());

    }
}
