package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here

        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            int[] count = new int[110];
            String inp = rd.readLine();
            if (inp.length() == 1 && inp.charAt(0) == '0') break;
            inp = rd.readLine();

            String[] arr = inp.split(" ");
            for (int i = 0; i < arr.length; ++i) {
                int intVal = Integer.parseInt(arr[i]);
//                System.out.println(intVal);
                count[intVal]++;
            }
            boolean isFirst = true;
            StringBuffer ans = new StringBuffer();

            for (int num = 1; num < 100; ++num) {
                if (count[num] > 0) {
                    for (int i = 0 ; i < count[num]; ++i) {
                        if (isFirst) {
                            ans.append(num);
                            isFirst = false;
                        } else {
                            ans.append(' ');
                            ans.append(num);
                        }
                    }
                }
            }

            System.out.println(ans);
        }
    }
}
