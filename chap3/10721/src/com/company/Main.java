package com.company;

import java.util.Scanner;

public class Main {

    static long[][][][] dp = new long[55][55][55][2];
    static int BLACK = 0, WHITE = 1;
    static void calculate() {
        for (int n = 0; n <= 50; ++n) for (int k = 0; k <= 50; ++k) for (int m = 0; m <= 50; ++m)
            for (int type = 0; type <= 1; ++type) {

                if (n == 0) {
                    if (k == 0) dp[n][k][m][type] = 1;
                    else dp[n][k][m][type] = 0;
                } else {
                    if (k == 0) dp[n][k][m][type] = 0;
                    else {
                        long symbols = 0;
                        for (int bars = 1; bars <= m && bars <= n; ++bars) {
                            symbols += dp[n - bars][k - 1][m][type == BLACK ? WHITE : BLACK];
                        }
                        dp[n][k][m][type] = symbols;
                    }
                }

            }
    }
    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int n, k, m;
        calculate();
        while (sc.hasNext()) {
            n = sc.nextInt();
            k = sc.nextInt();
            m = sc.nextInt();
            System.out.println(dp[n][k][m][BLACK]);
        }
    }
}
