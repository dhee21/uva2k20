package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    static int THRESHOLD = 40050;
    public static void main(String[] args) throws IOException {
	// write your code here
        boolean[][] canReach = new boolean[80200][45];
        boolean[][] realReach = new boolean[80200][45];
        char sign[] = new char[45];
        int [] numbers = new int[45];
        int n, f;
        int minSum = -40000, maxSum = 40000;
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String[] inp = rd.readLine().split(" ");
            n = Integer.parseInt(inp[0]);
            f = Integer.parseInt(inp[1]);
            if (n == 0 && f == 0) break;
            for (int i = 0 ; i < n; ++i) {
                numbers[i] = Integer.parseInt(rd.readLine());
            }
            for (boolean[] row: canReach) {
                Arrays.fill(row, false);
            }
            for (boolean[] row: realReach) {
                Arrays.fill(row, false);
            }

            canReach[f + THRESHOLD][0] = true;

            for (int i = 0; i < n; ++i) {
                for (int sum = minSum; sum <= maxSum; ++sum) {
                    if (canReach[sum + THRESHOLD][i]) {
                        if (sum + numbers[i] >= minSum && sum + numbers[i] <= maxSum) {
                            canReach[sum + numbers[i] + THRESHOLD][i + 1] = true;
                        }
                        if (sum - numbers[i] >= minSum && sum - numbers[i] <= maxSum) {
                            canReach[sum - numbers[i] + THRESHOLD][i + 1] = true;
                        }
                    }
                }
            }

            if (!canReach[0 + THRESHOLD][n]) {
                System.out.println("*");
                continue;
            }
            realReach[0 + THRESHOLD][n] = true;
            Arrays.fill(sign, '0');
            for (int i = n; i > 0; --i) {
                for (int sum = minSum; sum <= maxSum; ++sum) {
                    if (realReach[sum + THRESHOLD][i]) {
                        boolean plus = false;
                        if (sum + numbers[i - 1] >= minSum && sum + numbers[i - 1] <= maxSum) {
                           plus = canReach[sum + numbers[i - 1] + THRESHOLD][i - 1];
                           realReach[sum + numbers[i - 1] + THRESHOLD][i - 1] = true;
                        }
                        boolean minus = false;
                        if (sum - numbers[i - 1] >= minSum && sum - numbers[i - 1] <= maxSum) {
                             minus = canReach[sum - numbers[i - 1] + THRESHOLD][i - 1];
                             realReach[sum - numbers[i - 1] + THRESHOLD][i - 1] = true;
                        }

                        if (plus) {
                            sign[i - 1] = sign[i - 1] == '?' ? '?' : (sign[i - 1] == '-') ? '?' : '+';
                        }
                        if (minus) {
                            sign[i - 1] = sign[i - 1] == '?' ? '?' : (sign[i - 1] == '+') ? '?' : '-';
                        }
                    }
                }
            }

            for (int i = 0 ; i < n; ++i) {
                System.out.print(sign[i]);
            }
            System.out.println();

        }
    }
}
