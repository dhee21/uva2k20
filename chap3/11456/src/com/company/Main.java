package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    static void calLDSFromEnd (int[] LDS, int[] array, int len) {
        for (int i = len - 1; i >= 0; --i) {
            int lds = 1;
            for (int j = i + 1; j < len ; ++j) {
                if (array[j] > array[i]) lds = Math.max(lds, LDS[j] + 1);
            }
            LDS[i] = lds;
        }
    }
    static void calLISFromEnd (int[] LIS, int[] array, int len) {
        for (int i = len - 1; i >= 0; --i) {
            int lis = 1;
            for (int j = i + 1; j < len ; ++j) {
                if (array[j] < array[i]) lis = Math.max(lis, LIS[j] + 1);
            }
            LIS[i] = lis;
        }
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(reader.readLine());
        int [] array = new int[2100];
        StringBuilder ans = new StringBuilder();
        while (t-- > 0) {
            int cars = Integer.parseInt(reader.readLine());
            for (int i = 0; i < cars; ++i) {
                array[i] = Integer.parseInt(reader.readLine());
            }
            int[] LISFromEnd = new int[2100];
            int[] LDSFromEnd = new int[2100];
            calLDSFromEnd(LDSFromEnd, array, cars);
            calLISFromEnd(LISFromEnd, array, cars);
//            for (int i = 0 ; i < cars; ++i) {
//                System.out.print(LISFromEnd[i] + " ");
//            }
//            System.out.println();
//            for (int i = 0 ; i < cars; ++i) {
//                System.out.print(LDSFromEnd[i] + " ");
//            }
//            System.out.println();
            int max = 0;
            for (int i = 0 ; i < cars; ++i) {
                max = Math.max(LDSFromEnd[i] + LISFromEnd[i] - 1, max);
            }
            ans.append(max).append('\n');
        }
        System.out.print(ans);
    }
}
