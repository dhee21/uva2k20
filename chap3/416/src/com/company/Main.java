package com.company;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.String;
public class Main {

    static String numbers[] = {
            "YYYYYYN",
            "NYYNNNN",
            "YYNYYNY",
            "YYYYNNY",
            "NYYNNYY",
            "YNYYNYY",
            "YNYYYYY",
            "YYYNNNN",
            "YYYYYYY",
            "YYYYNYY"
    };

    static boolean canSequenceRepresentGivenNumber(boolean[] defected, String sequence, int numToRepresent) {
        String numberToRepresentString = numbers[numToRepresent];
        for (int i = 0; i < 7; ++i) if (sequence.charAt(i) == 'Y' && numberToRepresentString.charAt(i) == 'N') return false;
        for (int i = 0; i < 7; ++i) if (sequence.charAt(i) == 'N' && numberToRepresentString.charAt(i) == 'Y') defected[i] = true;
        return true;
    }

    static boolean canItBeCountDownStartingFrom(int startNum, int len, String[] sequences) {
        boolean defected[] = {false, false, false, false, false, false, false};
        int numToRepresent = startNum;
        for (int i = 0 ; i < len; ++i) {
            String sequenceToCheckWith = sequences[i];
            for (int bulb = 0; bulb < 7; ++bulb) {
                if (defected[bulb] && sequenceToCheckWith.charAt(bulb) == 'Y') return false;
            }
            boolean canRepresent = canSequenceRepresentGivenNumber(defected, sequenceToCheckWith, numToRepresent--);
            if (!canRepresent) return false;
        }
        return true;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        String[] sequences = new String[12];
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            int numOfSequences = Integer.parseInt(rd.readLine().trim());
            if (numOfSequences == 0) break;
            for (int i = 0 ; i < numOfSequences; ++i) {
                sequences[i] = rd.readLine().trim();
            }

            boolean isCountDown = false;
            for (int startNum = 9; startNum >= numOfSequences - 1; --startNum) {
                isCountDown = canItBeCountDownStartingFrom(startNum, numOfSequences, sequences);
                if (isCountDown) break;
            }
            if (isCountDown) System.out.println("MATCH");
            else System.out.println("MISMATCH");
        }
    }
}
