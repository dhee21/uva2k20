package com.company;

import java.util.Scanner;

public class Main {

    static char[][] grid = new char[11][11];
    static char[][] newGrid = new char[11][11];

    static void initNewGrid () {
        for (int i = 0; i < 10; ++i) {
            for (int j = 0 ; j < 10; ++j) newGrid[i][j] = grid[i][j];
        }
    }
    static boolean isOkay(int r, int c) {
        return r >= 0 && r < 10 && c >= 0 && c < 10;
    }
    static void toggle (int r, int c) {
        newGrid[r][c] = newGrid[r][c] == 'O' ? '#' : 'O';
    }
    static void toggleNewGrid(int row, int col) {
        if (isOkay(row, col)) toggle(row, col);
        if (isOkay(row - 1, col)) toggle(row - 1, col);
        if (isOkay(row + 1, col)) toggle(row + 1, col);
        if (isOkay(row, col - 1)) toggle(row, col - 1);
        if (isOkay(row, col + 1)) toggle(row, col + 1);
    }
    static boolean isLit(int row, int col) {
        return newGrid[row][col] == 'O';
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);


        while (true) {
            String inp = sc.nextLine();
            if (inp.equals("end")) {
                break;
            }
            String x;
            for (int i = 0 ; i < 10; ++i) {
                x = sc.nextLine();
                for (int j = 0 ; j < 10; ++j) grid[i][j] = x.charAt(j);
            }

            int minToggles = Integer.MAX_VALUE;
            for (int i = 0 ; i < (1 << 10); ++i) {
                int toggles = 0;
                initNewGrid();

                for (int col = 0; col < 10; ++col) {
                    if ((i & (1 << col)) > 0) {
                        toggleNewGrid(0, col);
                        toggles++;
                    }
                }
                for (int row = 1; row < 10; ++row) {
                    for (int col = 0; col < 10; ++col) {
                        if (isLit(row - 1, col)) {
                            toggleNewGrid(row, col); toggles++;
                        }
                    }
                }
//                for (int i = 0; i < 10; ++i) {
//                    for (int j = 0 ; j < 10; ++j) System.out.print(newGrid);
//                }
                boolean isOff = true;
                for (int col = 0 ; col < 10; ++col) {
                    if (isLit(9, col)) {
                        isOff = false; break;
                    }
                }
                if (isOff) minToggles = Math.min(minToggles, toggles);
            }
            System.out.println(inp + " " + (minToggles == Integer.MAX_VALUE ? -1 : minToggles));
        }
    }
}
