package com.company;

import java.util.Scanner;

public class Main {

    static int[] xCoor = new int[15],  yCoor = new int[15];
    static boolean isVisited (int node, int mask) {
        return (mask & (1 << node)) > 0;
    }

    static int distance(int node, int toReach) {
        int x =  xCoor[node] - xCoor[toReach];
        int y = yCoor[node] - yCoor[toReach];

        return Math.abs(x) + Math.abs(y);
    }

    static int maskAfterVisitNode(int mask, int node) {
        return mask | (1 << node);
    }

    static int TSP(int numberOfNodes, int [][] dp, int [][]ending) {
        int allVisitedMask = (1 << numberOfNodes) - 1;
        for (int node = 0; node < numberOfNodes; ++node) {
            dp[allVisitedMask][node] = 0;
            ending[allVisitedMask][node] = node;
        }
        for (int mask = allVisitedMask - 1; mask > 0; --mask) {
            for (int node = 0; node < numberOfNodes; ++node) {

                if (!isVisited(node, mask)) dp[mask][node] = Integer.MAX_VALUE;
                else {
                    int minVal = Integer.MAX_VALUE;
                    int endingNodeFromHere = 0;
                    for (int toGo = 0; toGo < numberOfNodes; ++toGo) {
                        if (!isVisited(toGo, mask)) {
                            int maskAfterVisitNode = maskAfterVisitNode(mask, toGo);
                            int minDistWithToGo = distance(node, toGo) +  dp[maskAfterVisitNode][toGo];
                            if (minDistWithToGo < minVal) {
                                minVal = minDistWithToGo;
                                endingNodeFromHere = ending[maskAfterVisitNode][toGo];
                            }
                        }
                    }
                    dp[mask][node] = minVal;
                    ending[mask][node] = endingNodeFromHere;
                }

            }
        }



        int ans = Integer.MAX_VALUE;
        int startingNode = 0;
        for (int node  = 0; node < numberOfNodes; ++node) {
            int maskWithOnlyThisNodeVisited = 1 << node;
            int endingNodeIfStartFromThis = ending[maskWithOnlyThisNodeVisited][node];
            int distanceWithThisAsFirstNode = dp[maskWithOnlyThisNodeVisited][node] +
                    distance(node, numberOfNodes) + distance(endingNodeIfStartFromThis, numberOfNodes);
            if (distanceWithThisAsFirstNode < ans) {
                ans = distanceWithThisAsFirstNode;
                startingNode = node;
            }
        }
        return ans;
    }


    public static void main(String[] args) {
	// write your code here
        int tc;
        int dp[][] = new int[1 << 13][10];
        int ending[][] = new int[1 << 13][10];

        Scanner sc = new Scanner(System.in);
        tc = sc.nextInt();
        while (tc-- > 0){
            sc.nextInt(); sc.nextInt();
            int positionX = sc.nextInt(), positionY = sc.nextInt();
            int numberOfNodes = sc.nextInt();
            xCoor[numberOfNodes] = positionX;
            yCoor[numberOfNodes] = positionY;
            for (int i = 0 ; i < numberOfNodes; ++i) {
                xCoor[i] = sc.nextInt();
                yCoor[i] = sc.nextInt();
            }
            System.out.printf("The shortest path has length %d\n", TSP(numberOfNodes, dp, ending));
        }
    }
}
