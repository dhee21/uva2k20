package com.company;

import java.util.Scanner;

public class Main {

    static int findSum(int r1, int r2, int c1, int c2, int[][] totalSum) {
        int total = totalSum[r2][c2];
        int left = c1 == 0 ? 0 : totalSum[r2][c1 - 1];
        int above = r1 == 0 ? 0 : totalSum[r1 - 1][c2];
        int diagBefore = c1 == 0 || r1 == 0 ? 0 : totalSum[r1 - 1][c1 - 1];

        return total - left - above + diagBefore;
    }
    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = sc.nextInt();
        int grid[][] = new int[80][80];
        int totalSum[][] = new int[80][80];
        while (tc -- > 0) {
            int maxSum = Integer.MIN_VALUE;
            int n = sc.nextInt();
            for (int i = 0 ; i < n; ++i) for (int j = 0 ; j < n ; ++j) grid[i][j] = sc.nextInt();
            for (int col = 0 ; col < n; ++col) totalSum[0][col] = (col == 0 ? 0 : totalSum[0][col - 1]) + grid[0][col];
            for (int row = 1 ; row < n; ++row) totalSum[row][0] = totalSum[row - 1][0] + grid[row][0];
            for (int row = 1; row < n; ++row)  for (int col = 1 ; col < n; ++col) {
                totalSum[row][col] = totalSum[row - 1][col] + totalSum[row][col - 1]
                        - totalSum[row - 1][col - 1] + grid[row][col];
            }

            for (int r1 = 0; r1 < n; ++r1) for (int r2 = r1; r2 < n; ++r2)
                for (int c1 = 0 ; c1 < n; ++c1) for (int c2 = c1; c2 < n; ++c2) {
                    int v1 = findSum(r1, r2, c1, c2, totalSum);
                    int rowMajor = findSum(r1, r2, 0, n - 1, totalSum);
                    int colMajor = findSum(0, n - 1, c1, c2, totalSum);
                    int v2 = rowMajor - v1;
                    int v3 = colMajor - v1;
                    int v4 = totalSum[n - 1][n - 1] - v1 - v2 - v3;
                    maxSum = Math.max(maxSum, v1);
                    if (!(c1 == 0 && c2 == n - 1)) {
                        maxSum = Math.max(maxSum, v2);
                    }
                    if (!(r1 == 0 && r2 == n - 1)) {
                        maxSum = Math.max(maxSum, v3);
                    }
                    if (!(c1 == 0 && c2 == n - 1) && !(r1 == 0 && r2 == n - 1)) {
                        maxSum = Math.max(maxSum, v4);
                    }
                }
            System.out.println(maxSum);
        }
    }
}
