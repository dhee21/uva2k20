package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int dp[][] = new int[300][300];
        int sum[][] = new int[300][300];
        int freq[] = new int[300];
        while (sc.hasNext()) {
            int n = sc.nextInt();
            for (int i = 0; i < n; ++i) freq[i] = sc.nextInt();

            for (int len = 1; len <= n; ++len) {
                for (int st = 0; st < n - len + 1; ++st) {
                    int ed = st + len - 1;
                    if (len == 1) {
                        dp[st][ed] = 0;
                        sum[st][ed] = freq[st];
                    } else {
                        int minCost = Integer.MAX_VALUE;
                        sum[st][ed] = sum[st][st] + sum[st + 1][ed];
                        for (int root = st; root <= ed; ++root) {
                            int costWithThisRoot = 0;
                            if (root != st) {
                                costWithThisRoot += dp[st][root - 1] + sum[st][root - 1];
                            }
                            if (root != ed) {
                                costWithThisRoot += dp[root + 1][ed] + sum[root + 1][ed];
                            }
                            minCost = Math.min(minCost, costWithThisRoot);
                        }
                        dp[st][ed] = minCost;
                    }
                }
            }
            System.out.println(dp[0][n - 1]);
        }
    }
}
