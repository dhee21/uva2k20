package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int xVal[] = new int[60];
        int yVal[] = new  int[60];
        int dp[][][] = new int[45][330][330];
        Scanner sc = new Scanner(System.in);
        int tc = sc.nextInt();
        while (tc -- > 0) {
            int coinsNum = sc.nextInt(), modulus = sc.nextInt();
            for (int i = 0 ; i < coinsNum; ++i) {
                xVal[i] = sc.nextInt();
                yVal[i] = sc.nextInt();
            }

            for (int x = 0 ; x <= modulus; ++x) for (int y = 0 ; y <= modulus; ++y) {
                if ((x*x + y*y) == (modulus*modulus)) dp[coinsNum][x][y] = 0;
                else dp[coinsNum][x][y] = Integer.MAX_VALUE;
            }

            for (int i = coinsNum - 1 ; i >= 0; --i) {
                for (int x = modulus; x >= 0; --x) for (int y = modulus; y >= 0; --y) {
                    int mincoins = dp[i + 1][x][y];
                    if (xVal[i] != 0 || yVal[i] != 0) {
                        int newX = x + xVal[i], newY = y + yVal[i];
                        if (newX <= modulus && newY <= modulus) {
                            int val = dp[i][newX][newY];
                            if (val != Integer.MAX_VALUE) {
                                mincoins = Math.min(1 + val, mincoins);
                            }
                        }
                    }
//                    System.out.println("i =" +i +  " x = " + x + "y = " + y + " = " + mincoins);
                    dp[i][x][y] = mincoins;
                }
            }
            int ans = dp[0][0][0];
            if (ans == Integer.MAX_VALUE) {
                System.out.println("not possible");
            } else {
                System.out.println(ans);
            }
        }


    }
}
