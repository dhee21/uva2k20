package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    static void floyWarshall(double[][] edges , int nodes, double[][] shortestDist) {
        for (double[] arr: shortestDist) {
            Arrays.fill(arr, Double.MAX_VALUE);
        }

        for (int i = 0 ; i < nodes; ++i) {
            for (int j = 0 ; j < nodes; ++j) {
                if (i == j) shortestDist[i][j] = 0;
                else if (edges[i][j] != Double.MAX_VALUE) {
                    shortestDist[i][j] = edges[i][j];
                }
            }
        }

        for (int k = 0 ; k < nodes; ++k) for (int n1 = 0; n1 < nodes; ++n1) for (int n2 = 0; n2 < nodes; ++n2) {
            if (shortestDist[n1][k] != Double.MAX_VALUE && shortestDist[k][n2] != Double.MAX_VALUE) {
                double val = Math.min(shortestDist[n1][k] + shortestDist[k][n2], shortestDist[n1][n2]);
                shortestDist[n1][n2] = val;
            }
        }
    }

    static boolean bookTaken(int mask, int book) {
        return (mask & (1 << book)) > 0;
    }

    static int takeBook(int book , int mask) {
        return mask | (1 << book);
    }

    static double cost (int booksTaken, int storeAt, int booksNum, double[][] shortestDist,
                      double[] save, int[] availableOn, double[][] dp) {
        if (booksTaken == ((1 << booksNum) - 1)) {
            return shortestDist[storeAt][0];
        }
        if (dp[booksTaken][storeAt] != Double.MAX_VALUE) return dp[booksTaken][storeAt];

        double minCost = Double.MAX_VALUE;
        for (int bookToTake = 0 ; bookToTake < booksNum; ++bookToTake) {
            if (!bookTaken(booksTaken, bookToTake)) {
                double cost = cost(takeBook(bookToTake, booksTaken), availableOn[bookToTake],
                        booksNum, shortestDist, save, availableOn, dp);
                if (cost != Double.MAX_VALUE) {
                    cost += shortestDist[storeAt][availableOn[bookToTake]];
                    cost -= save[bookToTake];
                }
                minCost = Math.min(minCost, cost);
            }
        }
        minCost = Math.min(minCost, shortestDist[storeAt][0]);
        dp[booksTaken][storeAt] = minCost;
        return minCost;
    }

    public static void main(String[] args) {
	// write your code here
        int tc;
        Scanner sc = new Scanner(System.in);
        tc = sc.nextInt();

        double edges[][] = new double[55][55];
        double shortestDist[][] = new double[55][55];
        double save[] = new double[55];
        int availableOn[] = new int[55];
        double dp[][] = new double[1 << 15][55];
        int st, ed;
        double distance;

        while (tc -- > 0) {
            for (double[] arr: edges) {
                Arrays.fill(arr, Double.MAX_VALUE);
            }
            for (double[] arr: dp) {
                Arrays.fill(arr, Double.MAX_VALUE);
            }

            int n = sc.nextInt(), m = sc.nextInt();
            for (int i = 0; i < m; ++i) {
                st = sc.nextInt();
                ed = sc.nextInt();
                distance = sc.nextDouble();
                edges[st][ed] = Math.min(edges[st][ed], distance);
                edges[ed][st] = Math.min(edges[ed][st], distance);
            }
            int booksNum = sc.nextInt();
            for (int i = 0 ; i < booksNum; ++i) {
                availableOn[i] = sc.nextInt();
                save[i] = sc.nextDouble();
            }

            floyWarshall(edges, n + 1, shortestDist);

            double costVal = cost(0, 0, booksNum, shortestDist, save, availableOn, dp);
            if (costVal > 0 || Math.abs(costVal - 0) < 0.00001) {
                System.out.println("Don't leave the house");
            } else {
                System.out.printf("Daniel can save $%.2f\n", -costVal);
            }
        }

    }
}
