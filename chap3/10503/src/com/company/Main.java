package com.company;

import java.util.Scanner;

public class Main {
    static int[] front = new int[20];
    static int[] rear = new int[20];
    static int[] dominoes = new int[20];
    static boolean[] used = new boolean[20];
    static boolean[] isFrontUsed = new boolean[20];

    public static boolean isPossible(int index, int spaces, int numDomino) {
        if (index == spaces + 1) {
            int lastDomino = dominoes[index];
            int prevDomino = dominoes[index - 1];
            boolean isFrontOfPrev = isFrontUsed[prevDomino];
            int backSideOfPrev = isFrontOfPrev ? rear[prevDomino] : front[prevDomino];
            if (front[lastDomino] == backSideOfPrev) return true;
            return false;
        }
        int prevDomino = dominoes[index - 1];
        boolean isFrontOfPrev = isFrontUsed[prevDomino];
        int backSideOfPrev = isFrontOfPrev ? rear[prevDomino] : front[prevDomino];
        for (int i = 2; i < numDomino + 2; ++i) {
            if (!used[i] && (front[i] == backSideOfPrev || rear[i] == backSideOfPrev)) {
                dominoes[index] = i;
                used[i] = true;
                isFrontUsed[i] = front[i] == backSideOfPrev;
                boolean isPossible = isPossible(index + 1, spaces, numDomino);
                used[i] = false;
                if (isPossible) return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (true) {
            int n = sc.nextInt();
            if (n == 0) break;
            int m = sc.nextInt();
            front[0] = sc.nextInt(); rear[0] = sc.nextInt(); used[0] = true; isFrontUsed[0] = true;
            front[1] = sc.nextInt(); rear[1] = sc.nextInt(); used[1] = true; isFrontUsed[1] = true;
            dominoes[0] = 0;
            dominoes[n + 1] = 1;
            for (int i = 0; i < m; ++i) {
                front[2 + i] = sc.nextInt();
                rear[2 + i] = sc.nextInt();
                used[2 + i] = false;
            }
            boolean isPossible = isPossible(1, n, m);
            if (isPossible) {
                System.out.println("YES");
            } else System.out.println("NO");
        }
    }
}
