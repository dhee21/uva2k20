package com.company;

import java.util.Scanner;

public class Main {

    static int isMajCoalition (int bitset, int[] strength, int total, int parties) {
        int coalStrength = 0;
        for (int i = 0 ; i < parties; ++i) {
            if ((bitset & (1 << i)) > 0) coalStrength += strength[i];
        }
        if (coalStrength > (total >> 1)) {
            return coalStrength;
        }
        return -1;
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = sc.nextInt();
        int[] strength = new int[25], powIndex = new int[25];
        while (tc-- > 0) {
            int parties = sc.nextInt();
            int total = 0;
            for (int i = 0 ; i < parties; ++i) {
                strength[i] = sc.nextInt();
                powIndex[i] = 0;
                total += strength[i];
            }
            int coalitionStrength;
            for (int i = 0; i < (1 << parties); ++i) {
                coalitionStrength = isMajCoalition(i, strength, total, parties);
                if (coalitionStrength != -1) {
                    for (int j = 0 ; j < parties; ++j) {
                        boolean doesCoalHasPartyJ = (i & (1 << j)) > 0;
                        if (doesCoalHasPartyJ && coalitionStrength - strength[j] <= (total >> 1)) powIndex[j]++;
                    }
                }
            }
            for (int i = 0 ; i < parties; ++i) {
                System.out.printf("party %d has power index %d\n", i + 1, powIndex[i]);
            }
            System.out.println();

        }
    }
}
