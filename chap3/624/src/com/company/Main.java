package com.company;

import java.util.Scanner;

public class Main {

    static int findLengthUsed (int mask, int[] tracks, int numOfTracks) {
        int index = 0, len = 0;
        while (mask > 0) {
            boolean isOne = (mask & 1) == 1;
            if (isOne) len += tracks[index];
            index++;
            mask >>= 1;
        }
        return len;
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int[] tracks = new int[25];
        while (sc.hasNext()) {
            int n = sc.nextInt();
            int trackNum = sc.nextInt();
            for (int i = 0 ; i < trackNum; ++i) tracks[i] = sc.nextInt();
            int minUnused = Integer.MAX_VALUE;
            int maskMinUsed = 0;
            for (int i = 0 ; i < (1 << trackNum); ++i) {
                int usedLength = findLengthUsed(i, tracks, trackNum);
                if (n - usedLength >= 0 && n - usedLength < minUnused) {
                    minUnused = n - usedLength;
                    maskMinUsed = i;
                }
            }
            int index = 0;
            while (maskMinUsed > 0) {
                boolean isOne = (maskMinUsed & 1) == 1;
                if (isOne) System.out.print(tracks[index] + " ");
                index++;
                maskMinUsed >>= 1;
            }
            System.out.println("sum:" + (n - minUnused));
        }
    }
}
