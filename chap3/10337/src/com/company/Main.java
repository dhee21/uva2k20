package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = sc.nextInt();
        int [][] windStrengths = new int[10][110];
        int [][] dp = new int[10][110];
        while (tc-- > 0) {
            int X = sc.nextInt();
            int len = X / 100;


            for (int i = 0; i < 10; ++i) {
                for (int j = 0; j < len; ++j) {
                    windStrengths[9 - i][j] = sc.nextInt();
                }
            }

            if (len == 1) {
                System.out.println(30 - windStrengths[0][0]);
                continue;
            }

            for (int h = 0; h < 10; ++h) {
                if (h == 0) dp[h][len] = 0;
                else dp[h][len] = Integer.MAX_VALUE;
            }

            for (int col = len - 1; col >= 0; --col) {
                for (int h = 0; h < 10; ++h) {
                    int min = Integer.MAX_VALUE;

                    if (h - 1 >= 0 && dp[h - 1][col + 1] != Integer.MAX_VALUE) {
                        min = Math.min(min, 20 - windStrengths[h][col] + dp[h - 1][col + 1]);
                    }
                    if (dp[h][col + 1] != Integer.MAX_VALUE) min = Math.min(min, 30 - windStrengths[h][col] + dp[h][col + 1]);
                    if (h + 1 < 10 && dp[h + 1][col + 1] != Integer.MAX_VALUE) {
                        min = Math.min(min, 60 - windStrengths[h][col] + dp[h + 1][col + 1]);
                    }
                    dp[h][col] = min;
                }
            }

            System.out.println(dp[0][0] + "\n");
        }
    }
}
