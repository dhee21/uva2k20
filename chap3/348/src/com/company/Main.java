package com.company;

import java.util.Scanner;

public class Main {

    static String findAns (int st, int ed, int[][]minCut) {
        if (st == ed) return "A" + (st + 1);
        int cut = minCut[st][ed];
        return "(" + findAns(st, cut, minCut) + " x " + findAns(cut + 1, ed, minCut) + ")";
    }
    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int n;
        int caseNo = 1;
        int rowInput[] = new int[15];
        int colInput[] = new int[15];
        int dp[][] = new int[15][15];
        int minCut[][] = new int[15][15];
        while (true) {
            n = sc.nextInt();
            if (n == 0) break;
            for (int i = 0; i < n; ++i) {
                rowInput[i] = sc.nextInt();
                colInput[i] = sc.nextInt();
            }
            for (int len = 1; len <= n; ++len) {
                for (int st = 0; st < n - len + 1; ++st) {
                    int ed = st + len - 1;
                    if (len == 1) dp[st][ed] = 0;
                    else {
                        int minMultiplication = Integer.MAX_VALUE, minCutVal = st;
                        for (int cut = st; cut < ed; ++cut) {
                            int multiplyCost = rowInput[st] * colInput[cut] * colInput[ed];
                            int minValWithThisCut = multiplyCost + dp[st][cut] + dp[cut + 1][ed];
                            if (minValWithThisCut < minMultiplication) {
                                minMultiplication = minValWithThisCut;
                                minCutVal = cut;
                            }
                        }
                        dp[st][ed] = minMultiplication;
                        minCut[st][ed] = minCutVal;
                    }
                }
            }

            String ans = findAns(0, n - 1, minCut);
            System.out.printf("Case %d: %s\n", caseNo++, ans);
        }
    }
}
