package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int[] frontTeeth = new int[110], rearTeeth = new int[110];
        while (true) {
            int front  = sc.nextInt();
            if (front == 0) break;
            int rear = sc.nextInt();
            for (int i = 0 ; i < front; ++i) frontTeeth[i] = sc.nextInt();
            for (int i = 0 ; i < rear; ++i) rearTeeth[i] = sc.nextInt();

            ArrayList<Double> ratios = new ArrayList<>();

            for (int i = 0; i < rear; ++i) {
                for (int j = 0 ; j < front; ++j) {
                    double x = (double)rearTeeth[i] / (double)frontTeeth[j];
//                    System.out.println("i "+ rearTeeth[i] + " j " + frontTeeth[j] + "   = " + x);
                    ratios.add(x);
                }
            }
            ratios.sort((a, b) -> {
                return  (a - b) < 0 ? -1 : (a - b) > 0 ? 1 : 0;
            });

            double maxSpread = 0;
            for (int i = 0 ; i < ratios.size() - 1; ++i) {
//                System.out.println(ratios.get(i));
                double spread = ratios.get(i + 1) / ratios.get(i);
                maxSpread = Math.max(spread, maxSpread);
            }

            System.out.printf("%.2f\n", maxSpread);
        }

    }
}
