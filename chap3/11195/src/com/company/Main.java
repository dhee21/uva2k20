package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;


public class Main {


    static int backTrack (int col, int n, int diagonalsGoingDown, int diagonalsGoinUp, int rowMask, boolean[][] badSquare) {
        if (col == n) {
            return 1;
        }
        int sum = 0;
        for (int r = 0 ; r < n; ++r) {
            if (!badSquare[r][col] && (rowMask & (1 << r) ) == 0 && (diagonalsGoingDown & (1 << (r - col + n - 1))) == 0 &&
                    (diagonalsGoinUp & (1 << (r + col))) == 0) {

                sum += backTrack(col + 1, n, diagonalsGoingDown | (1 << (r - col + n - 1)),
                        diagonalsGoinUp | (1 << (r + col)),
                        rowMask | (1 << r), badSquare);
            }
        }
        return sum;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader sc = new BufferedReader(new
                InputStreamReader(System.in));
        StringBuilder string = new StringBuilder();
        int n;
        int caseVal = 0;
        while (true) {
            int validQueens;

            n = Integer.parseInt(sc.readLine());
            boolean badSquare[][] = new boolean[n][n];
            if (n == 0) break;
            caseVal ++;
            for (int i = 0 ; i < n; ++i) {
                String x = sc.readLine();
                for (int j = 0 ; j < n; ++j) {
                    if (x.charAt(j) == '*') badSquare[i][j] = true;
                }
            }
            validQueens = backTrack(0, n, 0, 0, 0, badSquare);
            string.append("Case " + caseVal + ": "  + validQueens + "\n");
        }
        System.out.print(string);
    }
}
