package com.company;

import java.util.Scanner;

public class Main {

    static long dp[][] = new long[110][110];
    static long mod = 1000000;
    static void calculate() {
        for (int n = 0; n <= 100; ++n) for (int k = 0 ; k <= 100; ++k) {
            if (n == 0) {
                dp[n][k] = 1;
                continue;
            }
            if (k == 0) {
                dp[n][k] = 0;
                continue;
            }
            dp[n][k] = 0;
            for (int firstSum = 0; firstSum <= n; ++firstSum) {
                dp[n][k] = (dp[n][k] % mod +  dp[n - firstSum][k - 1] % mod) % mod;
            }
        }
    }
    public static void main(String[] args) {
	// write your code here
        calculate();
        Scanner sc = new Scanner(System.in);
        int n, k;
        while (true) {
            n = sc.nextInt();
            k = sc.nextInt();
            if (n == 0 && k == 0) break;

            System.out.println(dp[n][k]);
        }
    }
}
