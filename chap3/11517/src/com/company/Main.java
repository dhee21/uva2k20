package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    static class Pair {
        int amount, coins;
        Pair(int a, int b) {
            amount = a; coins = b;
        }
    }
    static int CANNOT_BE_PAID = -1;
    static Pair calculate(int index , int amount, int[]coins ,  int [][]amountDP, int [][] coinsDP, int len) {
        if (index == len) {
            if (amount > 0) return new Pair(CANNOT_BE_PAID , 0);
            else return new Pair(0, 0);
        }
        if (amount <= 0) {
            return new Pair(0, 0);
        }

        if (amountDP[index][amount] != -1) return new Pair(amountDP[index][amount], coinsDP[index][amount]);

        int minAmount = 0, mincoins = 0;

        Pair p1 = calculate(index + 1, amount, coins, amountDP, coinsDP, len);
        boolean isP1 = false;
        int amountP1 = 0, coinsP1 = 0;
        if (p1.amount != CANNOT_BE_PAID) {
            amountP1 = p1.amount; coinsP1 = p1.coins; isP1 = true;
        }

        Pair p2 = calculate(index + 1, amount - coins[index], coins, amountDP, coinsDP, len);
        int amountP2 = 0, coinsP2 = 0;
        boolean isP2 = false;
        if (p2.amount != CANNOT_BE_PAID) {
            amountP2 = p2.amount + coins[index]; coinsP2 = p2.coins + 1; isP2 = true;
        }

        if (isP1 && isP2) {
            if (amountP2 < amountP1) {
                minAmount = amountP2; mincoins = coinsP2;
            } else if (amountP1 < amountP2) {
                minAmount = amountP1; mincoins = coinsP1;
            } else {
                minAmount = amountP1;
                mincoins = Math.min(coinsP1, coinsP2);
            }
        } else if (isP1 || isP2) {
            if (isP1) {
                minAmount = amountP1; mincoins = coinsP1;
            } else {
                minAmount = amountP2; mincoins = coinsP2;
            }
        } else {
            minAmount = CANNOT_BE_PAID; mincoins = 0;
        }

        amountDP[index][amount] = minAmount; coinsDP[index][amount] = mincoins;
        return new Pair(minAmount, mincoins);
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
        StringBuffer string = new StringBuffer();
        int tc = Integer.parseInt(sc.readLine());
        int coins[] = new int[110];
        int amountDP[][] = new int[110][20010];
        int coinsDP[][] = new int[110][20010];

        while (tc-- > 0) {
            int amount = Integer.parseInt(sc.readLine());
            int numOfCoins = Integer.parseInt(sc.readLine());
            for (int i = 0;i < numOfCoins; ++i) coins[i] = Integer.parseInt(sc.readLine());

            for (int[] row : amountDP) Arrays.fill(row, -1);

            amountDP[numOfCoins][0] = 0; coinsDP[numOfCoins][0] = 0;
            for (int sum = 1; sum <= (20000); ++sum) amountDP[numOfCoins][sum] = CANNOT_BE_PAID;

            for (int i = numOfCoins - 1; i >= 0; --i) {
                for (int sum = 0; sum <= (20000); ++sum) {
                    int minAmount, minCoins;
                    minAmount = amountDP[i + 1][sum]; minCoins = coinsDP[i + 1][sum];

                    int sumAfterReduction = sum - coins[i];
                    int amountValAfterChoose = CANNOT_BE_PAID, coinValAfterChoose = 0;
                    if (sumAfterReduction >= 0 && amountDP[i + 1][sumAfterReduction] != CANNOT_BE_PAID) {
                       amountValAfterChoose = amountDP[i + 1][sumAfterReduction] + coins[i];
                       coinValAfterChoose = coinsDP[i + 1][sumAfterReduction] + 1;
                    } else if (sumAfterReduction < 0){
                        amountValAfterChoose = coins[i];
                        coinValAfterChoose = 1;
                    }

                    if (minAmount != CANNOT_BE_PAID && amountValAfterChoose != CANNOT_BE_PAID) {
                        if (amountValAfterChoose < minAmount) {
                            minAmount = amountValAfterChoose; minCoins = coinValAfterChoose;
                        } else if (amountValAfterChoose == minAmount) {
                            minCoins = Math.min(minCoins, coinValAfterChoose);
                        }
                    } else if (amountValAfterChoose != CANNOT_BE_PAID) {
                        minAmount = amountValAfterChoose; minCoins = coinValAfterChoose;
                    }

                    amountDP[i][sum] = minAmount; coinsDP[i][sum] = minCoins;
                }
            }

            string.append(amountDP[0][amount] + " " + coinsDP[0][amount] + "\n");
        }
        System.out.print(string);
    }
}
