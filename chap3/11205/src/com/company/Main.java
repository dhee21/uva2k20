package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

public class Main {

    static boolean isPossibleWithTheseLEDs (int ledsStillToChoose, int mask, int[] symbols,
                                            int numSymbols, int index) {
        if (ledsStillToChoose > (numSymbols - index)) return false;
        if (ledsStillToChoose == 0) {
            HashSet<Integer> set = new HashSet<>();
            for (int i = 0 ; i < numSymbols; ++i) {
                int newSymbol = symbols[i] & mask;
                if (set.contains(newSymbol)) return false;
                else set.add(newSymbol);
            }
            return true;
        }
        int chooseIndex = mask | (1 << index);
        boolean isPossible = isPossibleWithTheseLEDs(ledsStillToChoose - 1, chooseIndex, symbols, numSymbols, index + 1);
        if (isPossible) return true;
        return isPossibleWithTheseLEDs(ledsStillToChoose, mask, symbols, numSymbols, index + 1);
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        StringBuffer ansString = new StringBuffer();
        while (t-- > 0) {
            int numLEDs = sc.nextInt();
            int numSymbols = sc.nextInt();
            int[] symbols = new int[numSymbols];
            for (int i = 0 ; i < numSymbols; ++i) {
                int aSymbol = 0;
                for (int j = 0 ; j < numLEDs; ++j) {
                    aSymbol = (aSymbol << 1) | sc.nextInt();
                }
                symbols[i] = aSymbol;
            }
            int ans = 200;

            for (int mask = 0 ; mask < (1 << numLEDs); ++mask) {
                HashSet<Integer> set = new HashSet<>();
                boolean isPoss = true;
                for (int i = 0 ; i < numSymbols; ++i) {
                    int newSymbol = symbols[i] & mask;
                    if (set.contains(newSymbol)) {
                        isPoss = false;
                        break;
                    } else set.add(newSymbol);
                }
                if (isPoss) ans = Math.min(ans, Integer.bitCount(mask));
            }

//            for (int numActiveLEDs = 0; numActiveLEDs <= numLEDs; ++numActiveLEDs) {
//                boolean isPossible = isPossibleWithTheseLEDs(numActiveLEDs, 0, symbols, numSymbols,
//                        0);
//                if (isPossible) {
//                    ans = numActiveLEDs;
//                    break;
//                }
//            }
            ansString.append(ans).append("\n");
        }
        System.out.print(ansString);
    }
}
