package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashSet;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
        StringBuilder ans = new StringBuilder();

        while (true) {
            int num = Integer.parseInt(sc.readLine());
            if (num == 0) break;
            boolean allZero = true;
            for (int i = 0 ; i < num; ++i) {
                int val = Integer.parseInt(sc.readLine());
                if (val != 0) {
                    allZero = false;
                    ans.append(val).append(" ");
                }
            }
            if (allZero) {
                ans.append("0\n");
            } else {
                ans.setCharAt(ans.length() - 1, '\n');
            }
        }
        System.out.print(ans);
    }
}
