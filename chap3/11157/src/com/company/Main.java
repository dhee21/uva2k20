package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        int t;
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        StringBuilder string = new StringBuilder();
        t = Integer.parseInt(reader.readLine());
        boolean[] stones = new boolean[110];
        int[] distance = new int[110];
        int caseVal = 0;
        while (t-- > 0) {
            caseVal++;
            int n, d;
            String inp[] = reader.readLine().split(" ");
            n = Integer.parseInt(inp[0]); d = Integer.parseInt(inp[1]);
            inp = new String[2000];
            int total = 0;
            while (true) {
                String[] x = reader.readLine().split(" ");
                for (int i = 0 ; i < x.length; ++i) {
                    inp[total++] = x[i];
                }
                if (total == n) break;
            }
            stones[0] = true;
            distance[0] = 0;
//            System.out.println(stones[0] + " " + distance[0]);

            for (int i = 0 ; i < n; ++i) {
                String[] stone = inp[i].split("-");
                stones[i + 1] = stone[0].equals("B");
                distance[i + 1] = Integer.parseInt(stone[1]);
//                System.out.println(stones[i + 1] + " " + distance[i + 1]);
            }
            stones[n + 1] = true;
            distance[n + 1] = d;
//            System.out.println(stones[n + 1] + " " + distance[n + 1]);


            int maxLeap = 0;
            for (int i = 0; i <= n; ++i) {
                if (stones[i + 1]) maxLeap = Math.max(maxLeap, distance[i + 1] - distance[i]);
                else maxLeap = Math.max(maxLeap, distance[i + 2] - distance[i]);
            }

            string.append("Case ").append(caseVal).append(": ").append(maxLeap).append('\n');
        }
        System.out.print(string);
    }
}
