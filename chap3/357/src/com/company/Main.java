package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        int M = 30000, I = 5;
        long ways[][] = new long[M + 400][I + 4];
        int coins[] = {50, 25, 10, 5, 1};
        int len = coins.length;

        ways[0][len] = 1;
        for (int m = 1; m <= M; ++m) ways[m][len] = 0;

        for (int i = len - 1; i >= 0; --i) {
            for (int m = 0 ; m <= M; ++m) {
                if (m == 0) {
                    ways[m][i] = 1; continue;
                }
                long wayVal = 0;
                for (int j = i; j < len; ++j) {
                    if (m - coins[j] >= 0) wayVal += ways[m - coins[j]][j];
                }
                ways[m][i] = wayVal;
            }
        }

        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String inp = rd.readLine();
            if (inp == null) break;
            int input = Integer.parseInt(inp);
            long ans = ways[input][0];
            if (ans == 1) {
                System.out.printf("There is only 1 way to produce %d cents change.\n", input);
            } else {
                System.out.printf("There are %d ways to produce %d cents change.\n", ans, input);
            }
        }
    }
}
