package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void swap(int[] arr, int from, int to) {
        int toVal = arr[to];
        arr[to] = arr[from];
        arr[from] = toVal;
    }

    public static int findMinElementGreaterThan (int[] arr, int element, int startIndex) {
        int minElement = Integer.MAX_VALUE;
        int minElementIndex = arr.length;
        for (int i = startIndex; i < arr.length; ++i) {
            if ((arr[i] > element) && (arr[i] < minElement)) {
                minElementIndex = i;
                minElement = arr[i];
            }
        }
        return minElementIndex;
    }

    public static boolean nextPermutation(int[] arr) {
        int index = arr.length - 2;
        while (index >= 0 && arr[index] >= arr[index + 1]) index--;
        if (index < 0) return false;

        int indexToSwapTo = findMinElementGreaterThan(arr, arr[index], index + 1);
        swap(arr, index, indexToSwapTo);
        Arrays.sort(arr, index + 1, arr.length);
        return true;
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (true) {
            int n = sc.nextInt(), m = sc.nextInt();
            if (n == 0 && m == 0) break;
            int[] arr = new int[n], first = new int[m], second = new int[m], distance = new int[m];
            for (int i = 0 ; i < n; ++i) arr[i] = i;
            for (int i = 0 ; i < m; ++i) {
                first[i] = sc.nextInt();
                second[i] = sc.nextInt();
                distance[i] = sc.nextInt();
            }

            int[] indexes = new int[10];
            int ans = 0;
            while (true) {
                for (int i = 0 ; i < n; ++i) indexes[arr[i]] = i;
                boolean allConstraints = true;
                for (int i = 0 ; i < m; ++i) {
                    int c = distance[i];
                    int distanceBetween = Math.abs(indexes[first[i]] - indexes[second[i]]);
                    if (c > 0) {
                        allConstraints = distanceBetween <= c;
                    } else {
                        allConstraints = distanceBetween >= -c;
                    }
                    if (!allConstraints) break;
                }
                if (allConstraints) ans++;
                boolean isNext = nextPermutation(arr);
                if (!isNext) break;
            }
            System.out.println(ans);
        }
    }
}
