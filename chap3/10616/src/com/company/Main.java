package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    static int remainder (int num, int div) {
        if (num >= 0) return num % div;
        int x = (-num) % div;
        if (x == 0) return 0;
        return div - x;
    }

    static void calculateDPStates(int[][][] dp, int[] numbers,  int n, int m, int d) {
        for (int i = 0; i < n; ++i) for (int c = 0 ; c <= m; ++c) for (int r = 0; r < d; ++r) dp[i][c][r] = 0;
        int remainderOfZerothElement = remainder(numbers[0], d);
        dp[0][0][0] = 1;
        dp[0][1][remainderOfZerothElement] = 1;
        for (int i = 0; i < n - 1; ++i) {
            for (int c = 0 ; c <= m; ++c) {
                for (int r = 0; r < d; ++r) {
                   dp[i + 1][c][r] += dp[i][c][r];
                   int rem = (r + remainder(numbers[i + 1] ,  d) ) % d;
                   dp[i + 1][c + 1][rem] += dp[i][c][r];
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        int N = 205, M = 15, D = 22;
        int[][][] dp = new int[N][M][D];

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int numbers[] = new int[N];
        StringBuffer string = new StringBuffer();
        int caseVal = 1;
        while (true) {
            String[] inp = reader.readLine().split(" ");
            int n= Integer.parseInt(inp[0]), q = Integer.parseInt(inp[1]);
            if (n == 0 && q == 0) break;

            for (int i = 0 ; i < n; ++i) {
                numbers[i] = Integer.parseInt(reader.readLine());
            }
            string.append("SET " + caseVal + ":\n");
            for (int i = 0 ; i < q; ++i) {
                inp = reader.readLine().split(" ");
                int d = Integer.parseInt(inp[0]), m = Integer.parseInt(inp[1]);

                calculateDPStates(dp, numbers, n, m, d);
                string.append("QUERY " + (i + 1) + ": " + dp[n - 1][m][0] + "\n");
            }
            caseVal++;
        }
        System.out.print(string);
    }
}
