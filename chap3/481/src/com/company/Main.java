package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;

public class Main {

    static int findIndexToReplaceInBucket(int[] bucket, int len, int valToReplace) {
        int st = 0, ed = len - 1;
        while (st <= ed) {
            int mid = (st + ed) >> 1;
            if (bucket[mid] < valToReplace) {
                st = mid + 1;
            } else if (bucket[mid] == valToReplace) {
                return mid;
            } else {
                if (mid == 0 || bucket[mid - 1] < valToReplace) {
                    return mid;
                } else {
                    ed = mid - 1;
                }
            }
        }
        return st;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int array[] = new int[1000000];
        int len = 0;
        while (true) {
            String inp = reader.readLine();
            if (inp == null) break;
            int integer = Integer.parseInt(inp);
            array[len++] = integer;
        }
        if (len == 0) return;

        int bucket[] = new int[1000000], prevIndex[] = new int[1000000];
        HashMap<Integer, Integer> indexOF = new HashMap<>();
        int lenOfBucket = 0;

        bucket[0] = array[0];
        indexOF.put(array[0], 0);
        prevIndex[0] = -1;
        lenOfBucket = 1;

        for (int i = 1 ; i < len; ++i) {
            int indexToReplace = findIndexToReplaceInBucket(bucket, lenOfBucket, array[i]);
            if (indexToReplace == lenOfBucket) {
                prevIndex[i] = indexOF.get(bucket[lenOfBucket - 1]);
                bucket[lenOfBucket++] = array[i];
            } else {
                if (indexToReplace == 0) {
                    prevIndex[i] = -1;
                } else {
                    prevIndex[i] = indexOF.get(bucket[indexToReplace - 1]);
                }
                bucket[indexToReplace] = array[i];
            }
            indexOF.put(array[i], i);
        }

        StringBuilder string = new StringBuilder();
        string.append(lenOfBucket).append('\n').append("-\n");
        int indexOfLastElement = indexOF.get(bucket[lenOfBucket - 1]);
        int indexVal = indexOfLastElement;
        int ansLen = 0;
        while (indexVal != -1) {
          bucket[ansLen++] = array[indexVal];
          indexVal = prevIndex[indexVal];
        }

        for (int i = ansLen - 1 ; i >= 0; --i) {
            string.append(bucket[i]).append('\n');
        }
        System.out.print(string);
    }
}
