package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static int grid[][] = new int[9][9];

    public static void swap(Integer[] arr, int from, int to) {
        int toVal = arr[to];
        arr[to] = arr[from];
        arr[from] = toVal;
    }

    public static int findMinElementGreaterThan (Integer[] arr, Integer element, int startIndex) {
        int minElement = Integer.MAX_VALUE;
        int minElementIndex = arr.length;
        for (int i = startIndex; i < arr.length; ++i) {
            if (arr[i] > element && arr[i] < minElement) {
                minElementIndex = i;
                minElement = arr[i];
            }
        }
        return minElementIndex;
    }

    public static boolean nextPermutation(Integer[] arr) {
        int index = arr.length - 2;
        while (index >= 0 && arr[index] >= arr[index + 1]) index--;
        if (index < 0) return false;

        int indexToSwapTo = findMinElementGreaterThan(arr, arr[index], index + 1);
        swap(arr, index, indexToSwapTo);
        Arrays.sort(arr, index + 1, arr.length);
        return true;
    }


    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        while (t-- > 0) {
            int n = sc.nextInt();
            for (int i = 0 ; i < n; ++i) for (int j = 0 ; j < n; ++j) {
                grid[i][j] = sc.nextInt();
            }
            Integer row[] = new Integer[n];
            for (int i = 0 ; i < n; ++i) row[i] = i;
            int min = Integer.MAX_VALUE;
            do {
                int val = 0;
                for (int c = 0 ; c < n; ++c) {
                    val += grid[row[c]][c];
                }
                min = Math.min(min, val);
            } while (nextPermutation(row));
            System.out.println(min);
        }
    }
}
