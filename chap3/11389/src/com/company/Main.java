package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (true) {
            int n = sc.nextInt(), d = sc.nextInt(), r = sc.nextInt();
            if (n == 0 && d == 0 && r == 0) break;
            int [] morning = new int[110];
            int [] evening = new int[110];

            for (int i = 0 ; i < n ; ++i) morning[i] = sc.nextInt();
            for (int i = 0 ; i < n ; ++i) evening[i] = sc.nextInt();
            Arrays.sort(morning, 0, n );
            Arrays.sort(evening, 0, n );

            int ans = 0;
            for (int i = 0 ; i < n ; ++i) {
                int sum = morning[i] + evening[n - 1 - i];
                if (sum > d) ans += (sum - d) * r;
            }
            System.out.println(ans);
        }
    }
}
