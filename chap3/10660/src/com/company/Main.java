package com.company;

import java.util.Scanner;

public class Main {


    public static void fillOfficeRowAndCol (int i, int j, int k, int l, int m, int[] officerow, int[] officeCol) {
        officerow[0] = i / 5;
        officeCol[0] = i % 5;
        officerow[1] = j / 5;
        officeCol[1] = j % 5;
        officerow[2] = k / 5;
        officeCol[2] = k % 5;
        officerow[3] = l / 5;
        officeCol[3] = l % 5;
        officerow[4] = m / 5;
        officeCol[4] = m % 5;
    }
    static int findTotalMinDistance (int [] officeRow, int [] officeCol, int[] row, int[] col, int n, int[] population) {
        int mindistTotal = 0;
        for (int i = 0 ; i < n; ++i) {
            int minOfficedist = Integer.MAX_VALUE;
            int cityRow = row[i];
            int cityCol = col[i];
            for (int j = 0 ; j < 5; ++j) {
                int dist = Math.abs(officeRow[j] - cityRow) + Math.abs(officeCol[j] - cityCol);
                minOfficedist = Math.min(minOfficedist, dist);
            }
            mindistTotal += minOfficedist * population[i];
        }
        return mindistTotal;
    }
    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        int[] row = new int[25], col = new int[25], population = new int[25];
        while (t-- > 0) {
            int n = sc.nextInt();
            for (int i = 0 ; i < n; ++i) {
                row[i] = sc.nextInt();
                col[i] = sc.nextInt();
                population[i] = sc.nextInt();
            }

            int[] officeRow = new int[5], officeCol = new int[5];
            int[] ans = new int[5];
            int minDistance = Integer.MAX_VALUE;
            for (int i = 0 ; i < 21; ++i) for (int j = i + 1 ; j < 22; ++j) for (int k = j + 1 ; k < 23; ++k)
                for (int l = k + 1 ; l < 24; ++l) for (int m = l + 1 ; m < 25; ++m) {

                    fillOfficeRowAndCol(i, j, k, l, m, officeRow, officeCol);
                    int distance = findTotalMinDistance(officeRow, officeCol, row, col, n, population);
                    if (distance < minDistance) {
                        ans[0] = i; ans[1] = j; ans[2] = k; ans[3] = l; ans[4] = m;
                        minDistance = distance;
                    }
                }
            System.out.printf("%d %d %d %d %d\n", ans[0], ans[1], ans[2], ans[3], ans[4]);
        }

    }
}
