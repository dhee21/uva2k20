package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

    static class Pair {
        int favour;
        int expense;
        Pair (int a, int b) {
            favour = a; expense = b;
        }
    }

    static int maxFavour(int i, int sum, int items, int [][] dp, int[] price, int[] favour, int pm) {
        if (sum > pm + 200) return Integer.MIN_VALUE;

        if (i == items) {
            if (sum > pm && sum <= 2000) return Integer.MIN_VALUE;
            return 0;
        }

        if (dp[i][sum] != -1) return dp[i][sum];

        int notChoose = maxFavour(i + 1, sum, items, dp, price, favour, pm);
        int choose = favour[i] + maxFavour(i + 1, sum + price[i], items, dp, price,favour, pm);

        dp[i][sum] = Math.max(notChoose, choose);
        return dp[i][sum];

    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String inp;
        int[] price = new int[110], favour = new int[110];
        int[][] dp = new int[110][15000];
        while (true) {
            inp = reader.readLine();
            if (inp == null) break;
            if (inp.length() == 0) continue;
            String[] input = inp.split(" ");
            int pocketMoney = Integer.parseInt(input[0]), items = Integer.parseInt(input[1]);
            for (int i = 0 ; i < items; ++i) {
                input = reader.readLine().split(" ");
                price[i] = Integer.parseInt(input[0]);
                favour[i] = Integer.parseInt(input[1]);
            }

            if (items == 0) {
                System.out.println(0);
                continue;
            }

            for(int[] row: dp) {
                Arrays.fill(row, -1);
            }
            int ans = maxFavour(0, 0, items, dp , price, favour, pocketMoney);
            System.out.println(ans);
        }
    }
}
