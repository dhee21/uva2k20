package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

    static int MAXNODES = 500;

    static char grid[][] = new char[50][50];
    static boolean isEdgeMade[][] = new boolean[MAXNODES][MAXNODES];
    static int[][] adjList = new int[MAXNODES][MAXNODES], nodeNum = new int[50][50];
    static int[] neighboursNum = new int[MAXNODES], leftNodes = new int[MAXNODES];
    static int nodeIndex, leftNodesIndex;
    static int[] rowsInc = {-1, 1, 0, 0};
    static int[] colInc = {0, 0, 1, -1};


    static int[] owner = new int[MAXNODES];
    static boolean visited[] = new boolean[MAXNODES];

    static void init() {
        Arrays.fill(neighboursNum, 0);
        for (boolean[] arr: isEdgeMade) Arrays.fill(arr, false);
        for (int[] arr: nodeNum) Arrays.fill(arr, -1);
        nodeIndex = 0; leftNodesIndex = 0;
    }

    static boolean canGo (int r , int c, int R, int C) {
        return r >= 0 && r < R && c < C && c >= 0 && grid[r][c] == '*';
    }

    static void makeEdge(int n1, int n2) {
        if (isEdgeMade[n1][n2]) return;
        adjList[n1][neighboursNum[n1]++] = n2;
        isEdgeMade[n1][n2] = true;
    }

    static boolean idOdd(int r, int c) {
        return ((r + c) & 1) > 0;
    }

    static void makeNode (int r, int c) {
        boolean isOdd = idOdd(r, c);
        nodeNum[r][c] = nodeIndex++;
        if (isOdd) leftNodes[leftNodesIndex++] = nodeNum[r][c];
    }

    static void makeGraph(int h, int w) {
        for (int r = 0; r < h; ++r) for (int c = 0; c < w; ++c) if (grid[r][c] == '*') makeNode(r, c);
        for (int r = 0; r < h; ++r) for (int c = 0; c < w; ++c) if (grid[r][c] == '*' && idOdd(r, c)) {
            for (int i = 0; i < 4; ++i) {
                int rowVal = r  + rowsInc[i];
                int colVal = c + colInc[i];
                if (canGo(rowVal, colVal, h, w)) makeEdge(nodeNum[r][c], nodeNum[rowVal][colVal]);
            }
        }
    }

    static boolean canAugment(int node) {
        if (visited[node]) return false;
        visited[node] = true;
        int neighbours = neighboursNum[node];
        for (int i = 0 ; i < neighbours; ++i) {
            int neighbour = adjList[node][i];
            if (owner[neighbour] == -1 || canAugment(owner[neighbour])) {
                owner[neighbour] = node;
                return true;
            }
        }
        return false;
    }

    static int MCBM () {
        Arrays.fill(owner, -1);
        int pairs = 0;
        for (int i = 0; i < leftNodesIndex; ++i) {
            Arrays.fill(visited, false);
            if (canAugment(leftNodes[i])) pairs++;
        }
        return pairs;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(rd.readLine());

        while (n-- > 0) {
            init();
            int h, w;
            String[] inp = rd.readLine().split(" ");
            h = Integer.parseInt(inp[0]);
            w = Integer.parseInt(inp[1]);

            for (int i = 0 ; i < h; ++i) {
                String input = rd.readLine();
                for (int j = 0 ; j < input.length(); ++j) {
                    grid[i][j] = input.charAt(j);
                }
            }

            makeGraph(h, w);

           int pairs = MCBM();

           System.out.println(pairs + nodeIndex - (pairs << 1));
        }
    }
}
