package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.Arrays;

public class Main {

    static int SIZE = 30;
    static int cost[] = new int[SIZE];
    static int[] longestPath = new int[SIZE];
    static int [][] adjList = new int[SIZE][SIZE];
    static int[] neighboursNum = new int[SIZE];
    static boolean visited[] = new boolean[SIZE];
    static int topSortArr[] = new int[SIZE];
    static int topSortIndex;

    static void init () {
        Arrays.fill(cost, -1);
        Arrays.fill(neighboursNum, 0);
    }

    static void makeEdge (char n1, char n2) {
        adjList[n1 - 'A'][neighboursNum[n1 - 'A']++] = (n2 - 'A');
    }

    static void dfs(int node) {
        visited[node] = true;
        int numOfNeigb = neighboursNum[node];
        for (int i = 0; i < numOfNeigb; ++i) {
            int neighbour = adjList[node][i];
            if (!visited[neighbour]) dfs(neighbour);
        }
        topSortArr[topSortIndex++] = node;
    }

    static int topSort() {
        int longestPathLength =Integer.MIN_VALUE;
        Arrays.fill(visited, false);
        topSortIndex = 0;
        for (int i = 0 ; i < SIZE; ++i) if (cost[i] != -1 && !visited[i]) dfs(i);

        Arrays.fill(longestPath, 0);
        for (int i = 0 ; i < SIZE; ++i) if (cost[i] != -1) longestPath[i] = cost[i];

        for (int i = topSortIndex - 1; i >= 0; --i) {
            int node = topSortArr[i];
            longestPathLength = Math.max(longestPathLength, longestPath[node]);
            int numOfNeigh = neighboursNum[node];
            for (int n = 0 ; n < numOfNeigh; ++n) {
                int neighbour = adjList[node][n];
                longestPath[neighbour] = Math.max(longestPath[neighbour], longestPath[node] + cost[neighbour]);
            }
        }
        return longestPathLength;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        int tc;
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        tc = Integer.parseInt(rd.readLine());
        rd.readLine();
        int caseVal = 1;
        while (tc-- > 0) {
            init();
            while (true) {
                String inpString = rd.readLine();
                if (inpString == null) break;
                if (inpString.trim().length() == 0) break;

                String inp[] = inpString.split(" ");
                char node = inp[0].charAt(0);
                int days = Integer.parseInt(inp[1]);
                cost[node - 'A'] = days;
                if (inp.length > 2) {
                    String neighbours = inp[2];
                    for (int i = 0 ; i < neighbours.length(); ++i) {
                        char neighbour = neighbours.charAt(i);
                        makeEdge(node, neighbour);
                    }
                }
            }
            int ans = topSort();
            if (caseVal++ != 1) System.out.println();
            System.out.println(ans);
        }
    }
}
