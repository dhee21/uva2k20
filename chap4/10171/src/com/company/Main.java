package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

    static int [][] oldDist = new int[30][30];
    static int [][] newDist = new int[30][30];
    static int INFINITY = Integer.MAX_VALUE;
    static int M, N = 26;

    static void init () {
        for (int[] arr: oldDist) Arrays.fill(arr, INFINITY);
        for (int[] arr: newDist) Arrays.fill(arr, INFINITY);

    }

    static void floydWarshal (int [][] dist) {
        for (int i = 0 ; i < N; ++i) dist[i][i] = 0;

        for (int k = 0 ; k < N; ++k) for (int i = 0 ; i < N; ++i) for (int j = 0 ; j < N; ++j) {
            if (dist[i][k] != INFINITY && dist[k][j] != INFINITY) {
                dist[i][j] = Math.min(dist[i][k] + dist[k][j], dist[i][j]);
            }
        }
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        boolean[] val = new boolean[26];
        while (true) {
            M = Integer.parseInt(rd.readLine());
            if (M == 0) break;
            init();
            for (int i = 0 ; i < M; ++i) {
                String[] inp = rd.readLine().split(" ");
                boolean isYoung = inp[0].charAt(0) == 'Y';
                boolean isUniDirectional = inp[1].charAt(0) == 'U';
                int a = inp[2].charAt(0) - 'A';
                int b = inp[3].charAt(0) - 'A';
                int weight = Integer.parseInt(inp[4]);
                if (isYoung) {
                    newDist[a][b] = weight;
                    if (!isUniDirectional) newDist[b][a] = weight;
                } else {
                    oldDist[a][b] = weight;
                    if (!isUniDirectional) oldDist[b][a] = weight;
                }
            }

            String[] inp = rd.readLine().split(" ");
            int me = inp[0].charAt(0) - 'A';
            int prof = inp[1].charAt(0) - 'A';


            floydWarshal(oldDist);
            floydWarshal(newDist);

            int minVal = INFINITY;

            Arrays.fill(val, false);
            for (int k = 0 ; k < N; ++k) {
                if (newDist[me][k] != INFINITY && oldDist[prof][k] != INFINITY) {
                    minVal = Math.min(minVal, newDist[me][k] + oldDist[prof][k]);
                }
            }

            for (int k = 0 ; k < N; ++k) {
                if (newDist[me][k] != INFINITY && oldDist[prof][k] != INFINITY) {
                    if ( minVal == newDist[me][k] + oldDist[prof][k] ) {
                        val[k] = true;
                    }
                }
            }


            if (minVal == INFINITY) System.out.println("You will never meet.");
            else {
                System.out.printf("%d", minVal);
                for (int i = 0 ; i < N; ++i) if (val[i]) {
                    System.out.printf(" %c", ('A' + i));
                }
                System.out.println();
            }

        }
    }
}
