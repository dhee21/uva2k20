package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.PriorityQueue;

public class Main {
    static class Pair<A, B> {
        A first;
        B second;
        Pair(A a, B b) {
            first = a; second = b;
        }
    }

    static int SIZE = 110;
    static long [][] capacity = new long[SIZE][SIZE], flow = new long[SIZE][SIZE];
    static double [][] cost = new double[SIZE][SIZE];
    static int [][] adjListResGraph = new int[SIZE][SIZE];
    static int[] neighboursNum = new int[SIZE];
    static double[] dist = new double[SIZE];
    static boolean[] visited = new boolean[SIZE];
    static int[] bfsParent = new int[SIZE];
    static int INFINITY = Integer.MAX_VALUE;
    static double minCost;

    static void makeEdge (int from, int to, long cap, double costVal) {
        cost[from][to] = costVal;
        cost[to][from] = costVal;
        capacity[from][to] = cap;
        if (capacity[to][from] == 0) {
            adjListResGraph[from][neighboursNum[from]++] = to;
            adjListResGraph[to][neighboursNum[to]++] = from;
        }
    }

    static void init() {
        for (long[] arr: capacity) Arrays.fill(arr, 0);
        for (long[] arr: flow) Arrays.fill(arr, 0);
        Arrays.fill(neighboursNum, 0);
        minCost = 0;
    }

    static long resCapacity(int from, int to) {
        return capacity[from][to] - flow[from][to];
    }

    static double findCost(int from, int to) {
        return (flow[to][from] > 0 ? -cost[from][to] : cost[from][to]);
    }

    static boolean djakstra(int s, int t) {
        Arrays.fill(dist, Integer.MAX_VALUE);
        Arrays.fill(bfsParent, -1);
        PriorityQueue<Pair<Double, Integer>> pq = new PriorityQueue<>((a, b) -> a.first <= b.first ? -1 : 1);

        dist[s] = 0;
        pq.add(new Pair<>(0.0, s));

        while (!pq.isEmpty()) {
            Pair<Double, Integer> nodeData = pq.poll();
            int node = nodeData.second;
            double nodeDist = nodeData.first;
            if (Math.abs(nodeDist - dist[node]) < 0.00001) {
                if (node == t) return true;
                int neighbourNum = neighboursNum[node];
                for (int i = 0 ; i < neighbourNum; ++i) {
                    int neighbour = adjListResGraph[node][i];
                    long resCap = resCapacity(node, neighbour);
                    if (resCap > 0 && (dist[node] + findCost(node, neighbour) < dist[neighbour])) {
                        dist[neighbour] = dist[node] + findCost(node, neighbour);
                        bfsParent[neighbour] = node;
                        pq.add(new Pair<>(dist[neighbour], neighbour));
                    }
                }
            }
        }
        return false;
    }

    static long augmentFlow(int s, int t) {
        int node = t;
        long minResidualCap = Long.MAX_VALUE;
        while (bfsParent[node] != -1) {
            int parent = bfsParent[node];
            minResidualCap = Math.min(minResidualCap, resCapacity(parent, node));
            node = parent;
        }
        node = t;
        while (bfsParent[node] != -1) {
            int parent = bfsParent[node];
            flow[parent][node] += minResidualCap;
            flow[node][parent] -= minResidualCap;
            node = parent;
        }
        minCost += minResidualCap * dist[t];
        return  minResidualCap;
    }

    static long runMaxFlow(int s, int t) {
        long flow = 0;
        while (djakstra(s, t)) {
            flow += augmentFlow(s, t);
        }
        return flow;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            int n, m;
            String inp[] = rd.readLine().split(" ");
            n = Integer.parseInt(inp[0]);
            m = Integer.parseInt(inp[1]);
            if (n == 0 && m == 0) break;
            init();
            int SOURCE = 0;
            int SINK = n + m + 1;

            for (int node = 1; node <= m; ++node) makeEdge(SOURCE, node, 1, 0);
            for (int node = 1; node <= n; ++node) makeEdge(node + m, SINK, 1, 0);


            for (int to = 1 ; to <= n; ++to) {
                inp = rd.readLine().split(" ");
                for (int from = 1; from <= m; ++from) {
                    makeEdge(from, to + m,1, Double.parseDouble(inp[from - 1]));
                }
            }

            runMaxFlow(SOURCE, SINK);
            minCost = minCost / n;
            minCost = Math.floor(minCost*100.0 + 0.5 + 1e-9)/100.0;
            System.out.printf("%.2f\n", minCost );
        }
    }
}
