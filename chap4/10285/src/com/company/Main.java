package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.Arrays;

public class Main {

    static int SIZE = 110;
    static int[][] grid = new int[SIZE][SIZE];
    static boolean[][] visited = new boolean[SIZE][SIZE];
    static int topSortIndex;
    static int[][] topSortArr = new int[SIZE * SIZE][2];
    static int[][] longestPath = new int[SIZE][SIZE];
    static int rows, cols;

    static int rowN[] = {1, -1, 0, 0};
    static int colN[] = {0, 0, -1, 1};

    static boolean canGo (int r, int c) {
        return r >= 0 && r < rows && c >= 0 && c < cols;
    }

    static void dfs(int r, int c) {
        visited[r][c] = true;

        for (int i = 0 ; i < 4; ++i) {
            int rowToGo = r + rowN[i];
            int colToGo = c + colN[i];
            if (canGo(rowToGo, colToGo) && grid[rowToGo][colToGo] < grid[r][c] && !visited[rowToGo][colToGo]) {
                dfs(rowToGo, colToGo);
            }
        }
        topSortArr[topSortIndex++] = new int[]{r, c};
    }

    static int topSort () {
        topSortIndex = 0;
        for (boolean [] arr: visited )Arrays.fill(arr, false);

        for (int r = 0 ; r < rows; ++r) {
            for (int c = 0; c < cols; ++c) {
                if (!visited[r][c]) dfs(r, c);
            }
        }

        for (int [] arr: longestPath )Arrays.fill(arr, 1);
        int longestPathLength = Integer.MIN_VALUE;
        for (int i = topSortIndex - 1; i >= 0; --i) {
            int r = topSortArr[i][0], c = topSortArr[i][1];
            longestPathLength = Math.max(longestPathLength, longestPath[r][c]);
            for (int n = 0 ; n < 4; ++n) {
                int rowToGo = r + rowN[n];
                int colToGo = c + colN[n];
                if (canGo(rowToGo, colToGo) && grid[rowToGo][colToGo] < grid[r][c]) {
                    longestPath[rowToGo][colToGo] = Math.max(longestPath[rowToGo][colToGo], longestPath[r][c] + 1);
                }
            }
        }

        return longestPathLength;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(rd.readLine());

        while (tc -- > 0) {
            String inp[] = rd.readLine().trim().split(" ");
            String area = inp[0];
            rows = Integer.parseInt(inp[1]);
            cols = Integer.parseInt(inp[2]);

            for (int i = 0 ; i < rows; ++i) {
                inp = rd.readLine().split(" ");
                for (int c = 0 ; c < cols; ++c) grid[i][c] = Integer.parseInt(inp[c]);
            }

            int ans = topSort();
            System.out.println(area + ": " + ans);
        }
    }
}
