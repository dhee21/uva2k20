package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    static int [][] dist = new int[110][110];
    static int INFINITY = Integer.MAX_VALUE;
    static int M, N;

    static void init () {
        for (int[] arr: dist) Arrays.fill(arr, INFINITY);
    }

    static void floydWarshal () {
        for (int i = 0 ; i < N; ++i) dist[i][i] = 0;

        for (int k = 0 ; k < N; ++k) for (int i = 0 ; i < N; ++i) for (int j = 0 ; j < N; ++j) {
            if (dist[i][k] != INFINITY && dist[k][j] != INFINITY) {
                dist[i][j] = Math.min(dist[i][k] + dist[k][j], dist[i][j]);
            }
        }
    }
    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = sc.nextInt();
        int caseVal = 1;
        while (tc -- > 0) {
            init();
            N = sc.nextInt();
            M = sc.nextInt();
            for (int i = 0 ; i < M; ++i) {
                int from = sc.nextInt();
                int to = sc.nextInt();
                dist[from][to] = 1;
                dist[to][from] = 1;
            }
            floydWarshal();
            int s = sc.nextInt(), d = sc.nextInt();
            int ans  = -1;
            for (int v = 0 ; v < N; ++v) {
                if (dist[s][v] != INFINITY && dist[v][d] != INFINITY) {
                    ans = Math.max(ans, dist[s][v] + dist[v][d]);
                }
            }
            System.out.printf("Case %d: %d\n", caseVal++, ans);
        }
    }
}
