package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;

public class Main {

    static int MAXNODES = 2200;
    static int adjList[][][] = new int[MAXNODES][MAXNODES][3];
    static int neighbourNodes[] = new int[MAXNODES];
    static int[] degree = new int[MAXNODES];
    static boolean[] visited = new boolean[MAXNODES], nodeThere = new boolean[MAXNODES];
    static int maxNodeVal;
    static ArrayList<int[]> tour = new ArrayList<>();

    static void init () {
        Arrays.fill(neighbourNodes, 0);
        Arrays.fill(visited, false);
        Arrays.fill(degree, 0);
        Arrays.fill(nodeThere, false);
        maxNodeVal = -1;
    }

    static void makeEdge(int n1, int n2) {
        visited[n1] = false;
        visited[n2] = false;
        nodeThere[n1] = true;
        nodeThere[n2] = true;
        int index1 = neighbourNodes[n1]++;
        int index2 = neighbourNodes[n2]++;
        adjList[n1][index1] = new int[] {n2, 1, index2};
        adjList[n2][index2] = new int[] {n1, 1, index1};
        degree[n1]++;
        degree[n2]++;
    }

    static void eulerTour (int v) {
        visited[v] = true;
        for (int i = 0 ; i < neighbourNodes[v]; ++i) {
            int neighbour = adjList[v][i][0];
            int isEdgeVisited = adjList[v][i][1];
            int indexReverseOfSameEdge = adjList[v][i][2];
            if (isEdgeVisited == 1) {
                adjList[v][i][1]--;
                adjList[neighbour][indexReverseOfSameEdge][1]--;
                eulerTour(neighbour);
                tour.add(new int[] {neighbour, v});
            }
        }
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(rd.readLine());

        int caseVal = 1;
        while (tc -- > 0) {
            init();
            int n = Integer.parseInt(rd.readLine());
            for (int i = 0 ; i < n ; ++i) {
                String[] inp = rd.readLine().split(" ");
                int node1 = Integer.parseInt(inp[0]);
                int node2 = Integer.parseInt(inp[1]);
                makeEdge(node1, node2);
                maxNodeVal = Math.max(maxNodeVal, Math.max(node1, node2));
            }

            boolean areAllDegreesEven = true;
            for (int i = 0 ; i <= maxNodeVal; ++i) {
                if (degree[i] % 2 == 1) {
                    areAllDegreesEven = false; break;
                }
            }
            if (caseVal > 1) System.out.println();
            System.out.println("Case #" + caseVal++);

            if (!areAllDegreesEven) System.out.println("some beads may be lost");
            else {
                tour.clear();
                eulerTour(maxNodeVal);
                boolean ansThere = true;
                for (int i = 0 ; i <= maxNodeVal; ++i) if (nodeThere[i] && !visited[i]) {
                    ansThere = false; break;
                }

                if (ansThere) for (int i = 0 ; i < tour.size(); ++i) System.out.println(tour.get(i)[0] + " " +  tour.get(i)[1]);
                else System.out.println("some beads may be lost");
            }
        }

    }
}
