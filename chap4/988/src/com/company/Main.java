package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;

public class Main {

    static int SIZE = 1000;
    static int [][] adjList = new int[SIZE][SIZE];
    static int [] neighbourNum = new int[SIZE];
    static boolean [] visited = new boolean[SIZE];
    static int[] topSort = new int[SIZE];
    static int[] path = new int[SIZE];
    static int topSortIndex;

    static void init () {
        Arrays.fill(neighbourNum, 0);
        topSortIndex = 0;
    }

    static void  makeEdge (int n1, int n2) {
        adjList[n1][neighbourNum[n1]++] = n2;
    }

    static void dfs(int n) {
        visited[n] = true;
        int numOfNeigh = neighbourNum[n];
        for (int i = 0 ; i < numOfNeigh; ++i) {
            int neighbour = adjList[n][i];
            if (!visited[neighbour]) dfs(neighbour);
        }
        topSort[topSortIndex++] = n;
    }

    static int topoSort (int n) {
        int ans = 0;
        Arrays.fill(visited, false);

        for (int i = 0 ; i < n; ++i) if (!visited[i]) dfs(i);

        Arrays.fill(path, 0);
        path[0] = 1;

        for (int i = topSortIndex - 1; i >= 0; --i) {
            int node = topSort[i];

            int numOfNeigh = neighbourNum[node];

            if (numOfNeigh == 0) ans += path[node];

            for (int x = 0 ; x < numOfNeigh; ++x) {
                int neighbour = adjList[node][x];
                path[neighbour] += path[node];
            }
        }
        return ans;
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int caseVal = 1;
        while (sc.hasNext()) {
            init();
            int events = sc.nextInt();

            for (int i = 0; i < events; ++i) {
                int afterEvents = sc.nextInt();

                for (int j = 0; j < afterEvents; ++j) {
                    makeEdge(i, sc.nextInt());
                }
            }
            int ans = topoSort(events);
            if (caseVal++ > 1) System.out.println();
            System.out.println(ans);
            if (sc.hasNext()) sc.nextLine();
        }
    }
}
