package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

    static int N, M;
    static int SIZE = 1100;
    static int INFINITY = Integer.MAX_VALUE;
    static int[][][] adjList = new int[SIZE][SIZE][2];
    static int[] neighboursNumOfNodes = new int[SIZE];
    static boolean hasNegativeCycle;

    static int[] dist = new int[SIZE];
    static void init () {
        Arrays.fill(neighboursNumOfNodes, 0);
    }

    static void BellMenFord (int source) {
        Arrays.fill(dist, INFINITY);
        dist[source] = 0;

        hasNegativeCycle = false;

        for (int times = 0; times < N; ++times) {
            for (int node = 0 ; node < N; ++node) if (dist[node] != INFINITY) {
                for (int i = 0; i < neighboursNumOfNodes[node]; ++i) {
                    int neighbourNode = adjList[node][i][0], weight = adjList[node][i][1];
                    if (dist[node] + weight < dist[neighbourNode]) {
                        dist[neighbourNode] = dist[node] + weight;
                        if (times == N - 1) hasNegativeCycle = true;
                    }
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(rd.readLine());
        while (tc -- > 0) {
            init();
            String[] inp = rd.readLine().split(" ");
            N = Integer.parseInt(inp[0]);
            M = Integer.parseInt(inp[1]);
            for (int i = 0 ; i < M; ++i) {
                inp = rd.readLine().split(" ");
                int from = Integer.parseInt(inp[0]), to = Integer.parseInt(inp[1]), weight = Integer.parseInt(inp[2]);
                adjList[from][neighboursNumOfNodes[from]++] = new int[] {to, weight};
            }
            BellMenFord(0);
            if (hasNegativeCycle) System.out.println("possible");
            else System.out.println("not possible");
        }
    }
}
