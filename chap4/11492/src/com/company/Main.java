package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;

public class Main {

    static class Pair<A, B> {
        A first;
        B second;
        Pair(A a, B b) {
            first = a; second = b;
        }

        public boolean equals (Object right) {
            Pair<A, B> rightNew = (Pair) right;
            return this.first.equals(rightNew.first) && this.second.equals(rightNew.second);
        }

        public int hashCode() {
            return first.hashCode();
        }
    }

    static int INF = Integer.MAX_VALUE;
    static HashMap<String, String[]> wordToLang;
    static HashMap<String, ArrayList<String>> langToWords;
    static HashMap<Pair<String, String>, Integer> dist;

    static int djakstra (String sourceLang, String destLang) {

        ArrayList<String> listOfWordsOfStartLang = langToWords.get(sourceLang);
        PriorityQueue<Pair<Integer, Pair<String, String>>> pq = new PriorityQueue<>((a, b) -> a.first - b.first);

        for (String wordOfStartLang : listOfWordsOfStartLang) {
            Pair<String, String> node = new Pair<>(wordOfStartLang, sourceLang);
            dist.put(node,  wordOfStartLang.length());
            pq.add(new Pair<>(dist.get(node), node));
        }

        int ans = INF;
        while (!pq.isEmpty()) {
            Pair<Integer, Pair<String, String>> topObject = pq.poll();
            int distance = topObject.first; Pair<String, String> node = topObject.second;
            String word = node.first, lang = node.second;
            if (distance == dist.get(node)) {
                if (lang.equals(destLang)) ans = Math.min(ans, distance);
                String secondLang = wordToLang.get(word)[0].equals(lang)
                        ? wordToLang.get(word)[1] : wordToLang.get(word)[0];

                Pair<String, String> neighbourNodeWithSameWord = new Pair<>(word, secondLang);
                int newDistForThisNode = distance;
                if (!secondLang.equals(lang) && newDistForThisNode < dist.get(neighbourNodeWithSameWord)) {
                    dist.put(neighbourNodeWithSameWord, newDistForThisNode);
                    pq.add(new Pair<>(newDistForThisNode, neighbourNodeWithSameWord));
                }

                ArrayList<String> neibourWords = langToWords.get(lang);
                for (String oneNeighbourWord: neibourWords) if (oneNeighbourWord.charAt(0) != word.charAt(0)) {
                    Pair<String, String> neighbourNode = new Pair<>(oneNeighbourWord, lang);
                    int newDist = distance + oneNeighbourWord.length();
                    if (newDist < dist.get(neighbourNode)) {
                        dist.put(neighbourNode, newDist);
                        pq.add(new Pair<>(newDist, neighbourNode));
                    }
                }
            }
        }
        return ans;
    }

    public static void main(String[] args) throws IOException {

//        System.out.println(new Pair<>("sss", 3).equals( new Pair<>("sss", 1)));
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            int M = Integer.parseInt(rd.readLine());
            if (M == 0) break;
            String[] wordsofAnswer = rd.readLine().split(" ");
            String startLang = wordsofAnswer[0], endLang = wordsofAnswer[1];

            if (wordToLang != null) wordToLang.clear(); else wordToLang = new HashMap<>();
            if (langToWords != null) langToWords.clear(); else langToWords = new HashMap<>();
            if (dist != null) dist.clear(); else dist = new HashMap<>();

            for (int i = 0 ; i < M; ++i) {
                String[] edge = rd.readLine().split(" ");
                String lang1 = edge[0], lang2 = edge[1];
                String word = edge[2];
                wordToLang.put(word, new String[] {lang1, lang2});
                if (!langToWords.containsKey(lang1)) langToWords.put(lang1, new ArrayList<>());
                if (!langToWords.containsKey(lang2)) langToWords.put(lang2, new ArrayList<>());
                langToWords.get(lang1).add(word);
                if (!lang1.equals(lang2)) langToWords.get(lang2).add(word);
                dist.put(new Pair<>(word, lang1), INF);
                if (!lang1.equals(lang2)) dist.put(new Pair<>(word, lang2), INF);
            }

            if (!langToWords.containsKey(startLang) || !langToWords.containsKey(endLang) || startLang.equals(endLang)) {
                System.out.println("impossivel");
                continue;
            }
            int ans = djakstra(startLang, endLang);
            if (ans == INF) {
                System.out.println("impossivel");
            } else {
                System.out.println(ans);
            }
        }

    }
}
