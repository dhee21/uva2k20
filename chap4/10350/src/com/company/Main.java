package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.Arrays;

public class Main {

    static int [][][] time = new int[125][20][20];
    static int[] holes = new int[20];
    static int[] ladders = new int[20];

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String caseName = rd.readLine();
            if (caseName == null) break;

            String[] inp = rd.readLine().split(" ");
            int n = Integer.parseInt(inp[0]), m = Integer.parseInt(inp[1]);

            for (int floor = 1; floor <= n - 1; ++floor) {
                for (int hole = 1; hole <= m ; ++hole) {
                    inp = rd.readLine().split(" ");
                    for (int ladder = 1; ladder <= m; ++ladder) {
                        time[floor][hole][ladder] = Integer.parseInt(inp[ladder - 1]);
                    }
                }
            }

            Arrays.fill(holes, 0);

            for (int floor = 1; floor <= n - 1; ++floor) {
                Arrays.fill(ladders, Integer.MAX_VALUE);

                for (int hole = 1; hole <= m; ++hole) {
                    for (int ladder = 1; ladder <= m; ++ladder) {
                        ladders[ladder] = Math.min(ladders[ladder], holes[hole] + time[floor][hole][ladder]);
                    }
                }

                for (int ladder = 1; ladder <= m; ++ladder) {
                    holes[ladder] = ladders[ladder] + 2;
                }
            }

            int ans = Integer.MAX_VALUE;
            for (int i = 1 ; i <= m; ++i) {
                ans = Math.min(ans, holes[i]);
            }

            System.out.println(caseName.trim());
            System.out.println(ans);

        }
    }
}
