package com.company;

        import java.util.Arrays;
        import java.util.Scanner;

public class Main {

    static int SIZE = 1100;
    static int[][] adjList = new int[SIZE][SIZE];
    static int[] neighbours = new int[SIZE];
    static int[][] dp = new int[SIZE][SIZE];

    static void makeEdge(int n1, int n2) {
        adjList[n1][neighbours[n1]++] = n2;
    }


    static int mvc (int node, boolean taken, int parent) {
        int toReturn = taken ? 1 : 0;
        int index = taken ? 1 : 0;
        if (dp[node][index] != -1) return dp[node][index];
        if (!taken) {
            int numOfNeigh = neighbours[node];
            for (int i = 0 ; i < numOfNeigh; ++i) {
                int neibh = adjList[node][i];
                if (neibh != parent) toReturn += mvc(neibh, true, node);
            }
        } else {
            int numOfNeigh = neighbours[node];
            for (int i = 0 ; i < numOfNeigh; ++i) {
                int neibh = adjList[node][i];
                if (neibh != parent) toReturn += Math.min(mvc(neibh, true, node), mvc(neibh, false, node));
            }
        }
        dp[node][index] = toReturn;
        return dp[node][index];
    }

    public static void main(String[] args) {
        // write your code here
        int n;
        Scanner sc = new Scanner(System.in);
        while (true) {
            n = sc.nextInt();
            if (n == 0) break;
            for (int[] arr: dp) Arrays.fill(arr, -1);
            Arrays.fill(neighbours, 0);
            for (int node = 0; node < n; ++node) {
                int nbh = sc.nextInt();
                for (int i = 0 ; i < nbh; ++i) {
                    int nbhNode = sc.nextInt();
                    makeEdge(node, nbhNode - 1);
                }
            }
            int ans;
            if (n == 1) ans = 1;
            else ans = Math.min(mvc(0, false, -1), mvc(0, true, -1));
            System.out.println(ans);
        }
    }
}
