package com.company;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Main {

    static boolean isBipartite (boolean [][] adjMat, boolean[] visited, int[] level, int n) {
        Queue<Integer> q = new LinkedList<>();
        int nodeToStartBFS = 0;

        visited[nodeToStartBFS] = true;
        level[nodeToStartBFS] = 0;
        q.add(nodeToStartBFS);


        boolean isBip = true;
        while (!q.isEmpty()) {
            int node = q.poll();
            for (int i = 0 ; i < n ; ++i) {
                if (adjMat[node][i] && !visited[i]) {
                    visited[i] = true;
                    level[i] = level[node] + 1;
                    q.add(i);
                } else if (adjMat[node][i] && visited[i]) {
                    if (level[i] == level[node]) {
                        isBip = false;
                        break;
                    }
                }
            }
            if (!isBip) break;
        }
        return isBip;

    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        boolean [][] adjMat = new boolean[210][210];
        int [] level = new int[210];
        boolean visited[] = new boolean[210];
        while (true) {
            int n = sc.nextInt();
            if (n == 0) break;
            for (boolean[] arr: adjMat) Arrays.fill(arr, false);
            Arrays.fill(visited, false);
            Arrays.fill(level, -1);
            int m = sc.nextInt();

            for (int i = 0 ; i < m; ++i) {
                int a = sc.nextInt(), b = sc.nextInt();
                adjMat[a][b] = true;
                adjMat[b][a] = true;
            }

            boolean isBipartite = isBipartite(adjMat, visited, level, n);
            if (isBipartite) System.out.println("BICOLORABLE.");
            else System.out.println("NOT BICOLORABLE.");
        }
    }
}
