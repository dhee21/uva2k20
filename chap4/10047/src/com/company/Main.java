package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;

public class Main {

    static class Pair<A, B> {
        A first;
        B second;
        Pair(A a, B b) {
            first = a; second = b;
        }
    }

    // N, E, S, W
    static int NORTH = 0, EAST = 1, SOUTH = 2, WEST = 3;
    static int GREEN = 0, BLACK = 1, RED = 2, BLUE = 3, WHITE = 4;
    static int[] direction = {NORTH, EAST, SOUTH, WEST};
    static int m, n;
    static int[] colorValues = {GREEN, BLACK, RED, BLUE, WHITE};


    static int bitForRowOrCol = 5, bitForDir = 2, bitFoColor = 3;
    static int giveState (int row, int col , int dir, int color) {
        return (((((row << bitForRowOrCol) + col) << bitForDir) + dir) << bitFoColor) + color;
    }

    static int findBitValBetween(int from, int to, int state) {
        int bitlength = to - from + 1;
        int bitmaskOfThisLength = (1 << bitlength) - 1;
        int maskShifted = bitmaskOfThisLength << from;
        int valExtracted = state & maskShifted;
        return (valExtracted >> from);
    }

    static int[] findValuesFromState (int state) {
        int from = 0, to = bitFoColor - 1;
        int color = findBitValBetween(from, to, state);
        int dir = findBitValBetween(bitFoColor, bitFoColor + bitForDir - 1, state);
        int col = findBitValBetween(bitFoColor + bitForDir, bitFoColor + bitForDir + bitForRowOrCol - 1, state);
        int row = findBitValBetween(bitFoColor + bitForDir + bitForRowOrCol,
                bitFoColor + bitForDir + bitForRowOrCol + bitForRowOrCol - 1, state);
        int [] arr = {color, dir, col, row};
        return arr;
    }

    static int moveRight(int state) {
        Integer row , col , dir , color ;
        int [] val = findValuesFromState(state);
        color = val[0]; dir = val[1]; col = val[2]; row = val[3];
        dir = (dir + 1) % 4;
        return giveState(row, col, dir, color);
    }

    static int moveLeft(int state) {
        Integer row , col , dir , color ;
        int [] val = findValuesFromState(state);
        color = val[0]; dir = val[1]; col = val[2]; row = val[3];
        dir = dir - 1;
        if (dir == -1) dir = WEST;
        return giveState(row, col, dir, color);
    }

    static int moveAhead(int state, char[][] grid) {
        Integer row , col , dir , color ;
        int [] val = findValuesFromState(state);
        color = val[0]; dir = val[1]; col = val[2]; row = val[3];
        if (dir == NORTH) row--;
        else if (dir == SOUTH) row ++;
        else if (dir == EAST) col++;
        else if (dir == WEST) col--;

        color = (color + 1) % 5;

        if (row >= 0 && row < m && col >= 0 && col < n && grid[row][col] != '#') {
            return giveState(row, col, dir, color);
        }
        return -1;
    }

    static boolean isVisited(int state , int [] visited) {
        return visited[state] != -1;
    }

    public static void main(String[] args) throws IOException {
	    // write your code here
        char[][] grid = new char[30][30];
        int[] visited = new int[50000];
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        int caseVal = 1;
        while (true) {
            String[] inp = rd.readLine().split(" ");
            m = Integer.parseInt(inp[0]);
            n = Integer.parseInt(inp[1]);
            if (m == 0 && n == 0) break;
            Arrays.fill(visited, -1);

            int stRow = 0, stCol = 0, edRow = 0, edCol = 0;
            for (int r = 0; r < m; ++r) {
                String row = rd.readLine();
                for (int c = 0 ; c < n; ++c) {
                    if (row.charAt(c) == 'S') {
                        stRow = r; stCol = c;
                    } else if (row.charAt(c) == 'T') {
                        edRow = r; edCol = c;
                    }
                    if (row.charAt(c) != '#') grid[r][c] = '.';
                    else grid[r][c] = '#';
                }
            }

            int startState = giveState(stRow, stCol, NORTH, GREEN);
            HashSet<Integer> endState = new HashSet<>(Arrays.asList(
                    giveState(edRow, edCol, NORTH , GREEN),
                    giveState(edRow, edCol, SOUTH , GREEN),
                    giveState(edRow, edCol, EAST , GREEN),
                    giveState(edRow, edCol, WEST , GREEN)
            )) ;

            Queue<Integer> q = new LinkedList<>();
            q.add(startState);
            visited[startState] = 0;

            int ans = -1;
            while (!q.isEmpty()) {
                int currentState = q.poll();
                int currentLevel = visited[currentState];

                if (endState.contains(currentState)) {
                    ans = currentLevel; break;
                }

                int moveRightState = moveRight(currentState);
                int moveLeftState = moveLeft(currentState);
                int moveAheadState = moveAhead(currentState, grid);

                if (!isVisited(moveRightState, visited)) {
                    visited[moveRightState] = currentLevel + 1;
                    q.add(moveRightState);
                }
                if (!isVisited(moveLeftState, visited)) {
                    visited[moveLeftState] = currentLevel + 1;
                    q.add(moveLeftState);
                }
                if (moveAheadState != -1 && !isVisited(moveAheadState, visited)) {
                    visited[moveAheadState] = currentLevel + 1;
                    q.add(moveAheadState);
                }
            }

            if (caseVal > 1) System.out.println();
            System.out.printf("Case #%d\n", caseVal++);
            if (ans == -1){
                System.out.println("destination not reachable");
            } else {
                System.out.printf("minimum time = %d sec\n", ans);
            }

        }
    }
}
