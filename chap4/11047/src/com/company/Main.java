package com.company;

import org.omg.CORBA.INTERNAL;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;

public class Main {
    static int N, SIZE = 110, INF = Integer.MAX_VALUE;
    static int dist[][] = new int[SIZE][SIZE];
    static int p[][] = new int[SIZE][SIZE];
    static HashMap<String, Integer> indexOf = new HashMap<>();
    static int [] ans = new int[SIZE];

    static void init() {
        for (int[] arr: dist) Arrays.fill(arr, INF);
        for (int[] arr: p) Arrays.fill(arr, -1);
        if (indexOf != null) indexOf.clear(); else indexOf = new HashMap<>();
    }

    static void floyd() {
        for (int k = 0 ; k < N; ++k) for (int i = 0 ; i < N; ++i) for (int j = 0 ; j < N; ++j) {
            if (dist[i][k] != INF && dist[k][j] != INF) {
                if (dist[i][k] + dist[k][j] < dist[i][j]) {
                    dist[i][j] = dist[i][k] + dist[k][j];
                    p[i][j] = p[k][j];
                }
            }
        }
    }

    static void printPath (int from, int to, String[] nodes) {
        System.out.printf("Path:");

//        System.out.println("from = " + from);
//        System.out.println("to = " + to);
        int index = 0;
        int node = to;
        ans[index++] = node;
        node = p[from][node];
        while (node != from) {
            ans[index++] = node;
            node = p[from][node];
        }
        ans[index++] = node;

        for (int i = index - 1; i >= 0; --i) {
            if (i == (index - 1)) System.out.printf(nodes[ans[i]]);
            else System.out.printf(" " + nodes[ans[i]]);
        }
        System.out.printf("\n");
    }


    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(rd.readLine().trim());

        while (tc -- > 0) {
            N = Integer.parseInt(rd.readLine().trim());
            init();
            String nodes[] = rd.readLine().trim().split("\\s+");
            for (int i = 0 ; i < nodes.length; ++i) indexOf.put(nodes[i], i);

            for (int i = 0 ; i  < N; ++i) {
                String[] inp = rd.readLine().trim().split("\\s+");
                for (int j = 0 ; j < N; ++j) {
                    int val = Integer.parseInt(inp[j]);
                    if (val != -1) {
                        dist[i][j] = val;
                        p[i][j] = i;
                    }
                }
            }
            floyd();

            int R = Integer.parseInt(rd.readLine().trim());

            for (int i = 0; i < R; ++i) {
                String[] inp = rd.readLine().trim().split("\\s+");
                String emp = inp[0];
                String from = inp[1]; String to = inp[2];
                int fromNum = indexOf.get(from), toNum  =indexOf.get(to);
                int len = dist[fromNum][toNum];
                if (len != INF) {
                    System.out.printf("Mr %s to go from %s to %s, you will receive %d euros\n", emp, from, to, len);
                    printPath(indexOf.get(from), indexOf.get(to), nodes);
                } else {
                    System.out.printf("Sorry Mr %s you can not go from %s to %s\n",
                            emp, from, to);
                }
            }
        }
    }
}
