package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class Main {

    static int SIZE = 110;
    static int [][] capacity = new int[SIZE][SIZE], flow = new int[SIZE][SIZE];
    static int [][] adjListResGraph = new int[SIZE][SIZE];
    static int[] neighboursNum = new int[SIZE];
    static boolean[] visited = new boolean[SIZE];
    static int[] bfsParent = new int[SIZE];

    static void makeEdge (int from, int to, int cap) {
        capacity[from][to] += cap;
        if (capacity[to][from] == 0) {
            adjListResGraph[from][neighboursNum[from]++] = to;
            adjListResGraph[to][neighboursNum[to]++] = from;
        }
    }

    static void init() {
        for (int[] arr: capacity) Arrays.fill(arr, 0);
        for (int[] arr: flow) Arrays.fill(arr, 0);
        Arrays.fill(neighboursNum, 0);
    }

    static int resCapacity(int from, int to) {
        return capacity[from][to] - flow[from][to];
    }

    static boolean bfs(int s, int t) {
        Arrays.fill(visited, false);
        Arrays.fill(bfsParent, -1);
        Queue<Integer> q = new LinkedList<>();

        visited[s] = true;
        q.add(s);

        while (!q.isEmpty()) {
            int node = q.poll();
            if (node == t) return true;
            for (int i = 0; i < neighboursNum[node]; ++i) {
                int neighbour = adjListResGraph[node][i];
                int resCap = resCapacity(node, neighbour);
                if (!visited[neighbour] && resCap > 0) {
                    visited[neighbour] = true;
                    bfsParent[neighbour] = node;
                    q.add(neighbour);
                }
            }
        }

        return false;
    }

    static int augmentFlow(int s, int t) {
        int node = t;
        int minResidualCap = Integer.MAX_VALUE;
        while (bfsParent[node] != -1) {
            int parent = bfsParent[node];
            minResidualCap = Math.min(minResidualCap, resCapacity(parent, node));
            node = parent;
        }
        node = t;
        while (bfsParent[node] != -1) {
            int parent = bfsParent[node];
            flow[parent][node] += minResidualCap;
            flow[node][parent] -= minResidualCap;
            node = parent;
        }
        return minResidualCap;
    }

    static int runMaxFlow(int s, int t) {
        int flow = 0;
        while (bfs(s, t)) {
            flow += augmentFlow(s, t);
        }
        return flow;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int caseVal = 1;
        while (true) {
            int n = Integer.parseInt(rd.readLine());
            if (n == 0) break;
            init();
            int s,t,c;
            String[] inp = rd.readLine().split(" ");
            s = Integer.parseInt(inp[0]);
            t = Integer.parseInt(inp[1]);
            c = Integer.parseInt(inp[2]);

            for (int i = 0; i < c; ++i) {
                inp = rd.readLine().split(" ");
                int from = Integer.parseInt(inp[0]);
                int to = Integer.parseInt(inp[1]);
                int bandWidth = Integer.parseInt(inp[2]);
                makeEdge(from, to, bandWidth);
                makeEdge(to, from, bandWidth);
            }
            int flow = runMaxFlow(s, t);
            System.out.printf("Network %d\n", caseVal++);
            System.out.printf("The bandwidth is %d.\n", flow);
            System.out.println();

        }
    }
}
