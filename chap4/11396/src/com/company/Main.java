package com.company;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Main {

    static boolean isBipartite (boolean [][] adjMat, boolean[] visited, int[] level, int n, int nodeStart) {
        Queue<Integer> q = new LinkedList<>();
        int nodeToStartBFS = nodeStart;

        visited[nodeToStartBFS] = true;
        level[nodeToStartBFS] = 0;
        q.add(nodeToStartBFS);


        boolean isBip = true;
        while (!q.isEmpty()) {
            int node = q.poll();
            for (int i = 0 ; i < n ; ++i) {
                if (adjMat[node][i] && !visited[i]) {
                    visited[i] = true;
                    level[i] = level[node] + 1;
                    q.add(i);
                } else if (adjMat[node][i] && visited[i]) {
                    if (level[i] == level[node]) {
                        isBip = false;
                        break;
                    }
                }
            }
            if (!isBip) break;
        }
        return isBip;

    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);

        boolean [][] adjMat = new boolean[310][310];
        int [] level = new int[310];
        boolean visited[] = new boolean[310];

        while (true) {
            int n = sc.nextInt();
            if (n == 0) break;

            for (boolean[] arr: adjMat) Arrays.fill(arr, false);
            Arrays.fill(visited, false);
            Arrays.fill(level, -1);

            for (int i = 0 ; ; ++i) {
                int a = sc.nextInt(), b = sc.nextInt();
                if (a == 0 && b == 0) break;
                adjMat[a][b] = true;
                adjMat[b][a] = true;
            }

            boolean isBipartite = true;

            for (int i = 0 ; i < n; ++i) if (!visited[i]) {
                isBipartite &= isBipartite(adjMat, visited, level, n, i);
                if (!isBipartite) break;
            }

            if (isBipartite) System.out.println("YES");
            else System.out.println("NO");

        }
    }
}
