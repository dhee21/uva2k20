package com.company;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Main {

    static int oddLevel, evenLevel;

    static boolean isBipartite (boolean [][] adjMat, boolean[] visited, int[] level, int n, int nodeToStart) {
        Queue<Integer> q = new LinkedList<>();
        int nodeToStartBFS = nodeToStart;

        visited[nodeToStartBFS] = true;
        level[nodeToStartBFS] = 0;
        q.add(nodeToStartBFS);

        int nodeTravelled = 0;

        boolean isBip = true;
        while (!q.isEmpty()) {
            nodeTravelled++;
            int node = q.poll();
            if ((level[node] & 1) > 0) oddLevel++;
            else evenLevel++;

            for (int i = 0 ; i < n ; ++i) {
                if (adjMat[node][i] && !visited[i]) {
                    visited[i] = true;
                    level[i] = level[node] + 1;
                    q.add(i);
                } else if (adjMat[node][i] && visited[i]) {
                    if (level[i] == level[node]) {
                        isBip = false;
                        break;
                    }
                }
            }
            if (!isBip) break;
        }
        if (nodeTravelled == 1) {
            oddLevel = 1;
            evenLevel = 1;
        }
        return isBip;

    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        boolean [][] adjMat = new boolean[210][210];
        int [] level = new int[210];
        boolean visited[] = new boolean[210];
        int tc = sc.nextInt();
        while (tc-- > 0) {
            int n = sc.nextInt();
            int m = sc.nextInt();

            for (boolean[] arr: adjMat) Arrays.fill(arr, false);
            Arrays.fill(visited, false);
            Arrays.fill(level, -1);

            for (int i = 0 ; i < m; ++i) {
                int a = sc.nextInt(), b = sc.nextInt();
                adjMat[a][b] = true;
                adjMat[b][a] = true;
            }

            boolean isBipartite = true;

            int ans = 0;
            for (int i = 0 ; i < n; ++i) if (!visited[i]) {
                oddLevel = 0; evenLevel = 0;
                isBipartite &= isBipartite(adjMat, visited, level, n, i);
                if (!isBipartite) break;
                else ans += Math.min(oddLevel, evenLevel);
            }

            if (!isBipartite) System.out.println("-1");
            else System.out.println(ans);


        }
    }
}
