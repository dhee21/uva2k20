package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    static boolean isVisited(int mask, char node) {
        return (mask & (1 << (node - 'A'))) > 0;
    }

    static int visitNode(int mask , char node) {
        return mask | (1 << (node - 'A'));
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String inp = rd.readLine();
            if (inp.equals("#")) break;
            String[] adjAndNode = inp.split("\\.");
            String[] adjList = new String[30];
            char thesus, minotaur;
            int k;
            String adjString = adjAndNode[0];
            String[] adjMatrixInput = adjString.split(";");
            for (String inputForNode : adjMatrixInput) {
                String[] inputComponents = inputForNode.split(":");
                if (inputComponents.length == 2) adjList[inputComponents[0].charAt(0) - 'A'] = inputComponents[1];
            }
            String[] restOfInput = adjAndNode[1].trim().split(" ");
            minotaur = restOfInput[0].charAt(0);
            thesus = restOfInput[1].charAt(0);
            k = Integer.parseInt(restOfInput[2]);

            int visited = 0;
            int val = 1 % k;
            char fromNode = thesus;
            char currNode = minotaur;
            while (true) {
//                System.out.println("see" + val + " " + fromNode + " " + currNode + " ed");
                boolean canVisitSomething = false;
                String adjNodes = adjList[currNode - 'A'];
                if (adjNodes != null) {
                    for (int i = 0; i < adjNodes.length(); ++i) {
                        char adjacentNode = adjNodes.charAt(i);
                        if (!isVisited(visited, adjacentNode) && adjacentNode != fromNode) {
                            canVisitSomething = true; break;
                        }
                    }
                }
                if (!canVisitSomething) {
                    System.out.print("/" + currNode + "\n");
                    break;
                } else if (val == 0) {
                    visited = visitNode(visited, currNode);
                    System.out.print(currNode + " ");
                }

                for (int i = 0; i < adjNodes.length(); ++i) {
                    char adjacentNode  = adjNodes.charAt(i);
                    if (!isVisited(visited, adjacentNode) && adjacentNode != fromNode) {
                        val = (val + 1) % k;
                        fromNode = currNode;
                        currNode = adjacentNode;
                        break;
                    }
                }

            }

        }
    }
}
