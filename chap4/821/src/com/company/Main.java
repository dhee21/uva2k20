package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    static int[][] dist = new int[110][110];
    static int INFINITY = Integer.MAX_VALUE;
    static int N = -1;

    static void init () {
        for (int[] arr: dist) {
            Arrays.fill(arr, INFINITY);
        }
    }

    static void floydWarshall() {
        for (int i = 0 ; i < N ; ++i) dist[i][i] = 0;

        for (int k = 0 ; k < N; ++k) {
            for (int i = 0 ; i < N; ++i) for (int j = 0; j < N; ++j) {
                if (dist[i][k] != INFINITY && dist[k][j] != INFINITY) dist[i][j] = Math.min(dist[i][j], dist[i][k] + dist[k][j]);
            }
        }
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int caseVal = 1;
        while (true) {
            init();
            N = 0;
            while (true) {
                int a = sc.nextInt(), b = sc.nextInt();
                if (a == 0 && b == 0) break;
                dist[a - 1][b - 1] = 1;
                N = Math.max(N, Math.max(a, b));
            }
            if (N == 0) break;
            floydWarshall();

            int sum = 0;
            int total = 0;
            for (int i = 0 ; i < N; ++i) for (int j = 0; j < N; ++j) if (i != j && dist[i][j] != INFINITY) {
                sum += dist[i][j]; total++;
            }
//            System.out.println(sum + " " + total);
            System.out.printf("Case %d: average length between pages = %.3f clicks\n", caseVal++, ((double)sum / (double)total) );

        }
    }
}
