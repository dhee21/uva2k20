package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Stack;

public class Main {

    static int SIZE = 2010;
    static HashMap<String, Integer> nodeToIndex;
    static int numOfNodes;
    static int [] arrivalTime =  new int[SIZE];
    static boolean adjMatrix[][] = new boolean[SIZE][SIZE];
    static boolean[] visited = new boolean[SIZE];
    static String[] nodes;
    static int time;

    static Stack<Integer> nodesArrived;
    static boolean[] cutFromGraph = new boolean[SIZE];

    static void initGraph() {
        time = 0;
        numOfNodes = 0;
        for (boolean[] arr: adjMatrix) Arrays.fill(arr, false);
        Arrays.fill(visited, false);
        Arrays.fill(arrivalTime, -1);
    }

    static boolean isSC;

    static int stronglyConnectedComponents(int node, int root) {
        visited[node] = true;
        arrivalTime[node] = time++;

        int dbe = arrivalTime[node];
        for (int i = 0; i < numOfNodes; ++i) if (adjMatrix[node][i]) {
            if (!visited[i]) {
                dbe = Math.min(dbe, stronglyConnectedComponents(i, root));
            } else {
                if (!cutFromGraph[i]) dbe = Math.min(dbe, arrivalTime[i]);
            }
        }

        if (dbe == arrivalTime[node] && node != root) {
            isSC = false;
        }
        return dbe;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String[] inp = rd.readLine().split(" ");
            int n, m;
            n = Integer.parseInt(inp[0]);
            m = Integer.parseInt(inp[1]);
            if (n == 0 && m == 0) break;

            initGraph();
            numOfNodes = n;

            for (int i = 0 ; i < m; ++i) {
                String [] input = rd.readLine().split(" ");
                int v, w, p;
                v = Integer.parseInt(input[0]);
                w = Integer.parseInt(input[1]);
                p = Integer.parseInt(input[2]);
                if (p == 1) adjMatrix[v - 1][w - 1] = true;
                else {
                    adjMatrix[v - 1][w - 1] = true;
                    adjMatrix[w - 1][v - 1] = true;
                }
            }

            isSC = true;
            stronglyConnectedComponents(0, 0);

            if (!isSC) {
                System.out.println("0"); continue;
            }
            boolean isSomebodyLeft = false;
            for (int i = 0 ; i < n; ++i) if (!visited[i]) {
                isSomebodyLeft = true;
                break;
            }

            if (isSomebodyLeft) System.out.println("0");
            else  System.out.println("1");
        }
    }
}
