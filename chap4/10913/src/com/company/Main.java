import java.util.Arrays;
import java.util.Scanner;

public class Main {

    static int SIZE = 80;
    static int LEFT = 1, RIGHT = 2, TOP = 0;
    static int[][] grid = new int[SIZE][SIZE];
    static Integer [][][][] dp = new Integer[SIZE][SIZE][7][3];
    static int n, k;


    static boolean canGo(int r, int c) {
        return r >= 0 && r < n && c >= 0 && c < n;
    }

    static Integer maxPossible(int r, int c, int negativeLeft, int cameFrom) {
        int negOrig = negativeLeft;
        if (grid[r][c] < 0) negativeLeft--;
        if (negativeLeft < 0) return Integer.MIN_VALUE;
        if (r == n - 1 && c == n - 1) {
            return grid[r][c];
        }

        if (dp[r][c][negOrig][cameFrom] != null) {
            return dp[r][c][negOrig][cameFrom];
        }


        int toReturn = Integer.MIN_VALUE;
        if (canGo(r + 1, c)) toReturn = Math.max(toReturn, maxPossible(r + 1, c, negativeLeft, TOP));
        if (cameFrom != LEFT && canGo(r, c - 1)) toReturn = Math.max(toReturn, maxPossible(r, c - 1, negativeLeft, RIGHT));
        if (cameFrom != RIGHT && canGo(r, c + 1)) toReturn = Math.max(toReturn, maxPossible(r , c + 1, negativeLeft, LEFT));

        if (toReturn == Integer.MIN_VALUE){
            dp[r][c][negOrig][cameFrom] = toReturn;
            return toReturn;
        } else {
            dp[r][c][negOrig][cameFrom] = toReturn + grid[r][c];
            return dp[r][c][negOrig][cameFrom];
        }
    }

    public static void main(String[] args) {
        // write your code here
        int caseVal = 1;
        Scanner sc = new Scanner(System.in);
        while (true) {
            n = sc.nextInt(); k = sc.nextInt();
            if (n == 0 && k == 0) break;
            for (Integer[][][] a : dp) for (Integer[][] b : a) for (Integer[] c: b) Arrays.fill(c, null);

            for (int i = 0 ; i < n; ++i) for (int j = 0 ; j < n; ++j) grid[i][j] = sc.nextInt();

            int ans = maxPossible(0, 0, k, TOP);

            if (ans == Integer.MIN_VALUE) System.out.printf("Case %d: impossible\n", caseVal++);
            else System.out.printf("Case %d: %d\n", caseVal++, ans);;

        }
    }
}
