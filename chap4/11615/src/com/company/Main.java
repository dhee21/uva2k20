package com.company;

import java.util.Scanner;

public class Main {

    static int powerOfTwo(int exp) {
     return 1 << exp;
    }

    static int log(int n) {
        int val = 0;
        while (n > 1) {
            val++;
            n >>= 1;
        }
        return val;
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);

        int tc = sc.nextInt();
        while (tc-- > 0) {
            int N = sc.nextInt(), a = sc.nextInt(), b = sc.nextInt();
            int level1 = log(a);
            int level2 = log(b);
            int lastLevel = N - 1;

            int greaterLevel = Math.max(level1, level2);
            int levelDiff = lastLevel - greaterLevel;
            int toRem = powerOfTwo(levelDiff + 1) - 2;
            System.out.println(powerOfTwo(N) - 1 - toRem);

        }
    }
}
