package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

    static boolean dfs (int row, int col, char[][] grid, int n, boolean[][] visited) {
        boolean isShip = grid[row][col] == 'x';
        visited[row][col] = true;

        int adjRows[] = {row - 1, row + 1, row , row};
        int adjCols[] = {col, col, col - 1, col + 1};

        for (int i = 0 ; i < 4; ++i) {
            int r = adjRows[i];
            int c = adjCols[i];
            if (r >= 0 && r < n && c >= 0 && c < n && !visited[r][c] && (grid[r][c] == 'x' || grid[r][c] == '@')) {
                isShip = isShip | dfs(r, c, grid, n, visited);
            }
        }
        return isShip;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(rd.readLine());
        char[][] grid = new char[110][110];
        boolean visited[][] = new boolean[110][110];
        for (int caseVal = 1; caseVal <= tc; ++caseVal) {
            int n = Integer.parseInt(rd.readLine());

            for (boolean[] arr: visited) Arrays.fill(arr, false);

            for (int r = 0; r < n; ++r) {
                String inp = rd.readLine();
                for (int c = 0; c < inp.length(); ++c) {
                    grid[r][c] = inp.charAt(c);
                }
            }

            int numShips = 0;

            for (int r = 0; r < n; ++r) {
                for (int c = 0; c < n; ++c) {
                    if (!visited[r][c] && (grid[r][c] == 'x' || grid[r][c] == '@')) {
                        if (dfs(r, c, grid, n, visited)) {
                            numShips++;
                        }
                    }
                }
            }
            System.out.printf("Case %d: %d\n", caseVal, numShips);

        }
    }
}
