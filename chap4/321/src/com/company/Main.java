package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class Main {
    static class Pair<A, B> {
        A first;
        B second;
        Pair(A a, B b) {
            first = a; second = b;
        }
    }

    static boolean isRoomLit(int roomNum, int roomMask) {
        return ( roomMask & (1 << (roomNum - 1)) ) > 0;
    }

    static boolean isThereSwitchOfRoom(int roomNum, int switchMask) {
        return (switchMask & (1 << roomNum) ) > 0;
    }

    static int turnOnRoomNumber(int roomNum, int roomMask) {
        return roomMask | (1 << (roomNum - 1));
    }

    static int turnOffRoomNumber(int roomNum, int roomMask) {
        return roomMask & (~(1 << (roomNum - 1)));
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        int r, d, s;
        int room1, room2;
        ArrayList<Integer>[] adjList = new ArrayList[15];
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int[] switches = new int[15];
        boolean[] visited = new boolean[20000];
        int [] parent = new int[20000], finalPath = new int[20000];
        int fourBitMask = 1 | (1 << 1) | (1 << 2) | (1 << 3);

        int caseVal = 1;
        while (true) {
            String[] inp = rd.readLine().split(" ");
            r = Integer.parseInt(inp[0]);
            d = Integer.parseInt(inp[1]);
            s = Integer.parseInt(inp[2]);

            if (r==0 && d==0 && s==0) break;

            for (int i = 0; i < 15; ++i) {
                if (adjList[i] == null) adjList[i] = new ArrayList<>();
                else adjList[i].clear();
            }
            Arrays.fill(switches, 0);
            Arrays.fill(visited, false);

            for (int i = 0 ; i < d; ++i) {
                inp = rd.readLine().split(" ");
                room1 = Integer.parseInt(inp[0]);
                room2 = Integer.parseInt(inp[1]);
                adjList[room1].add(room2);
                adjList[room2].add(room1);
            }
            for (int i = 0; i < s; ++i) {
                inp = rd.readLine().split(" ");
                room1 = Integer.parseInt(inp[0]);
                room2 = Integer.parseInt(inp[1]);
                switches[room1] |= 1 << room2;
            }
            rd.readLine();

            Queue<Pair<Integer, Integer>> q = new LinkedList<>();

            int roomStart = 1; // 4 bits for room
            int roomsLit = 1;
            int bitsForCurrentRoom = 4;
            int startState = roomStart | (roomsLit << bitsForCurrentRoom);
            q.add(new Pair<>(startState, 0));

            int finalRoomsLit = 1 << (r - 1);
            int finalState = r | (finalRoomsLit << bitsForCurrentRoom);

            visited[startState] = true;
            int ans = 0;

            while (!q.isEmpty()) {
                Pair<Integer, Integer> currentStateObject = q.poll();
                int level = currentStateObject.second;
                int currentState = currentStateObject.first;
                if (currentState == finalState) {
                    ans = level;
                    break;
                }

                int currentRoom = currentState & fourBitMask;
                roomsLit = currentState >> bitsForCurrentRoom;

                int nextState =  - 1;
                for (int room = 1; room <= r; ++room) if (room != currentRoom &&
                        isThereSwitchOfRoom(room, switches[currentRoom])) {
                    int newRoomsLit;

                    if (isRoomLit(room, roomsLit))  newRoomsLit = turnOffRoomNumber(room, roomsLit);
                    else  newRoomsLit = turnOnRoomNumber(room, roomsLit);

                    nextState = currentRoom | (newRoomsLit << bitsForCurrentRoom);
                    if (!visited[nextState]) {
                        visited[nextState] = true;
                        parent[nextState] = currentState;
                        q.add(new Pair<>(nextState, level + 1));
                    }
                }

                for (int i = 0 ; i < adjList[currentRoom].size(); ++i) {
                    int neighbour = adjList[currentRoom].get(i);
                    if (isRoomLit(neighbour, roomsLit)) {
                        nextState = neighbour | (roomsLit << bitsForCurrentRoom);
                        if (!visited[nextState]) {
                            visited[nextState] = true;
                            parent[nextState] = currentState;
                            q.add(new Pair<>(nextState, level + 1));
                        }
                    }
                }

            }

            System.out.printf("Villa #%d\n", caseVal++);
            if (!visited[finalState]) {
                System.out.println("The problem cannot be solved.\n");
                continue;
            }
            finalPath[0] = finalState;
            for (int i = 1; i <= ans; ++i) {
                finalPath[i] = parent[finalPath[i - 1]];
            }

            System.out.printf("The problem can be solved in %d steps:\n", ans);
            for (int i = ans - 1; i >= 0; --i) {
                int prevState = finalPath[i + 1];
                int currentState = finalPath[i];

                int prevRoom = prevState & fourBitMask;
                int roomLitPRev = prevState >> bitsForCurrentRoom;

                int currentRoom = currentState & fourBitMask;
                int roomLitNow = currentState >> bitsForCurrentRoom;

                if (prevRoom != currentRoom) {
                    System.out.printf("- Move to room %d.\n", currentRoom);
                } else {

                    for (int room = 1; room <= r; ++room) {
                        int bit = 1 << (room - 1);
                        if ((roomLitNow & bit) != (roomLitPRev & bit)) {
                            if (!isRoomLit(room, roomLitNow)) {
                                System.out.printf("- Switch off light in room %d.\n", room);
                            } else {
                                System.out.printf("- Switch on light in room %d.\n", room);
                            }
                            break;
                        }
                    }
                }

            }
            System.out.println();

        }
    }
}
