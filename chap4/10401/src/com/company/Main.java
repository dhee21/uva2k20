package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

    static int SIZE = 20;
    static long dp[][] = new long[SIZE][SIZE];
    static boolean canVisit[][] = new boolean[SIZE][SIZE];

    static void init() {
        for (boolean[] arr: canVisit) Arrays.fill(arr, true);
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String inp = rd.readLine();
            if (inp == null) break;
            init();
            inp = inp.trim();
            int n = inp.length();
            if (n == 0) continue;

            for (int c = 0; c < n; ++c) if (inp.charAt(c) != '?') {
                char characterVal = inp.charAt(c);
                int rowValue;

                if (characterVal >= 'A' && characterVal <= 'F') rowValue = characterVal - 'A' + 10 - 1;
                else rowValue = inp.charAt(c) - '0' - 1;

                for (int r = 0; r < n; ++r) if (r != rowValue){
                    canVisit[r][c] = false;
                }
                if (c + 1 < n) {
                    canVisit[rowValue][c + 1] = false;
                    if (rowValue - 1 >= 0) canVisit[rowValue - 1][c + 1] = false;
                    if (rowValue + 1 < n) canVisit[rowValue + 1][c + 1] = false;
                }
                if (c - 1 >= 0) {
                    canVisit[rowValue][c - 1] = false;
                    if (rowValue - 1 >= 0) canVisit[rowValue - 1][c - 1] = false;
                    if (rowValue + 1 < n) canVisit[rowValue + 1][c - 1] = false;
                }
            }

            for (int r = 0; r < n; ++r) dp[r][n] = 1;
            for (int c = n - 1; c > 0; --c) {
                for (int r = 0 ; r < n; ++r) {
                    long val = 0;
                    for (int rowVal = 0; rowVal < n; ++rowVal) {
                        if (rowVal <= r + 1 && rowVal >= r - 1) continue;
                        if (canVisit[rowVal][c]) val += dp[rowVal][c + 1];
                    }
                    dp[r][c] = val;
                }
            }

            long ans = 0;
            for (int r = 0 ; r < n; ++r) if (canVisit[r][0]) ans += dp[r][1];
            System.out.println(ans);

        }
    }
}
