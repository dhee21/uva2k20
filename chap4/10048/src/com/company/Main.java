package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

    static int SIZE = 110;
    static int[][] adjMat = new int[SIZE][SIZE];

    static void floydWarshal(int n) {

        for (int k = 0; k < n; ++k) {
            for (int i = 0; i < n; ++i) for (int j = 0; j < n; ++j) {
                int maxVal = Math.max(adjMat[i][k], adjMat[k][j]);
                adjMat[i][j] = Math.min(adjMat[i][j], maxVal);
            }
        }
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        int n, m, q;
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int caseVal = 1;
        while (true) {
            String[] inp = rd.readLine().split(" ");
            n = Integer.parseInt(inp[0]); m = Integer.parseInt(inp[1]); q = Integer.parseInt(inp[2]);
            if (n == 0 && m == 0 && q == 0) break;

            for (int [] arr: adjMat) Arrays.fill(arr, Integer.MAX_VALUE);
            for (int i = 0 ; i < n; ++i) adjMat[i][i] = 0;

            for (int i = 0 ; i < m ; ++i) {
                int c1, c2, d;
                inp = rd.readLine().split(" ");
                c1 = Integer.parseInt(inp[0]); c2 = Integer.parseInt(inp[1]); d = Integer.parseInt(inp[2]);
                adjMat[c1 - 1][c2 - 1] = d;
                adjMat[c2 - 1][c1 - 1] = d;
            }

            floydWarshal(n);
            if (caseVal > 1) System.out.println();
            System.out.printf("Case #%d\n", caseVal++);
            for (int i = 0 ; i < q; ++i) {
                inp = rd.readLine().split(" ");
                int from = Integer.parseInt(inp[0]);
                int to = Integer.parseInt(inp[1]);
                int ans = adjMat[from - 1][to - 1];
                System.out.println( ans == Integer.MAX_VALUE ? "no path": ans);
            }
        }
    }
}
