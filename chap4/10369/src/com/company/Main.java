package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class Main {
    static class Pair<A, B> {
        A first;
        B second;
        Pair(A a, B b) {
            first = a; second = b;
        }
    }


    static class UnionFind {

        private ArrayList<Integer> pset, nodes;
        public int numOfSets;

        UnionFind (int n) {
            pset = new ArrayList<>(n);
            nodes = new ArrayList<>(n);
            for (int i = 0 ; i < n; ++i) {
                pset.add(i);
                nodes.add(1);
            }
            numOfSets = n;
        }

        void unionSet (int vertex1, int vertex2) {
            int set1 = findSet(vertex1), set2 = findSet(vertex2);
            if (set1 == -1 || set2 == -1 || set1 == set2) return;
            if (nodes.get(set1) > nodes.get(set2)) {
                pset.set(set2, set1);
                nodes.set(set1, nodes.get(set1) + nodes.get(set2));
            } else {
                pset.set(set1, set2);
                nodes.set(set2, nodes.get(set1) + nodes.get(set2));
            }
            numOfSets--;
        }

        void compressThePath (int vertex, int set) {
            while (vertex != set) {
                int parent = pset.get(vertex);
                pset.set(vertex, set);
                vertex = parent;
            }
        }

        int findSet (int vertex) {
            if (vertex < 0 || vertex >= pset.size()) return -1;

            int ptr = vertex;
            while (pset.get(ptr) != ptr) ptr = pset.get(ptr);
            compressThePath(vertex, ptr);
            return ptr;
        }

        boolean isSameSet (int vertex1, int vertex2) {
            int set1 = findSet(vertex1), set2 = findSet(vertex2);
            if (set1 == -1 || set2 == -1) return false;
            return set1 == set2;
        }

        int numDisjointSets() {
            return  numOfSets;
        }

        int sizeOfSet (int vertex) {
            int set = findSet(vertex);
            if (set == -1) return -1;
            return nodes.get(set);
        }

    }


    static int SIZE = 1010;
    static ArrayList<Pair<Double, Pair<Integer, Integer>>> edgeList;

    static Double dist (int x1, int y1, int x2, int y2) {
        int xDist = x1 - x2;
        int yDist = y1 - y2;
        return Math.sqrt((xDist * xDist) + (yDist * yDist));
    }


    static Double KruskalAlgo (int n, int s) {
        int compToMake = s;
        if (compToMake == n) return 0.0;

        UnionFind unionFindDS = new UnionFind(n);
        edgeList.sort((a, b) -> {
            return a.first - b.first < 0 ? -1 : 1;
        });

        Double ans = 0.0;

        for (int i = 0; i < edgeList.size() && unionFindDS.numOfSets > 1 ; ++i) {
            double edgeLen = edgeList.get(i).first;
            int node1 = edgeList.get(i).second.first, node2 = edgeList.get(i).second.second;
            if (!unionFindDS.isSameSet(node1, node2)) {
                unionFindDS.unionSet(node1, node2);
                if (unionFindDS.numOfSets == compToMake) {
                    ans = edgeLen;
                    break;
                }
            }
        }
        return ans;
    }

    public static void main(String[] args) throws IOException {
        // write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(rd.readLine());

        int[] x = new int[SIZE], y = new int[SIZE];
        for (int caseVal = 1; caseVal <= tc; ++caseVal) {
            int s, n;
            String[] inp = rd.readLine().split(" ");
            s = Integer.parseInt(inp[0]); n = Integer.parseInt(inp[1]);
            for (int i = 0 ; i < n; ++i) {
                inp = rd.readLine().split(" ");
                x[i] = Integer.parseInt(inp[0]); y[i] = Integer.parseInt(inp[1]);
            }


            if (edgeList != null) edgeList.clear();
            else  edgeList = new ArrayList<>();

            for (int i = 0; i < n; ++i) for (int j = i + 1; j < n; ++j) {
                edgeList.add(new Pair<>(dist(x[i], y[i], x[j], y[j]), new Pair<>(i, j)));
            }


            Double ans = KruskalAlgo(n, s);
            System.out.printf("%.2f\n", ans);

        }

    }
}
