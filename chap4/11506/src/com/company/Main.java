package com.company;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;

public class Main {
    static int SIZE = 110;
    static int [][] capacity = new int[SIZE][SIZE], flow = new int[SIZE][SIZE];
    static int [][] adjListResGraph = new int[SIZE][SIZE];
    static int[] neighboursNum = new int[SIZE];
    static int SOURCE = 0, SINK = 37;
    static boolean[] visited = new boolean[SIZE];
    static int[] bfsParent = new int[SIZE];

    static void makeEdge (int from, int to, int cap) {
//        System.out.println(from + " " + to + " " + cap);
        capacity[from][to] = cap;
        if (capacity[to][from] == 0) {
            adjListResGraph[from][neighboursNum[from]++] = to;
            adjListResGraph[to][neighboursNum[to]++] = from;
        }
    }

    static void init() {
        for (int[] arr: capacity) Arrays.fill(arr, 0);
        for (int[] arr: flow) Arrays.fill(arr, 0);
        Arrays.fill(neighboursNum, 0);
    }

    static int resCapacity(int from, int to) {
        return capacity[from][to] - flow[from][to];
    }

    static boolean bfs(int s, int t) {
        Arrays.fill(visited, false);
        Arrays.fill(bfsParent, -1);
        Queue<Integer> q = new LinkedList<>();

        visited[s] = true;
        q.add(s);

        while (!q.isEmpty()) {
            int node = q.poll();
            if (node == t) return true;
            for (int i = 0; i < neighboursNum[node]; ++i) {
                int neighbour = adjListResGraph[node][i];
                int resCap = resCapacity(node, neighbour);
                if (!visited[neighbour] && resCap > 0) {
                    visited[neighbour] = true;
                    bfsParent[neighbour] = node;
                    q.add(neighbour);
                }
            }
        }

        return false;
    }

    static int augmentFlow(int s, int t) {
        int node = t;
        int minResidualCap = Integer.MAX_VALUE;
        while (bfsParent[node] != -1) {
            int parent = bfsParent[node];
            minResidualCap = Math.min(minResidualCap, resCapacity(parent, node));
            node = parent;
        }
        node = t;
        while (bfsParent[node] != -1) {
            int parent = bfsParent[node];
            flow[parent][node] += minResidualCap;
            flow[node][parent] -= minResidualCap;
            node = parent;
        }
        return minResidualCap;
    }

    static long runMaxFlow(int s, int t) {
        long flow = 0;
        while (bfs(s, t)) {
            flow += augmentFlow(s, t);
        }
        return flow;
    }

     static void makeNode(int node , int cost, int M) {
        makeEdge(node, M + node, cost);
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);

        while (true) {
            int n = sc.nextInt(), m = sc.nextInt();
            if (n == 0 && m == 0) break;
            init();
            for (int i = 2 ; i <= n - 1 ; ++i) {
                int node = sc.nextInt();
                int cost = sc.nextInt();
                makeNode(node, cost, n);
            }
            for (int i = 0 ; i < m; ++i) {
                int j = sc.nextInt(), k = sc.nextInt();
                int cost = sc.nextInt();
                int jIn = j, jOut = j;
                int kIn = k, kOut = k;
                if (j != 1 && j != n) jOut = n + j;
                if (k != 1 && k != n) kOut = n + k;

                makeEdge(jOut, kIn, cost);
                makeEdge(kOut, jIn, cost);

            }
            long ans = runMaxFlow(1, n);
            System.out.println(ans);
        }
    }
}
