package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    static int articulationVertexes = 0, time;
    static int articulationCheck(int node, int root, boolean[] visited, boolean[][] adjMatrix, int [] arrivalTime, int n) {
        visited[node] = true;
        arrivalTime[node] = time++;


        int maxDeepestBackEdgeFromChildren = Integer.MIN_VALUE;
        int minDeepestBackEdgeFromChildren = Integer.MAX_VALUE;
        int deepestBackEdgeFromMe = arrivalTime[node];
        int childNum = 0;
        for (int i = 0 ; i < n; ++i) if (adjMatrix[node][i]) {
            if (!visited[i]) {
                childNum++;
                int x = articulationCheck(i, root, visited, adjMatrix, arrivalTime, n);
                maxDeepestBackEdgeFromChildren = Math.max(maxDeepestBackEdgeFromChildren, x);
                minDeepestBackEdgeFromChildren = Math.min(minDeepestBackEdgeFromChildren, x);
            } else {
                deepestBackEdgeFromMe = Math.min(deepestBackEdgeFromMe, arrivalTime[i]);
            }
        }

        if (node == root) {
            if (childNum > 1) {
                articulationVertexes++; return 0;
            }
        } else {
            if (childNum > 0 && maxDeepestBackEdgeFromChildren >= arrivalTime[node]) {
                articulationVertexes++;
            }
        }

        return Math.min(minDeepestBackEdgeFromChildren, deepestBackEdgeFromMe);
    }


    public static void main(String[] args) {
	// write your code here
        boolean [][] adjMatrix = new boolean[210][210];
        boolean [] visited = new boolean[210];
        int [] arrivalTime =  new int[210];


        Scanner sc = new Scanner(System.in);
        while (true) {
            int n = Integer.parseInt(sc.nextLine());
            if (n == 0) break;

            for (boolean[] arr: adjMatrix) Arrays.fill(arr, false);
            Arrays.fill(visited, false);
            Arrays.fill(arrivalTime, -1);

            while (true) {
                String nodes[] = sc.nextLine().split(" ");
                if (nodes.length  == 1 && nodes[0].equals("0")) break;
                int parent = Integer.parseInt(nodes[0]);
                for (int i = 1; i < nodes.length; ++i) {
                    int child = Integer.parseInt(nodes[i]);
                    adjMatrix[parent - 1][child - 1] = true;
                    adjMatrix[child - 1][parent - 1] = true;
                }
            }

            articulationVertexes = 0; time = 0;
            articulationCheck(0, 0, visited, adjMatrix, arrivalTime, n);
            System.out.println(articulationVertexes);

        }
    }
}
