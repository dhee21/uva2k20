package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Stack;

public class Main {

    static int SIZE = 100010;
    static int []  topsort = new int[SIZE];
    static ArrayList<Integer>[] adjList = new ArrayList[SIZE];
    static boolean[] visited = new boolean[SIZE];
    static boolean[] cutFromGraph = new boolean[SIZE];
    static int depTime;
    static int indexOf[] = new int[SIZE];

    static void dfsStack (int node) {
        Stack<Integer> stack = new Stack<>();
        stack.add(node);
        while (!stack.empty()) {
            int nodeAtTop = stack.peek();
            visited[nodeAtTop] = true;

            ArrayList<Integer> neighbours = adjList[nodeAtTop];
            boolean toDepart = true;
            for (int i = indexOf[nodeAtTop]; i < neighbours.size(); ++i) {
                int neightborVal = neighbours.get(i);
                if (!visited[neightborVal]) {
                    toDepart = false;
                    stack.push(neightborVal);
                    break;
                }
            }
            if (toDepart) {
                topsort[depTime++] = nodeAtTop;
                stack.pop();
            }
        }
    }

    static void dfsStack2 (int node) {
        Stack<Integer> stack = new Stack<>();
        stack.add(node);
        while (!stack.empty()) {
            int nodeAtTop = stack.peek();
            visited[nodeAtTop] = true;

            ArrayList<Integer> neighbours = adjList[nodeAtTop];
            boolean toDepart = true;
            for (int i = indexOf[nodeAtTop]; i < neighbours.size(); ++i) {
                int neightborVal = neighbours.get(i);
                if (!visited[neightborVal]) {
                    toDepart = false;
                    stack.push(neightborVal);
                    indexOf[nodeAtTop]++;
                    break;
                }
            }
            if (toDepart) {
                stack.pop();
            }
        }
    }

    static void dfs1(int node) {
        visited[node] = true;

        ArrayList<Integer> neighbours = adjList[node];
        int size = neighbours.size();
        for (int i = 0; i < size; ++i) {
            int neightborVal = neighbours.get(i);
            if (!visited[neightborVal]) {
               dfs1(neightborVal);
            }
        }
        topsort[depTime++] = node;
    }

    static void dfs2(int node) {
        cutFromGraph[node] = true;

        ArrayList<Integer> neighbours = adjList[node];
        int size = neighbours.size();
        for (int i = 0; i < size; ++i) {
            int neightborVal = neighbours.get(i);
            if (!cutFromGraph[neightborVal]) {
                dfs2(neightborVal);
            }
        }
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        int tc = Integer.parseInt(rd.readLine());
        while (tc -- > 0) {
            int n, m;
            String[] nM = rd.readLine().trim().split(" ");
            n = Integer.parseInt(nM[0]);
            m = Integer.parseInt(nM[1]);

            depTime = 0;
            for (int i = 0; i < SIZE; ++i) {
                if (adjList[i] != null) adjList[i].clear();
                else adjList[i] = new ArrayList<>();
            }
            Arrays.fill(visited, 0, SIZE, false);
            Arrays.fill(indexOf, 0, SIZE,0);

            for (int i = 0 ; i < m; ++i) {
                String[] edge = rd.readLine().split(" ");
                int from = Integer.parseInt(edge[0]) - 1, to = Integer.parseInt(edge[1]) - 1;
                adjList[from].add(to);
            }

            for (int i = 0 ; i < n ; ++i) if (!visited[i]) {
                dfsStack(i);
            }

            Arrays.fill(indexOf, 0,n + 9,0);
            Arrays.fill(visited, 0, SIZE,false);
            int ans = 0;
            for (int i = depTime - 1 ; i >= 0; --i) {
                int oneNode = topsort[i];
                if (!visited[oneNode]) {
                    ans++;
                    dfsStack2(oneNode);
                }
            }

            System.out.println(ans);
        }
    }
}
