package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class Main {

    static int SIZE = 20;
    static char [][] grid = new char[SIZE][SIZE];
    static int row, col;
    static int[][] path = new int[SIZE][SIZE];
    static boolean[][] visited = new boolean[SIZE][SIZE];
    static int[][] level = new int[SIZE][SIZE];
    static int N;


    static int mod = 20437;

    static int[] rowNext = {1, -1, 0, 0};
    static int[] colNext = {0, 0, -1, 1};

    static boolean canGo (int r, int c, char toReach) {
        return r >= 0 && r < N && c >= 0 && c < N && (grid[r][c] == '.' || grid[r][c] == toReach);
    }

    static int add (int a, int b) {
        return (a % mod + b % mod) % mod;
    }

    static int bfs (char c) {
        char toReach = (char) (c + 1);
        int pathNum = path[row][col];
        for (int[] arr: path) Arrays.fill(arr, 0);
        path[row][col] = pathNum;
        for (boolean[] arr: visited) Arrays.fill(arr, false);
        for (int[] arr: level) Arrays.fill(arr, -1);

        Queue<int[]> q = new LinkedList<>();
        q.add(new int[] {row, col});
        visited[row][col] = true;
        level[row][col] = 0;

        while (!q.isEmpty()) {
            int[] node = q.poll();
            int rowVal = node[0], colVal = node[1];
            if (grid[rowVal][colVal] == toReach) {
                grid[row][col] = '.';
                row = rowVal; col = colVal;
                return level[rowVal][colVal];
            }

            for (int i = 0; i < 4; ++i) {
                int rToGo = rowVal + rowNext[i];
                int cToGo = colVal + colNext[i];
                if (canGo(rToGo, cToGo, toReach) && !visited[rToGo][cToGo]) {
                    visited[rToGo][cToGo] = true;
                    path[rToGo][cToGo] = add(path[rToGo][cToGo], path[rowVal][colVal]);
                    level[rToGo][cToGo] = level[rowVal][colVal] + 1;
                    q.add(new int[]{ rToGo, cToGo });
                } else if (canGo(rToGo, cToGo, toReach) && visited[rToGo][cToGo] && level[rToGo][cToGo] == level[rowVal][colVal] + 1) {
                    path[rToGo][cToGo] = add(path[rToGo][cToGo], path[rowVal][colVal]);
                }
            }
        }
        return -1;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int caseVal = 1;
        while (true) {
            N = Integer.parseInt(rd.readLine());
            if (N == 0) break;

            int stR = 0, stC = 0;
            char lastChar = 'A';
            for (int i = 0 ; i < N; ++i) {
                String inp = rd.readLine();
                for (int j = 0 ; j < inp.length(); ++j) {
                    grid[i][j] = inp.charAt(j);
                    if (Character.isAlphabetic(grid[i][j])) lastChar = (char) Math.max(lastChar, grid[i][j]);

                    if (grid[i][j] == 'A') {
                        stR = i; stC = j;
                    }
                }
            }

            row = stR; col = stC;
            path[row][col] = 1;
            int pathLength = 0;
            boolean possible = true;
            for (char c = 'A'; c < lastChar; ++c) {
                int len = bfs(c);
                if (len == -1) {
                    possible = false;
                    break;
                }
                else pathLength += len;
            }

            if (!possible) System.out.printf("Case %d: Impossible\n", caseVal++);
            else {
                System.out.printf("Case %d: %d %d\n", caseVal++, pathLength, path[row][col]);
            }
        }
    }
}
