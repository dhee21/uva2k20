package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

    static int dfs (int row, int col, int[][] grid, int n, boolean[][] visited) {
        int visitedNodes = 1;
        visited[row][col] = true;

        int adjRows[] = {row - 1, row + 1, row , row};
        int adjCols[] = {col, col, col - 1, col + 1};

        for (int i = 0 ; i < 4; ++i) {
            int r = adjRows[i];
            int c = adjCols[i];
            if (r >= 0 && r < n && c >= 0 && c < n && !visited[r][c] && grid[r][c] == grid[row][col]) {
                visitedNodes += dfs(r, c, grid, n, visited);
            }
        }
        return visitedNodes;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        int[][] grid = new int[110][110];
        boolean[][] visited = new boolean[110][110];

        while (true) {
            int n = Integer.parseInt(rd.readLine());
            if (n == 0) break;

            for (int[] arr: grid) Arrays.fill(arr, 0);
            for (boolean[] arr: visited) Arrays.fill(arr, false);

            for (int i = 1; i < n; ++i) {
                String[] input = rd.readLine().split(" ");
                int index = 0;
                for (int j = 0; j < (input.length >> 1); ++j) {
                    int row = Integer.parseInt(input[index++]);
                    int col = Integer.parseInt(input[index++]);
                    grid[row - 1][col - 1] = i;
                }
            }

            boolean isWrong = false;
            for (int r = 0; r < n; ++r) {
                for(int c = 0; c < n; ++c) {
                    if (!visited[r][c]) {
                        int val = dfs(r, c, grid, n, visited);
                        if (val != n) {
                            isWrong = true; break;
                        }
                    }
                }
                if (isWrong) break;
            }
            if(isWrong) System.out.println("wrong");
            else System.out.println("good");

        }
    }
}
