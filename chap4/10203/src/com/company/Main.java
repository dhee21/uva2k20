package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {


    static double dist(int x1, int y1, int x2, int y2) {
        long valX = x1 - x2;
        long valY = y2 - y1;
        return Math.sqrt(valX * valX + valY * valY);
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(rd.readLine());
        rd.readLine();

        int caseVal = 1;
        while (tc -- > 0) {
            rd.readLine();
            double sum = 0;
            while (true) {
                String inpString = rd.readLine();
                if (inpString == null || inpString.length() == 0) break;
                String inp[] = inpString.split(" ");
                sum += dist(Integer.parseInt(inp[0]),
                        Integer.parseInt(inp[1]),
                        Integer.parseInt(inp[2]),
                        Integer.parseInt(inp[3]));
            }

            sum *= 2;
            double time = (sum * 3) / 1000;
            int hours = (int)time / 60;
            double minutes = time - (hours * 60);
            int minRounded = (int)Math.floor(minutes + 0.5);
            if (minRounded == 60) {
                minRounded = 0;
                hours++;
            }
            if (caseVal++ != 1){
                System.out.println();
            }
            System.out.println(hours + ":" + ((minRounded < 10) ? "0" + minRounded : minRounded));


        }
    }
}
