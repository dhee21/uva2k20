package com.company;

import java.lang.reflect.Array;
import java.util.*;

public class Main {
    static int N, M;
    static int SIZE = 110;
    static int INFINITY = Integer.MAX_VALUE;
    static int[][] adjList = new int[SIZE][SIZE];
    static int[] neighboursNumOfNodes = new int[SIZE];
    static boolean hasNegativeCycleToReachEnd;

    static int[] dist = new int[SIZE];
    static int[] cost = new int[SIZE];
    static void init () {
        Arrays.fill(neighboursNumOfNodes, 0);
    }

    static boolean[] visited = new boolean[SIZE];
    static boolean canReachEnd (int node) {
        Arrays.fill(visited, false);
        int end = N - 1;
        Queue<Integer> q = new LinkedList<>();
        visited[node] = true;
        q.add(node);
        while (!q.isEmpty()) {
            int oneNode = q.poll();
            if (oneNode == end) return true;
            for (int i = 0; i < neighboursNumOfNodes[oneNode]; ++i) {
                int neighbourNode = adjList[oneNode][i];
                if (!visited[neighbourNode]) {
                    visited[neighbourNode] = true;
                    q.add(neighbourNode);
                }
            }
        }
        return false;
    }

    static void checkCycleToReachEnd() {
        hasNegativeCycleToReachEnd = false;
        for (int node = 0 ; node < N; ++node) if (dist[node] < 100) {
            for (int i = 0; i < neighboursNumOfNodes[node]; ++i) {
                int neighbourNode = adjList[node][i], weight = cost[neighbourNode];
                if (dist[node] + weight < dist[neighbourNode]) {
                    if (canReachEnd(node)) {
                        hasNegativeCycleToReachEnd = true;
                        return;
                    }
                }
            }
        }
    }

    static void BellMenFord (int source) {
        Arrays.fill(dist, INFINITY);
        dist[source] = 0;

        for (int times = 0; times < N - 1; ++times) {
            for (int node = 0 ; node < N; ++node) if (dist[node] < 100) {
                for (int i = 0; i < neighboursNumOfNodes[node]; ++i) {
                    int neighbourNode = adjList[node][i], weight = cost[neighbourNode];
                    if (dist[node] + weight < dist[neighbourNode]) {
                        dist[neighbourNode] = dist[node] + weight;
                    }
                }
            }
        }
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (true) {
            init();
            N = sc.nextInt();
            if (N == -1) break;

            for (int i = 0 ; i < N; ++i) {
                cost[i] = -sc.nextInt();
                int edgesNum = sc.nextInt();
                for (int j = 0; j < edgesNum; ++j) {
                    int to = sc.nextInt() - 1;
                    adjList[i][neighboursNumOfNodes[i]++] = to;
                }
            }

            BellMenFord(0);
            if (dist[N - 1] < 100) {
                System.out.println("winnable");
                continue;
            }

            checkCycleToReachEnd();
            if (hasNegativeCycleToReachEnd) System.out.println("winnable");
            else System.out.println("hopeless");
        }

    }
}
