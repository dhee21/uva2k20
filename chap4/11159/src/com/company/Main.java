package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

    static int MAXNODES = 1100;

    static int[][] adjList = new int[MAXNODES][MAXNODES];
    static int[] neighboursNum = new int[MAXNODES];
    static int[] left = new int[MAXNODES];
    static int[] right = new int[MAXNODES];



    static int[] owner = new int[MAXNODES];
    static boolean visited[] = new boolean[MAXNODES];

    static void init() {
        Arrays.fill(neighboursNum, 0);
    }

    static boolean canAugment(int node) {
        if (visited[node]) return false;
        visited[node] = true;
        int neighbours = neighboursNum[node];
        for (int i = 0 ; i < neighbours; ++i) {
            int neighbour = adjList[node][i];
            if (owner[neighbour] == -1 || canAugment(owner[neighbour])) {
                owner[neighbour] = node;
                return true;
            }
        }
        return false;
    }

    static int MCBM (int leftNodes) {
        Arrays.fill(owner, -1);
        int pairs = 0;
        for (int i = 0; i < leftNodes; ++i) {
            Arrays.fill(visited, false);
            if (canAugment(i)) pairs++;
        }
        return pairs;
    }

    static void makeEdge (int n1, int n2) {
        adjList[n1][neighboursNum[n1]++] = n2;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(rd.readLine());
        int caseVal = 1;
        while (n-- > 0) {
            init();
            String [] inp = rd.readLine().split(" ");
            int N = Integer.parseInt(inp[0]);
            for (int i = 1; i <= N; ++i) left[i - 1] = Integer.parseInt(inp[i]);
            inp = rd.readLine().split(" ");
            int M = Integer.parseInt(inp[0]);
            for (int i = 1; i <= M; ++i) right[i - 1] = Integer.parseInt(inp[i]);

            for (int i = 0; i < N; ++i) {
                for (int j = 0 ; j < M; ++j) {
                    if (left[i] != 0) {
                        if (right[j] % left[i] == 0) makeEdge(i, N + j);
                    } else {
                        if (right[j] == 0) makeEdge(i, N + j);
                    }

                }
            }

            int ans = MCBM(N);
            System.out.printf("Case %d: %d\n", caseVal++, ans);
        }
    }
}
