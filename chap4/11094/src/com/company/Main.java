package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

    static  char land;

    static int dfs (char[][] grid, int stRow, int stCol, boolean[][] visited, int m, int n) {
        int nodeVisited = 1;
        visited[stRow][stCol] = true;
        int[] adjacentRow = {stRow, stRow, stRow - 1 , stRow + 1};
        int[] adjacentCol = {stCol - 1 == -1 ? n - 1: stCol - 1, (stCol + 1) % n, stCol, stCol};

        for (int i = 0; i < 4; ++i) {
            int adjR = adjacentRow[i];
            int adjC = adjacentCol[i];
            if (adjR >= 0 && adjR < m && adjC >= 0 && adjC < n && !visited[adjR][adjC] && grid[adjR][adjC] == land) {
                nodeVisited += dfs(grid, adjR, adjC, visited, m, n);
            }
        }
        return nodeVisited;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        char[][] grid = new char[50][50];
        boolean[][] visited = new boolean[50][50];
        while (true) {
            for (boolean[] arr: visited) Arrays.fill(arr, false);
            int m, n;
            String input = rd.readLine();
            if (input == null) break;
            String[] mAndN = input.split(" ");
            m = Integer.parseInt(mAndN[0]);
            n = Integer.parseInt(mAndN[1]);
            for (int r = 0 ; r < m; ++r) {
                String row = rd.readLine();
                for (int c = 0 ; c < n; ++c) {
                    grid[r][c] = row.charAt(c);
                }
            }
            int x, y;
            String[] xAndY = rd.readLine().split(" ");
            x = Integer.parseInt(xAndY[0]);
            y = Integer.parseInt(xAndY[1]);

            land = grid[x][y];
            dfs(grid, x, y, visited, m, n);

            int max = 0;
            for (int r = 0; r < m; ++r) for (int c = 0 ; c < n; ++c) {
                if (!visited[r][c] && grid[r][c] == land) {
                    max = Math.max(max, dfs(grid, r, c, visited, m, n));
                }
            }

            System.out.println(max);
            rd.readLine();
        }

    }
}
