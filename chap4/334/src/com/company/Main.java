package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;

public class Main {
    static int N, M, SIZE = 210;
    static boolean dist [][] = new boolean[SIZE][SIZE];

   static String[] nodes = new String[SIZE];
    static int index = 0;
    static HashMap<String, Integer> indexOf;

    static void makeNode (String val) {
        int newIndex = index++;
        nodes[newIndex] = val;
        indexOf.put(val, newIndex);
    }

    static void makeEdge(String one, String two) {
        dist[indexOf.get(one)][indexOf.get(two)] = true;
    }

    static void floyd () {
        for (int i = 0 ; i < N; ++i) dist[i][i] = true;
        for (int k = 0; k < N ; ++k) for (int i = 0 ; i < N; ++i) for (int j = 0 ; j < N; ++j) {
            dist[i][j] = dist[i][j] | (dist[i][k] && dist[k][j]);
        }
    }

    static void init () {
        for (boolean[] arr: dist) Arrays.fill(arr, false);
        index = 0;
        if (indexOf != null) indexOf.clear();
        else indexOf = new HashMap<>();
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int caseVal = 1;
        String[] ans = new String[10];
        while (true) {
            int lines = Integer.parseInt(rd.readLine().trim());
            if (lines == 0) break;
            init();
            for (int i = 0 ; i < lines; ++i) {
                String[] inp = rd.readLine().trim().split(" ");
                for (int j = 1 ; j < inp.length; ++j) makeNode(inp[j]);
                for (int j = 1; j < inp.length - 1; ++j) makeEdge(inp[j], inp[j + 1]);
            }
            N = index;
            M = Integer.parseInt(rd.readLine().trim());
            for (int i = 0 ; i < M; ++i) {
                String[] inp = rd.readLine().trim().split(" ");
                String one = inp[0];
                String two = inp[1];
                makeEdge(one, two);
            }

            floyd();
            int indexAns = 0;
            for (int i = 0; i < N; ++i) for (int j = i + 1; j < N; ++j) {
                if (!dist[i][j] && !dist[j][i]) {
                    if (indexAns < 2) ans[indexAns] = "(" + nodes[i] + "," + nodes[j] + ")";
                    indexAns++;
                }
            }
            if (indexAns == 0) {
                System.out.printf("Case %d, no concurrent events.\n", caseVal++);
            } else {
                System.out.printf("Case %d, %d concurrent events:\n", caseVal++, indexAns);
                for (int i = 0; i < Math.min(indexAns, 2); ++i) {
                    System.out.print(ans[i] + " ");
                }
                System.out.println();
            }
        }
    }
}
