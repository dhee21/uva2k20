package com.company;

import java.util.*;

public class Main {

    static class Pair<A, B> {
        A first;
        B second;
        Pair(A a, B b) {
            first = a; second = b;
        }
    }

    static boolean canGo(int row, int col) {
        return row >= 0 && col >= 0 && row <= 2000 && col <= 2009;
    }
    static int bfs (Queue<Pair<Integer, Pair<Integer, Integer>>> q, int[][] visited) {

        int x[] = {0, 0, 1, -1};
        int y[] = {1, -1, 0, 0};

        while (!q.isEmpty()) {
            Pair<Integer, Pair<Integer, Integer>> p = q.poll();
            int st = p.second.first, av = p.second.second;
            int lvl = p.first;
            for (int i = 0 ; i < 4; ++i) {
                int newSt = st  + x[i], newAv = av + y[i];
                if (canGo(newSt , newAv) && visited[newSt][newAv] != 1) {
                    if (visited[newSt][newAv] == 2) return lvl + 1;
                    q.add(new Pair<>(lvl + 1, new Pair<>(newSt, newAv)));
                    visited[newSt][newAv] = 1;
                }
            }
        }
        return 0;
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int[][] visited = new int[2010][2010];

        while (true) {
            int mall1 = sc.nextInt();
            if (mall1 == 0) break;
            for (int[] arr: visited) Arrays.fill(arr, -1);

            Queue<Pair<Integer, Pair<Integer, Integer>>> q = new LinkedList<>();
            for (int i = 0 ; i < mall1; ++i) {
                int a, s;
                a = sc.nextInt(); s = sc.nextInt();
                q.add(new Pair<>(0, new Pair<>(s, a)) );
                visited[s][a] = 1;
            }

            int mall2 = sc.nextInt();
            for (int i = 0 ; i < mall2; ++i) {
                int a, s;
                a = sc.nextInt(); s = sc.nextInt();
                visited[s][a] = 2;
            }

            int ans = bfs(q, visited);

            System.out.println(ans);

        }
    }
}
