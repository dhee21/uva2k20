package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;

public class Main {

    static class Pair<A, B> {
        A first;
        B second;
        Pair(A a, B b) {
            first = a; second = b;
        }
    }


    static int SIZE = 200020;
    static int[] pset = new int[SIZE], nodes = new int[SIZE];
    static class UnionFind {


        public int numOfSets;
        public  int numOfNodes ;

        UnionFind (int n) {
            for (int i = 0 ; i < n; ++i) {
                pset[i] = i;
                nodes[i] = 1;
            }
            numOfSets = n;
            numOfNodes = n;
        }

        void unionSet (int vertex1, int vertex2) {
            int set1 = findSet(vertex1), set2 = findSet(vertex2);
            if (set1 == -1 || set2 == -1 || set1 == set2) return;
            if (nodes[set1] > nodes[set2]) {
                pset[set2] = set1;
                nodes[set1] = nodes[set1] + nodes[set2];
            } else {
                pset[set1] = set2;
                nodes[set2] =  nodes[set1] + nodes[set2];
            }
            numOfSets--;
        }

        void compressThePath (int vertex, int set) {
            while (vertex != set) {
                int parent = pset[vertex];
                pset[vertex] = set;
                vertex = parent;
            }
        }

        int findSet (int vertex) {
            if (vertex < 0 || vertex >= numOfNodes) return -1;

            int ptr = vertex;
            while (pset[ptr] != ptr) ptr = pset[ptr];
            compressThePath(vertex, ptr);
            return ptr;
        }

        boolean isSameSet (int vertex1, int vertex2) {
            int set1 = findSet(vertex1), set2 = findSet(vertex2);
            if (set1 == -1 || set2 == -1) return false;
            return set1 == set2;
        }

        int numDisjointSets() {
            return  numOfSets;
        }

        int sizeOfSet (int vertex) {
            int set = findSet(vertex);
            if (set == -1) return -1;
            return nodes[set];
        }

    }


    static ArrayList<Pair<Integer, Pair<Integer, Integer>>> edgeList;
    static int spannedTotal;
    static void KruskalAlgo (int n) {
        spannedTotal = 0;
        UnionFind unionFindDS = new UnionFind(n);
        edgeList.sort(Comparator.comparingInt(a -> a.first));

        for (int i = 0; i < edgeList.size() && unionFindDS.numOfSets > 1 ; ++i) {
            int edgeLen = edgeList.get(i).first;
            int node1 = edgeList.get(i).second.first, node2 = edgeList.get(i).second.second;
            if (!unionFindDS.isSameSet(node1, node2)) {
                spannedTotal += edgeLen;
                unionFindDS.unionSet(node1, node2);
            }
        }
    }

    public static void main(String[] args) throws IOException {
        // write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int from, to, weight;
        while (true) {
            int n, m;
            String[] inp = rd.readLine().split(" ");
            m = Integer.parseInt(inp[0]); n = Integer.parseInt(inp[1]);

            if (n == 0 && m == 0) break;

            if (edgeList != null) edgeList.clear();
            else  edgeList = new ArrayList<>();

            int total = 0;
            for (int i = 0 ; i < n; ++i) {
                inp = rd.readLine().split(" ");
                from = Integer.parseInt(inp[0]); to = Integer.parseInt(inp[1]); weight = Integer.parseInt(inp[2]);
                edgeList.add(new Pair<>(weight, new Pair<>(from, to)));
                total += weight;
            }

            KruskalAlgo(m);
            System.out.println(total - spannedTotal);

        }

    }
}
