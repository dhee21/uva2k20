package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;

public class Main {
    static class Pair<A, B> {
        A first;
        B second;
        Pair(A a, B b) {
            first = a; second = b;
        }
    }
    static int BIT_FOR_FUEL = 7;
    static int SIZE = ((1 << 17));
    static int citySize = 1100;
    static int[][] adjList = new int[citySize][citySize];
    static int[][] weightList = new int[citySize][citySize];
    static int[] neighbourNum = new int[citySize];
    static int[] dist = new int[SIZE];

    static int prices[] = new int[citySize], N ,M;


    static void init () {
        Arrays.fill(neighbourNum, 0);
    }

    static void makeEdge(int node1, int node2, int weight) {
        int nextIndex = neighbourNum[node1]++;
        adjList[node1][nextIndex] = node2;
        weightList[node1][nextIndex] = weight;

        int nextIndexNode2 = neighbourNum[node2]++;
        adjList[node2][nextIndexNode2] = node1;
        weightList[node2][nextIndexNode2] = weight;
    }

    static int djakstra(int sourceNode, int destCity, int cap) {
        int ansVal = Integer.MAX_VALUE;
        Arrays.fill(dist, Integer.MAX_VALUE);
        dist[sourceNode] = 0;

        PriorityQueue<Pair<Integer, Integer>> pq = new PriorityQueue<>((a, b) -> a.first - b.first);
        pq.add(new Pair<>(0, sourceNode));

        while (!pq.isEmpty()) {
            Pair<Integer, Integer> pqObject = pq.poll();
            int distanceLabel = pqObject.first, node = pqObject.second;
            if (distanceLabel != dist[node] ) continue;

            int fuel = getFuelFromNode(node), city = getCityFromNode(node);

            if (city == destCity) ansVal = Math.min(ansVal, distanceLabel);

            if (fuel + 1 <= cap) {
                int fuelPlusOneNeighbour = getNode(fuel + 1, city);
                int possibleDistanceForNeighbour = dist[node] + prices[city];
                if (possibleDistanceForNeighbour < dist[fuelPlusOneNeighbour]) {
                    dist[fuelPlusOneNeighbour] = possibleDistanceForNeighbour;
                    pq.add(new Pair<>(possibleDistanceForNeighbour, fuelPlusOneNeighbour));
                }
            }

            for (int i = 0 ; i < neighbourNum[city]; ++i) {
                int neighbourCity = adjList[city][i], edgeWeight = weightList[city][i];
                if (fuel >= edgeWeight) {
                    int neighbour = getNode(fuel - edgeWeight, neighbourCity);
                    int possibleDistanceLableForNeighbour = distanceLabel;
                    if (possibleDistanceLableForNeighbour < dist[neighbour]) {
                        dist[neighbour] = possibleDistanceLableForNeighbour;
                        pq.add(new Pair<>(possibleDistanceLableForNeighbour, neighbour));
                    }
                }
            }
        }
        return ansVal;
    }

    static int getNode(int fuel, int city) {
        return (city << BIT_FOR_FUEL) | fuel;
    }
    static int getFuelFromNode(int node) {
        return node & ((1 << BIT_FOR_FUEL) - 1);
    }
    static int getCityFromNode(int node) {
        return node >> BIT_FOR_FUEL;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        StringBuilder str = new StringBuilder();

        while (true) {
            String isNull = rd.readLine();
            if (isNull == null) break;
            init();

            String inp[] = isNull.trim().split(" ");
            N = Integer.parseInt(inp[0]); M = Integer.parseInt(inp[1]);
            int pricesCame = 0;
            while (pricesCame != N) {
                inp = rd.readLine().trim().split(" ");
                for (int i = 0 ; i < N; ++i) prices[pricesCame++] = Integer.parseInt(inp[i]);
            }
            for (int i = 0 ; i < M; ++i) {
                inp = rd.readLine().trim().split(" ");
                int from = Integer.parseInt(inp[0]);
                int to = Integer.parseInt(inp[1]);
                int weight = Integer.parseInt(inp[2]);
                makeEdge(from, to, weight);
            }

            int q = Integer.parseInt(rd.readLine());
            while (q-- > 0) {
                int startCity, endCity , cap;
                inp = rd.readLine().trim().split(" ");
                cap = Integer.parseInt(inp[0]); startCity = Integer.parseInt(inp[1]); endCity = Integer.parseInt(inp[2]);

                int ans = djakstra(getNode(0, startCity), endCity, cap);
                if (ans == Integer.MAX_VALUE) str.append("impossible\n");
                else str.append(ans).append("\n");
            }


        }
        System.out.print(str);
    }
}
