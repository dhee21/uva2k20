package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class Main {

    static int SIZE = 110;
    static long [][] capacity = new long[SIZE][SIZE], flow = new long[SIZE][SIZE];
    static int [][] adjListResGraph = new int[SIZE][SIZE];
    static int[] neighboursNum = new int[SIZE];
    static boolean[] visited = new boolean[SIZE];
    static int[] bfsParent = new int[SIZE];

    static void makeEdge (int from, int to, long cap) {
        capacity[from][to] = cap;
        if (capacity[to][from] == 0) {
            adjListResGraph[from][neighboursNum[from]++] = to;
            adjListResGraph[to][neighboursNum[to]++] = from;
        }
    }

    static void init() {
        for (long[] arr: capacity) Arrays.fill(arr, 0);
        for (long[] arr: flow) Arrays.fill(arr, 0);
        Arrays.fill(neighboursNum, 0);
    }

    static long resCapacity(int from, int to) {
        return capacity[from][to] - flow[from][to];
    }

    static boolean bfs(int s, int t) {
        Arrays.fill(visited, false);
        Arrays.fill(bfsParent, -1);
        Queue<Integer> q = new LinkedList<>();

        visited[s] = true;
        q.add(s);

        while (!q.isEmpty()) {
            int node = q.poll();
            if (node == t) return true;
            for (int i = 0; i < neighboursNum[node]; ++i) {
                int neighbour = adjListResGraph[node][i];
                long resCap = resCapacity(node, neighbour);
                if (!visited[neighbour] && resCap > 0) {
                    visited[neighbour] = true;
                    bfsParent[neighbour] = node;
                    q.add(neighbour);
                }
            }
        }

        return false;
    }

    static int bfs2(int s, int t, int[][]ans) {
        int len = 0;
        Arrays.fill(visited, false);
        Arrays.fill(bfsParent, -1);
        Queue<Integer> q = new LinkedList<>();

        visited[s] = true;
        q.add(s);

        while (!q.isEmpty()) {
            int node = q.poll();
            for (int i = 0; i < neighboursNum[node]; ++i) {
                int neighbour = adjListResGraph[node][i];
                long resCap = resCapacity(node, neighbour);
                long cap = capacity[node][neighbour];
                if (!visited[neighbour] && resCap > 0) {
                    visited[neighbour] = true;
                    bfsParent[neighbour] = node;
                    q.add(neighbour);
                } else if (!visited[neighbour] && resCap == 0) {
                    int lenNew = len++;
                    ans[lenNew][0] = node;
                    ans[lenNew][1] = neighbour;
                }
            }
        }

        return len;
    }


    static long augmentFlow(int s, int t) {
        int node = t;
        long minResidualCap = Long.MAX_VALUE;
        while (bfsParent[node] != -1) {
            int parent = bfsParent[node];
            minResidualCap = Math.min(minResidualCap, resCapacity(parent, node));
            node = parent;
        }
        node = t;
        while (bfsParent[node] != -1) {
            int parent = bfsParent[node];
            flow[parent][node] += minResidualCap;
            flow[node][parent] -= minResidualCap;
            node = parent;
        }
        return minResidualCap;
    }

    static long runMaxFlow(int s, int t) {
        long flow = 0;
        while (bfs(s, t)) {
            flow += augmentFlow(s, t);
        }
        return flow;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        int n, m;
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int[][] ans = new int[600][2];
        while (true) {
            String inp[] = rd.readLine().split( " ");
            n = Integer.parseInt(inp[0]);
            m = Integer.parseInt(inp[1]);
            if (n == 0 && m == 0) break;
            init();
            for (int i = 0; i < m; ++i) {
                inp = rd.readLine().split(" ");
                int from = Integer.parseInt(inp[0]);
                int to = Integer.parseInt(inp[1]);
                int cost = Integer.parseInt(inp[2]);
                makeEdge(from, to, cost);
                makeEdge(to, from, cost);
            }
            runMaxFlow(1, 2);
            int len = bfs2(1, 2, ans);
            for (int i = 0 ; i < len; ++i) if (!visited[ans[i][1]]) {
                System.out.printf("%d %d\n", ans[i][0], ans[i][1]);
            }
            System.out.println();
        }
    }
}
