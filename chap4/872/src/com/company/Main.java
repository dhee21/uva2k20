package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

public class Main {

    static boolean isAlreadyOnList(char character, int onList) {
        return ( onList & (1 << (character - 'A')) ) > 0;
    }

    static int addCharToOnList(char character, int onList) {
        return  onList |  (1 << (character - 'A')) ;
    }

    static boolean backtrack(int index, int len,int onList, char[] ansList, char[] variableList,
                             ArrayList<HashSet<Character>> shouldComeEarlier) {
        if (index == len) {
            for (int i = 0 ; i < len; ++i) System.out.print(i == 0 ? ansList[i] : (" " + ansList[i]));
            System.out.print("\n");
            return true;
        }

        boolean toReturn = false;
        for (int i = 0; i < len; ++i) {
            if (isAlreadyOnList(variableList[i], onList)) continue;
            char toPutAtThisIndex = variableList[i];

            boolean shouldPutHere = true;
            for (int k = 0 ; k < index; ++k) {
                HashSet<Character> earlierPeopleForK = shouldComeEarlier.get(ansList[k] - 'A');
                if (earlierPeopleForK != null && earlierPeopleForK.contains(toPutAtThisIndex)) {
                    shouldPutHere = false; break;
                }
            }

            if (shouldPutHere) {
                ansList[index] = toPutAtThisIndex;
                toReturn = toReturn | backtrack(index + 1, len,
                        addCharToOnList(toPutAtThisIndex, onList), ansList,
                        variableList, shouldComeEarlier);
            }
        }

        return toReturn;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        int tc;
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        tc = Integer.parseInt(rd.readLine());
        char[] variableList = new char[30];
        char[] ansList = new char[30];
        boolean firstCase = true;
        while (tc -- > 0) {

            if (!firstCase) System.out.println();
            firstCase = false;

            rd.readLine();
            String[] variables = rd.readLine().split(" ");
            Arrays.sort(variables);
            for (int i = 0; i < variables.length; ++i) variableList[i] = variables[i].charAt(0);

            ArrayList<HashSet<Character>> shouldComeEarlier = new ArrayList<>();
            for (int i = 0 ; i < 30; ++i) shouldComeEarlier.add( new HashSet<>() );


            String[] relations = rd.readLine().split(" ");
            for (String relation : relations) {
                String[] value = relation.split("<");
                shouldComeEarlier.get(value[1].charAt(0) - 'A').add(value[0].charAt(0));
            }


            boolean x = backtrack(0, variables.length, 0, ansList, variableList, shouldComeEarlier);
            if (!x) {
                System.out.println("NO");
            }

        }
    }
}
