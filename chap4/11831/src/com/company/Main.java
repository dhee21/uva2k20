package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        char [][] grid = new char[110][110];
        while (true) {
            int N, M, S;
            String[] input = rd.readLine().split(" ");
            N = Integer.parseInt(input[0]);
            M = Integer.parseInt(input[1]);
            S = Integer.parseInt(input[2]);
            if (N == 0 && M == 0 && S == 0) break;
            int startRow = 0, startCol = 0;
            char alignment = 'g';
            for (int r = 0; r < N; ++r) {
                String row = rd.readLine();
                for (int c = 0 ; c < M; ++c) {
                    grid[r][c] = row.charAt(c);
                    if (row.charAt(c) == 'N' || row.charAt(c) == 'S' || row.charAt(c) == 'L' || row.charAt(c) == 'O') {
                        startRow = r; startCol = c;
                        alignment = row.charAt(c);
                        grid[r][c] = '.';
                    }
                }
            }

            String commands = rd.readLine();
            Integer sticker = 0;
//            if (grid[startRow][startCol] == '*') sticker++;

            char[] direction = {'N', 'L', 'S', 'O'};
            int currentDirectionIndex = alignment == 'N'
                    ? 0 : alignment == 'S'
                    ? 2 : alignment == 'L'
                    ? 1 : 3;

            int presentRow = startRow, presentCol = startCol;
            for (int i = 0 ; i < S ; ++i) {
                char command = commands.charAt(i);

                switch (command) {
                    case 'D':
                        currentDirectionIndex = (currentDirectionIndex + 1) % 4;
                        break;
                    case 'E':
                        currentDirectionIndex = currentDirectionIndex == 0 ? 3 : currentDirectionIndex - 1;
                        break;
                    case 'F':
                        int newRow = presentRow, newCol = presentCol;
                        switch (direction[currentDirectionIndex]) {
                            case 'N':
                                newRow = presentRow - 1;
                                break;
                            case 'L':
                                newCol = presentCol + 1;
                                break;
                            case 'S':
                                newRow = presentRow + 1;
                                break;
                            case 'O':
                                newCol = presentCol - 1;
                                break;
                        }
                        if (newRow >= 0 && newRow < N && newCol >= 0 && newCol < M && grid[newRow][newCol] != '#') {
                            presentRow = newRow;
                            presentCol = newCol;
                            if (grid[presentRow][presentCol] == '*') {
                                sticker++;
                                grid[presentRow][presentCol] = '.';
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

            System.out.println(sticker);

        }
    }
}
