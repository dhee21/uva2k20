package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;

public class Main {
    static class Pair<A, B> {
        A first;
        B second;
        Pair(A a, B b) {
            first = a; second = b;
        }
    }

    static int bitsForLift = 3;
    static int SIZE = (1 << 10) - 1;
    static int timesOfLift[] = new int[10];
    static int nodes[] = new int[SIZE];
    static int numOfNodes;
    static int nodesOfFloorK[] = new int[10];
    static int numberOfNodesOfFloorK;
    static ArrayList<Pair<Integer, Integer>>[] adjList = new ArrayList[SIZE];
    static ArrayList<Integer>[] liftThatCanBeOnFloor = new ArrayList[110];
    static int[] indexOf = new int[SIZE];
    static int[] dist = new int[SIZE];

    static int[][] nextFloorOfLift = new int[10][110];
    static int[][] prevFloorOfLift = new int[10][110];

    static int getFloorFromNode(int node) {
        return node >> bitsForLift;
    }

    static int getLiftFromNode (int node) {
        return node & ((1 << bitsForLift) - 1);
    }

    static int getNode(int floor, int liftNum) {
        return  (floor << bitsForLift) + liftNum;
    }

    static void makeEdge (int node1, int node2, int weight) {
        adjList[getNodeNum(node1)].add(new Pair<>(getNodeNum(node2), weight));
    }

    static int makeNewNode (int floor, int liftNum) {
        int node = getNode(floor, liftNum);
        if (indexOf[node] == -1) {
            int newNodeIndex = numOfNodes++;
            indexOf[node] = newNodeIndex;
            nodes[newNodeIndex] = node;
        }
        return node;
    }

    static int getNodeNum (int node) {
        return indexOf[node];
    }

    static int getTime(int liftNum, int floor1, int floor2) {
        return timesOfLift[liftNum] * (floor2 - floor1);
    }

    static void makeEdgesForEachNodeStartingFromOne () {
        for (int i = 1; i < numOfNodes; ++i) {
            int node = nodes[i];
            int floor, lift;
            floor = getFloorFromNode(node); lift = getLiftFromNode(node);
            int nextFloor = nextFloorOfLift[lift][floor], prevFloor = prevFloorOfLift[lift][floor];

            if (nextFloor != -1) makeEdge(node, getNode(nextFloor, lift), getTime(lift, floor, nextFloor));
            if (prevFloor != -1) makeEdge(node, getNode(prevFloor, lift), getTime(lift, prevFloor, floor));
            ArrayList<Integer> liftsForThisFloor = liftThatCanBeOnFloor[floor];
            for (int j = 0; j < liftsForThisFloor.size(); ++j) {
                int possibleLiftOnThisFloor = liftsForThisFloor.get(j);
                if (possibleLiftOnThisFloor != lift) makeEdge(node,getNode(floor, possibleLiftOnThisFloor), 60);
            }
        }
    }

    static int getMinDistOfFloorK () {
        int minDist = Integer.MAX_VALUE;
        for (int i = 0 ; i < numberOfNodesOfFloorK; ++i) {
            int node = nodesOfFloorK[i];
            int nodeNum = getNodeNum(node);
            minDist = Math.min(dist[nodeNum], minDist);
        }
        return minDist;
    }

    static void init () {
        numberOfNodesOfFloorK = 0;
        numOfNodes = 0;
        Arrays.fill(indexOf, -1);
        for (int i = 0 ; i < SIZE; ++i) {
            if (adjList[i] == null) adjList[i] = new ArrayList<>();
            else adjList[i].clear();
        }
        for (int i = 0 ; i < 110; ++i) {
            if (liftThatCanBeOnFloor[i] == null) liftThatCanBeOnFloor[i] = new ArrayList<>();
            else liftThatCanBeOnFloor[i].clear();
        }
        for (int [] arr: nextFloorOfLift) Arrays.fill(arr, -1);
        for (int [] arr: prevFloorOfLift) Arrays.fill(arr, -1);
    }

    static void djakstra(int source) {
        Arrays.fill(dist, Integer.MAX_VALUE);
        dist[source] = 0;

        PriorityQueue<Pair<Integer, Integer>> pq = new PriorityQueue<>((a, b) -> a.first - b.first);
        for (int i = 0 ; i < numOfNodes; ++i) pq.add(new Pair<>(dist[i], i));

        while (!pq.isEmpty()) {
            Pair<Integer, Integer> pqObject = pq.poll();
            int distanceLabel = pqObject.first, node = pqObject.second;
            if (distanceLabel > dist[node] || distanceLabel == Integer.MAX_VALUE) continue;

            ArrayList<Pair<Integer, Integer>> neighbours = adjList[node];
            for (int i = 0 ; i < neighbours.size(); ++i) {
                int neighbour = neighbours.get(i).first, edgeWeight = neighbours.get(i).second;
                int possibleDistanceLableForNeighbour = dist[node] + edgeWeight;
                if (possibleDistanceLableForNeighbour < dist[neighbour]) {
                    dist[neighbour] = possibleDistanceLableForNeighbour;
                    pq.add(new Pair<>(possibleDistanceLableForNeighbour, neighbour));
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String inputString = rd.readLine();
            if (inputString == null) break;

            init();

            String[] inp = inputString.split(" ");
            int n = Integer.parseInt(inp[0]), k = Integer.parseInt(inp[1]);

            inp = rd.readLine().split(" ");
            for (int lift = 1; lift <= n; ++lift) timesOfLift[lift] = Integer.parseInt(inp[lift - 1]);

            int dummyNode = makeNewNode(0, 0);
            for (int lift = 1; lift <= n; ++lift) {
                inp = rd.readLine().split(" ");
                for (int i = 0; i < inp.length; ++i) {
                    int floorThisLiftCanGoTo = Integer.parseInt(inp[i]);
                    int nextFloorOfThisLift = i == (inp.length - 1) ? -1 : Integer.parseInt(inp[i + 1]);
                    int prevFloorOfThisLift = i == 0 ? -1 : Integer.parseInt(inp[i - 1]);
                    int newNodeVal = makeNewNode(floorThisLiftCanGoTo, lift);

                    if (floorThisLiftCanGoTo == 0) makeEdge(dummyNode, newNodeVal, 0);
                    if (floorThisLiftCanGoTo == k) nodesOfFloorK[numberOfNodesOfFloorK++] = newNodeVal;
                    nextFloorOfLift[lift][floorThisLiftCanGoTo] = nextFloorOfThisLift;
                    prevFloorOfLift[lift][floorThisLiftCanGoTo] = prevFloorOfThisLift;
                    liftThatCanBeOnFloor[floorThisLiftCanGoTo].add(lift);
                }
            }

            makeEdgesForEachNodeStartingFromOne();
            if (numberOfNodesOfFloorK == 0) {
                System.out.println("IMPOSSIBLE");
                continue;
            }

            djakstra(getNodeNum(dummyNode));

            int minDistOfFloorK = getMinDistOfFloorK();
            if (minDistOfFloorK == Integer.MAX_VALUE) System.out.println("IMPOSSIBLE");
            else System.out.println(minDistOfFloorK);
        }
    }
}
