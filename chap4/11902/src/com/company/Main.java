package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

public class Main {


    static void dfs(int node, int[][] adjMatrix, boolean[] visited, int n, int disabledNode) {
        visited[node] = true;
        for (int i = 0 ; i < n; ++i) {
            if (i != disabledNode && adjMatrix[node][i] == 1 && !visited[i]) {
                dfs(i, adjMatrix, visited, n, disabledNode);
            }
        }
    }

    static void printDashLine(int n) {
        System.out.print("+");
        for (int i = 0; i < (n << 1) - 1; ++i) System.out.print("-");
        System.out.print("+\n");
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(rd.readLine());

        for (int caseVal = 0; caseVal < tc; ++caseVal) {
            int adjMatrix[][] = new int[110][110], ans[][] = new int[110][110];

            int n = Integer.parseInt(rd.readLine());

            for (int r = 0 ; r < n; ++r) {
                String rowString[] = rd.readLine().split(" ");
                for (int c = 0 ; c < n; ++c) {
                    adjMatrix[r][c] = Integer.parseInt(rowString[c]);
                }
            }

            boolean visited[] = new boolean[110];

            dfs(0, adjMatrix, visited, n, -1);


            for (int node = 0; node < n; ++node) {
                if (visited[node]) ans[0][node] = 1;
            }
            for (int disNode = 1; disNode < n; ++disNode) {
                boolean[] visiteDis = new boolean[110];
                dfs(0, adjMatrix, visiteDis, n, disNode);
                for (int node = 0; node < n; ++node) {
                    if (visited[node] && !visiteDis[node]) ans[disNode][node] = 1;
                }
            }

            System.out.printf("Case %d:\n", caseVal + 1);
            printDashLine(n);
            for (int r = 0 ; r < n; ++r) {
                for (int c = 0; c < n; ++c) {
                   if (ans[r][c] == 1) System.out.print("|Y");
                   else  System.out.print("|N");
                }
                System.out.print("|\n");
                printDashLine(n);
            }
        }

    }
}
