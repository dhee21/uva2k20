package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

    static int MAXNODES = 1100;

    static int[][] adjList = new int[MAXNODES][MAXNODES];
    static int[] neighboursNum = new int[MAXNODES];


    static int[] owner = new int[MAXNODES];
    static boolean visited[] = new boolean[MAXNODES];

    static void init() {
        Arrays.fill(neighboursNum, 0);
    }

    static boolean canAugment(int node) {
        if (visited[node]) return false;
        visited[node] = true;
        int neighbours = neighboursNum[node];
        for (int i = 0 ; i < neighbours; ++i) {
            int neighbour = adjList[node][i];
            if (owner[neighbour] == -1 || canAugment(owner[neighbour])) {
                owner[neighbour] = node;
                return true;
            }
        }
        return false;
    }

    static int MCBM (int leftNodes) {
        Arrays.fill(owner, -1);
        int pairs = 0;
        for (int i = 0; i < leftNodes; ++i) {
            Arrays.fill(visited, false);
            if (canAugment(i)) pairs++;
        }
        return pairs;
    }

    static void makeEdge (int n1, int n2) {
        adjList[n1][neighboursNum[n1]++] = n2;
    }

    public static void main(String[] args) throws IOException {
        // write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        int n = Integer.parseInt(rd.readLine());

        int caseVal = 1;
        while (n-- > 0) {
            init();
            int h, w;
            String[] inp = rd.readLine().split(" ");
            h = Integer.parseInt(inp[0]);
            w = Integer.parseInt(inp[1]);

            for (int i = 0 ; i < h; ++i) {
                String input[] = rd.readLine().split(" ");
                for (int j = 0 ; j < w; ++j) {
                   int x = Integer.parseInt(input[j]);
                   if (x == 1) makeEdge(i, h + j);
                }
            }


            int pairs = MCBM(h);

            System.out.printf("Case %d: a maximum of %d nuts and bolts can be fitted together\n", caseVal++, pairs);
        }
    }
}
