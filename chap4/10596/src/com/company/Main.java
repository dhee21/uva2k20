package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.Arrays;

public class Main {

    static int MAXNODES = 10010;
    static int degree[] = new int[MAXNODES];
    static int[][] adjList = new int[MAXNODES][MAXNODES];
    static int[] neighbours= new int[MAXNODES];
    static boolean[] visited = new boolean[MAXNODES];
    static int nodesVisited;

    static void init () {
        Arrays.fill(degree, 0);
        Arrays.fill(neighbours, 0);
        Arrays.fill(visited, false);
        nodesVisited = 0;
    }

    static void makeEdge(int n1, int n2) {
        adjList[n1][neighbours[n1]++] = n2;
        adjList[n2][neighbours[n2]++] = n1;
        degree[n1]++;
        degree[n2]++;
    }

    static void dfs (int node) {
        visited[node] = true;
        nodesVisited ++;
        int neg = neighbours[node];
        for (int i = 0; i < neg; ++i) {
            int neighbour = adjList[node][i];
            if (!visited[neighbour]) dfs(neighbour);
        }
    }


    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            String input = rd.readLine();
            if (input == null) break;
            init();
            String inpString[] = input.split(" ");
            int n = Integer.parseInt(inpString[0]);
            int m = Integer.parseInt(inpString[1]);
            if (m == 0){
                System.out.println("Not Possible");
                continue;
            }
            for (int i = 0 ; i < m; ++i) {
                inpString = rd.readLine().split(" ");
                int from = Integer.parseInt(inpString[0]);
                int to = Integer.parseInt(inpString[1]);
                makeEdge(from, to);
            }

            boolean dfsDone = false;
            boolean impossible = false;
            for (int i = 0 ; i < n ; ++i) {
                if (degree[i] > 0 && !dfsDone) {
                    dfs(i); dfsDone = true;
                } else if (!visited[i] && degree[i] > 0 && dfsDone) {
                    impossible = true;
                    break;
                }
            }

            if (!impossible) {
                for (int i = 0 ; i < n; ++i) {
                    if (degree[i] % 2  == 1) {
                        impossible = true; break;
                    }
                }
            }

            if (impossible) {
                System.out.println("Not Possible");
            } else {
                System.out.println("Possible");
            }
        }
    }
}
