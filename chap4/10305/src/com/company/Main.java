package com.company;

import java.util.*;

public class Main {

    static int index = 0;

    static void dfs (int node, boolean [] visited, boolean[][] adjMatrix, Integer[] topsort, int n) {
        visited[node] = true;
        for (int i = 0 ; i < n; ++i) {
            if (!visited[i] && adjMatrix[node][i]) dfs(i, visited, adjMatrix, topsort, n);
        }
        topsort[index++] = node;
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);

        boolean adjMatrix[][] = new boolean[110][110];
        boolean visited[] = new boolean[110];
        Integer topsort[] = new Integer[110];

        while (true) {
            int n = sc.nextInt(), m = sc.nextInt();
            if (n == 0 & m == 0) break;

            for (boolean[] arr: adjMatrix) Arrays.fill(arr, false);
            Arrays.fill(visited, false);

            for (int i = 0 ; i < m; ++i) {
                int a = sc.nextInt(), b = sc.nextInt();
                adjMatrix[a - 1][b - 1] = true;
            }

            index = 0;
            for (int i = 0 ; i < n; ++i) if (!visited[i]) dfs(i, visited, adjMatrix, topsort, n);

           for (int i = n - 1 ; i >= 0 ; --i) {
               System.out.print(i == (n - 1) ? (topsort[i] + 1) : " " + (topsort[i] + 1));
           }
           System.out.println();

        }
    }
}
