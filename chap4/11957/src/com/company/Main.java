package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {

    static int WHITE = 1, BLACK = 2, DOT = 0;
    static int SIZE = 110;
    static int[][] grid = new int[SIZE][SIZE];
    static int mod = 1000007;
    static int [][] path = new int[SIZE][SIZE];

    static boolean canGo (int r, int c, int n) {
        return r >= 0 && r < n && c >= 0 && c < n && grid[r][c] == DOT;
    }

    static int add (int a, int b) {
        return (a % mod + b % mod) % mod;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(rd.readLine());

        for (int caseVal = 1; caseVal <= tc; ++caseVal) {
            int n = Integer.parseInt(rd.readLine());
            int stR = 0 , stC = 0;
            for (int i = 0 ; i < n; ++i) {
                String str = rd.readLine();
                for (int c = 0 ; c < n; ++c) {
                    if (str.charAt(c) == '.') grid[i][c] = DOT;
                    else if (str.charAt(c) == 'B') grid[i][c] = BLACK;
                    else if (str.charAt(c) == 'W') {
                        grid[i][c] = WHITE;
                        stR = i; stC = c;
                    }
                }
            }


            for (int[] arr: path) Arrays.fill(arr, 0);
            path[stR][stC] = 1;


            for (int row = stR; row >= 1; --row) {
                for (int col = 0 ; col < n; ++col) {
                    if (path[row][col] > 0) {
                        if (canGo(row - 1, col - 1, n)) path[row - 1][col - 1] = add(path[row - 1][col - 1], path[row][col]);
                        else if (canGo(row - 2, col - 2, n)) path[row - 2][col - 2] = add(path[row - 2][col - 2], path[row][col]);

                        if (canGo(row - 1, col + 1, n)) path[row - 1][col + 1] = add(path[row - 1][col + 1], path[row][col]);
                        else if (canGo(row - 2, col + 2, n)) path[row - 2][col + 2] = add (path[row - 2][col + 2], path[row][col]);
                    }
                }
            }

            int ans = 0;
            for (int col = 0; col < n; ++col) {
                if (path[0][col] != 0) ans = add(ans , path[0][col]);
            }

            System.out.printf("Case %d: %d\n", caseVal, ans);
        }
    }
}
