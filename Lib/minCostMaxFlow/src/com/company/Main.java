package com.company;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class Main {


    static int SIZE = 110;
    static long [][] capacity = new long[SIZE][SIZE], flow = new long[SIZE][SIZE];
    static long [][] cost = new long[SIZE][SIZE];
    static int [][] adjListResGraph = new int[SIZE][SIZE];
    static int[] neighboursNum = new int[SIZE];
    static long[] dist = new long[SIZE];
    static boolean[] visited = new boolean[SIZE];
    static int[] bfsParent = new int[SIZE];
    static long K;
    static long minCost;

    static void makeEdge (int from, int to, long time) {
        cost[from][to] = time;
        cost[to][from] = time;
        adjListResGraph[from][neighboursNum[from]++] = to;
        adjListResGraph[to][neighboursNum[to]++] = from;
    }

    static void init() {
//        for (long[] arr: capacity) Arrays.fill(arr, 0);
        for (long[] arr: flow) Arrays.fill(arr, 0);
        Arrays.fill(neighboursNum, 0);
        minCost = 0;
    }

    static long resCapacity(int from, int to) {
        return K - flow[from][to];
    }

    static long findCost(int from, int to) {
        return (flow[to][from] > 0 ? -cost[from][to] : cost[from][to]);
    }

    static boolean djakstra(int s, int t) {
        Arrays.fill(dist, Integer.MAX_VALUE);
        Arrays.fill(bfsParent, -1);
        PriorityQueue<long[]> pq = new PriorityQueue<>((a, b) -> a[0] <= b[0] ? -1 : 1);

        dist[s] = 0;
        pq.add(new long[]{0, s});

        while (!pq.isEmpty()) {
            long[] nodeData = pq.poll();
            int node = (int)nodeData[1];
            long nodeDist = nodeData[0];
            if (nodeDist <= dist[node]) {
                if (node == t) return true;
                int neighbourNum = neighboursNum[node];
                for (int i = 0 ; i < neighbourNum; ++i) {
                    int neighbour = adjListResGraph[node][i];
                    long resCap = resCapacity(node, neighbour);
                    if (resCap > 0 && (dist[node] + findCost(node, neighbour) < dist[neighbour])) {
                        dist[neighbour] = dist[node] + findCost(node, neighbour);
                        bfsParent[neighbour] = node;
                        pq.add(new long[] {dist[neighbour], neighbour});
                    }
                }
            }
        }
        return false;
    }

    static long augmentFlow(int s, int t, long D, long flowVal) {
        int node = t;
        long minResidualCap = Long.MAX_VALUE;
        while (bfsParent[node] != -1) {
            int parent = bfsParent[node];
            minResidualCap = Math.min(minResidualCap, resCapacity(parent, node));
            node = parent;
        }
        node = t;
        long totalFlowToAugment = flowVal + minResidualCap > D ? D - flowVal : minResidualCap;
        while (bfsParent[node] != -1) {
            int parent = bfsParent[node];
            flow[parent][node] += totalFlowToAugment;
            flow[node][parent] -= totalFlowToAugment;
            node = parent;
        }
        minCost += totalFlowToAugment * dist[t];
        return totalFlowToAugment;
    }

    static long runMaxFlow(int s, int t, int D) {
        long flow = 0;
        while (djakstra(s, t)) {
            flow += augmentFlow(s, t, D, flow);
            if (flow == D) break;
        }
        return flow;
    }


    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            int n, m;
            String stringInput = rd.readLine();
            if (stringInput == null) break;
            init();
            String inp[] = stringInput.split(" ");
            n = Integer.parseInt(inp[0]);
            m = Integer.parseInt(inp[1]);

            for (int i = 0 ; i < m; ++i) {
                inp = rd.readLine().split(" ");
                int from = Integer.parseInt(inp[0]);
                int to = Integer.parseInt(inp[1]);
                int time = Integer.parseInt(inp[2]);
                makeEdge(from, to, time);
            }
            int data;
            inp = rd.readLine().split(" ");
            data = Integer.parseInt(inp[0]);
            K = Integer.parseInt(inp[1]);

            long flow = runMaxFlow(1, n, data);
            if (flow == data) System.out.println(minCost);
            else System.out.println("Impossible.");

        }
    }
}
