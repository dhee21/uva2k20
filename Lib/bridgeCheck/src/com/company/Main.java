package com.company;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class Main {
    static class Pair {
        public int getKey() {
            return key;
        }

        int key;

        public int getValue() {
            return val;
        }

        int val;
        Pair(int x, int y) {
            key = x;
            val = y;
        }

    }


    static int bridgesNum , LEN = 1000;
    static Pair[] bridges = new Pair[LEN];
    static int time;


    static int dfs (int node, int root, int parent, boolean[] visited, boolean[][] adjMatrix, int n, int[] arrivaltime) {
        visited[node] = true;
        arrivaltime[node] = time++;

        int dbe = Integer.MAX_VALUE;

        for (int i = 0 ; i < n; ++i ) {
            if (adjMatrix[node][i]) {
                if (visited[i] && i != parent) {
                    dbe = Math.min(dbe, arrivaltime[i]);
                } else if (!visited[i]) {
                    dbe = Math.min(dbe, dfs(i, root, node, visited, adjMatrix, n, arrivaltime));
                }
            }
        }

        if (dbe >= arrivaltime[node] && node != root) {
            bridges[bridgesNum++] = new Pair(Math.min(node, parent), Math.max(node, parent));
        }
        return dbe;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        boolean [][] adjMatrix = new boolean[LEN][LEN];
        boolean [] visited = new boolean[LEN];
        int [] arrivalTime =  new int[LEN];

        while (true) {
            boolean isEnd = false;

            int numNodes = 0;
            while (true) {
                String inp = rd.readLine();
                if (inp == null) {
                    isEnd = true;
                    break;
                } else if (inp.length() > 0) {
                    numNodes = Integer.parseInt(inp);
                    break;
                }

            }
            if (isEnd) break;

            for (boolean[] arr: adjMatrix) Arrays.fill(arr, false);
            Arrays.fill(visited, false);
            Arrays.fill(arrivalTime, -1);


            if (numNodes == 0) {
                System.out.println("0 critical links\n");
                continue;
            }

            for (int i = 0 ; i < numNodes; ++i) {
                String[] edgeData = rd.readLine().split(" ");
                int parentNode = Integer.parseInt(edgeData[0]);
                for (int j = 2; j < edgeData.length; ++j) {
                    int childNode = Integer.parseInt(edgeData[j]);
                    adjMatrix[parentNode][childNode] = true;
                }
            }

            bridgesNum = 0;
            time = 0;
            for (int i = 0 ; i < numNodes; ++i) if (!visited[i]) dfs(i, i, -1,  visited, adjMatrix, numNodes, arrivalTime);

            Arrays.sort(bridges, 0, bridgesNum, (a, b) -> {
                int x = a.getKey() - b.getKey();
                if (x != 0) return  x;

                return a.getValue() - b.getValue();
            });

            System.out.printf("%d critical links\n", bridgesNum);
            for (int i = 0 ; i < bridgesNum; ++i) {
                System.out.println(bridges[i].getKey() + " - " + bridges[i].getValue());
            }
            System.out.println();
        }

    }
}
