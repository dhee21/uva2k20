package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    static int maxLen = 110;

    static int findMaxSumSubArrayCadens (int[] arr, int len) {
        int ans = Integer.MIN_VALUE;
        int st = 0;
        while (st < len) {
            int sum = arr[st];
            ans = Math.max(sum, ans);
            if (sum < 0) {
                 st++; continue;
            }
            int ed;
            for (ed = st + 1; ed < len; ++ed) {
                if (sum + arr[ed] <= 0) {
                    break;
                } else {
                    sum += arr[ed];
                    ans = Math.max(ans, sum);
                }
            }
            st = ed;
        }
        return ans;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(reader.readLine());

        int total = 0;
        int numbers[][] = new int[maxLen][maxLen];
        while (true) {
            String inp[] = reader.readLine().split(" ");
            for (int i = 0; i < inp.length; ++i) {
                numbers[total / n][total % n] = Integer.parseInt(inp[i]);
                total++;
            }
            if (total == n * n) break;
        }
        int cumulativeSumRow[][] = new int[maxLen][maxLen];
        for (int c = 0; c < n; ++c) {
            for (int r = 0 ; r < n; ++r) {
                cumulativeSumRow[r][c] = (c == 0 ? 0 : cumulativeSumRow[r][c - 1]) + numbers[r][c];
            }
        }

        int subArrToFindMaxSumFrom[] = new int[maxLen];
        int maxSum = Integer.MIN_VALUE;
        for (int c1 = 0; c1 < n; ++c1) {
            for (int c2 = c1; c2 < n; ++c2) {
                for (int r = 0 ; r < n; ++r) {
                    subArrToFindMaxSumFrom[r] = cumulativeSumRow[r][c2] - (c1 == 0 ? 0 : cumulativeSumRow[r][c1 - 1]);
                }
                int val = findMaxSumSubArrayCadens(subArrToFindMaxSumFrom, n);
                maxSum = Math.max(val, maxSum);
            }
        }
        System.out.println(maxSum);
    }
}
