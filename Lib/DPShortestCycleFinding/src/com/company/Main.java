package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

public class Main {

    static double dist[][][] = new double[25][25][25];
    static int parent[][][] = new int[25][25][25];
    static int N;
    static double minInfinity = Double.MIN_VALUE;
    static int ansNode, ansLen;
    static int ans[] = new int[29];

    static void runDP() {
        for (int len = 2; len <= N; ++len) for (int i = 0 ; i < N ; ++i)
            for (int j = 0 ; j < N; ++j) for (int k = 0 ; k < N; ++k) {
                if (k == j) continue;
                if (len == 1 && k == i) continue;
                if (dist[i][k][len - 1] * dist[k][j][1] > dist[i][j][len]) {
                    dist[i][j][len] = dist[i][k][len - 1] * dist[k][j][1];
                    parent[i][j][len] = k;
                    if (i == j && dist[i][j][len] > 1.01) {
                        ansLen = len; ansNode = i; return;
                    }
                }
            }
    }

    static void init() {
        ansNode = -1;
        ansLen = -1;
        for (double[][] tD: dist) for (double[] arr: tD) Arrays.fill(arr, minInfinity);
    }

    static void printPath () {
        int index = 0;
        int node = ansNode;
        ans[index++] = node + 1;
        for (int l = ansLen; l >= 1; --l){
            ans[index++] = parent[ansNode][node][l] + 1;
            node = parent[ansNode][node][l];
        }
        for (int i = index - 1; i >= 0; --i) {
            if (i == index - 1) System.out.print(ans[i]);
            else System.out.print(" " + ans[i]);
        }
        System.out.println();
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            N = sc.nextInt();
            init();

            for (int i = 0; i < N; ++i) {
                for (int j = 0; j < i; ++j) {
                    dist[i][j][1] = sc.nextDouble();
                    parent[i][j][1] = i;
                }
                for (int j = i + 1; j < N; ++j) {
                    dist[i][j][1] = sc.nextDouble();
                    parent[i][j][1] = i;
                }
            }
            runDP();
            if (ansLen == -1) {
                System.out.println("no arbitrage sequence exists");
            } else {
                printPath();
            }
        }
    }
}
