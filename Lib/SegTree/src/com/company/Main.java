package com.company;

import java.util.ArrayList;

public class Main {

    static class SegTree {
         static class TreeNode {
             int data;
         }
         ArrayList<TreeNode> tree;
         int treeSize, arrSize;

         SegTree(int[] arr, int sizeOfArr) {
             arrSize = sizeOfArr;
             int treeHeight = (int) ( Math.floor(Math.log(sizeOfArr) / Math.log(2.0)) + 1);
             treeSize = (int) Math.pow(2, treeHeight + 1);

             tree = new ArrayList<>(treeSize);
             for (int i = 0 ; i < treeSize; ++i) tree.add(new TreeNode());

             buildTree(arr, 1, 0,  sizeOfArr - 1);
         }

         void updateLeafNode (int id, int newValue) {
            tree.get(id).data = newValue;
         }

         TreeNode operation (TreeNode left, TreeNode right) {
            TreeNode combinationNode = new TreeNode();
            combinationNode.data = Math.min(left.data, right.data);
            return combinationNode;
         }

         void buildTree (int[] arr, int id, int st, int ed) {
            if (st > ed) return;

            if (st == ed) {
                updateLeafNode(id, arr[st]);
                return;
            }

            int mid = (st + ed) >> 1, twiceId = id << 1;
            buildTree(arr, twiceId, st, mid);
            buildTree(arr, twiceId + 1, mid + 1, ed);
            tree.set(id, operation(tree.get(twiceId), tree.get(twiceId + 1)));
         }

         void update (int id, int st, int ed, int index, int newVal) {
                if (st > ed || index < st || index > ed) return;

                if (st == ed) {
                    updateLeafNode(id, newVal);
                    return;
                }

                int mid = (st + ed) >> 1, twiceId = id << 1;
                update(twiceId, st, mid, index, newVal);
                update(twiceId + 1, mid + 1, ed, index, newVal);
                tree.set(id, operation(tree.get(twiceId), tree.get(twiceId + 1)));
         }

         TreeNode query (int id, int nodeStart, int nodeEnd, int toFindStart, int toFindEnd) {
             if (nodeStart > nodeEnd || toFindStart > toFindEnd || toFindStart > nodeEnd || toFindEnd < nodeStart) {
                 return null;
             }
             if (nodeStart >= toFindStart && nodeEnd <= toFindEnd) {
                 return tree.get(id);
             }

             int mid = (nodeStart + nodeEnd) >> 1, twiceId = id << 1;
             TreeNode left = query(twiceId, nodeStart, mid, toFindStart, toFindEnd);
             TreeNode right = query(twiceId + 1, mid + 1, nodeEnd, toFindStart, toFindEnd);
             if (left == null) return right;
             if (right == null) return left;
             return operation(left, right);
         }

         int doQuery (int st, int ed) {
               return query(1, 0, arrSize - 1, st, ed).data;
         }

         void doUpdate(int index, int newVal) {
             update(1, 0, arrSize - 1, index, newVal);
         }
    }

    public static void main(String[] args) {
	// write your code here

    }
}
