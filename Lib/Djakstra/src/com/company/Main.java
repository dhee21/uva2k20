package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.PriorityQueue;

public class Main {
    static class Pair<A, B> {
        A first;
        B second;
        Pair(A a, B b) {
            first = a; second = b;
        }
    }
    static int SIZE = 100;
    static int nodes[] = new int[SIZE];
    static int numOfNodes;
    static ArrayList<Pair<Integer, Integer>>[] adjList = new ArrayList[SIZE];
    static int[] indexOf = new int[SIZE];
    static int[] dist = new int[SIZE];

    static int getNode() {
    }

    static int getNodeNum (int node) {
        return indexOf[node];
    }

    static void makeEdge (int node1, int node2, int weight) {
        adjList[getNodeNum(node1)].add(new Pair<>(getNodeNum(node2), weight));
    }

    static int makeNewNode () {
        int node = getNode();
        if (indexOf[node] == -1) {
            int newNodeIndex = numOfNodes++;
            indexOf[node] = newNodeIndex;
            nodes[newNodeIndex] = node;
        }
        return node;
    }

    static void init () {
        numOfNodes = 0;
        Arrays.fill(indexOf, -1);
        for (int i = 0 ; i < SIZE; ++i) {
            if (adjList[i] == null) adjList[i] = new ArrayList<>();
            else adjList[i].clear();
        }
    }

    static void djakstra(int source) {
        Arrays.fill(dist, Integer.MAX_VALUE);
        dist[source] = 0;

        PriorityQueue<Pair<Integer, Integer>> pq = new PriorityQueue<>((a, b) -> a.first - b.first);
        for (int i = 0 ; i < numOfNodes; ++i) pq.add(new Pair<>(dist[i], i));

        while (!pq.isEmpty()) {
            Pair<Integer, Integer> pqObject = pq.poll();
            int distanceLabel = pqObject.first, node = pqObject.second;
            if (distanceLabel > dist[node] || distanceLabel == Integer.MAX_VALUE) continue;

            ArrayList<Pair<Integer, Integer>> neighbours = adjList[node];
            for (int i = 0 ; i < neighbours.size(); ++i) {
                int neighbour = neighbours.get(i).first, edgeWeight = neighbours.get(i).second;
                int possibleDistanceLableForNeighbour = dist[node] + edgeWeight;
                if (possibleDistanceLableForNeighbour < dist[neighbour]) {
                    dist[neighbour] = possibleDistanceLableForNeighbour;
                    pq.add(new Pair<>(possibleDistanceLableForNeighbour, neighbour));
                }
            }
        }
    }

    public static void main(String[] args) {
	// write your code here
    }
}
