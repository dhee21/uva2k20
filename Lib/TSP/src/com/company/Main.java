package com.company;

import java.util.Scanner;

public class Main {

    static int[] xCoor = new int[15],  yCoor = new int[15];
    static boolean isVisited (int node, int mask) {
        return (mask & (1 << node)) > 0;
    }

    static double distance(int node, int toReach) {
        int x =  xCoor[node] - xCoor[toReach];
        int y = yCoor[node] - yCoor[toReach];

        return Math.sqrt((x * x) + (y * y));
    }

    static int maskAfterVisitNode(int mask, int node) {
        return mask | (1 << node);
    }

    static int TSP(int numberOfNodes, double [][] dp, int [][]connection) {
        int allVisitedMask = (1 << numberOfNodes) - 1;
        for (int node = 0; node < numberOfNodes; ++node) {
            dp[allVisitedMask][node] = 0;
        }
        for (int mask = allVisitedMask - 1; mask > 0; --mask) {
            for (int node = 0; node < numberOfNodes; ++node) {

                if (!isVisited(node, mask)) dp[mask][node] = Double.MAX_VALUE;
                else {
                    double minVal = Double.MAX_VALUE;
                    int connectionNode = 0;
                    for (int toGo = 0; toGo < numberOfNodes; ++toGo) {
                        if (!isVisited(toGo, mask)) {
                            double minDistWithToGo = distance(node, toGo) + 16 +  dp[maskAfterVisitNode(mask, toGo)][toGo];
                            if (minDistWithToGo < minVal) {
                                minVal = minDistWithToGo;
                                connectionNode = toGo;
                            }
                        }
                    }
                    dp[mask][node] = minVal;
                    connection[mask][node] = connectionNode;
                }

            }
        }
        double ans = Double.MAX_VALUE;
        int startingNode = 0;
        for (int node  = 0; node < numberOfNodes; ++node) {
            int maskWithOnlyThisNodeVisited = 1 << node;
            if (dp[maskWithOnlyThisNodeVisited][node] < ans) {
                ans = dp[maskWithOnlyThisNodeVisited][node];
                startingNode = node;
            }
        }
        return startingNode;
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        double dp[][] = new double[1 << 10][10];
        int connection[][] = new int[1 << 10][10];
        int caseVal = 1;
        while (true) {
            int numOfNodes = sc.nextInt();
            if (numOfNodes == 0) break;
            int allVisitedMask = (1 << numOfNodes) - 1;
            for (int i = 0 ; i < numOfNodes; ++i) {
                xCoor[i] = sc.nextInt();
                yCoor[i] = sc.nextInt();
            }

            int startNode = TSP(numOfNodes, dp, connection);
            System.out.println("**********************************************************");
            System.out.printf("Network #%d\n", caseVal++);
            int presentNode = startNode;
            for (int mask = (1 << startNode); mask <  allVisitedMask; ) {
                int toConnect = connection[mask][presentNode];
                System.out.printf("Cable requirement to connect (%d,%d) to (%d,%d) is %.2f feet.\n",
                        xCoor[presentNode], yCoor[presentNode], xCoor[toConnect], yCoor[toConnect], 16 + distance(presentNode, toConnect));
                presentNode = toConnect;
                mask = mask | (1 << toConnect);
            }
            System.out.printf("Number of feet of cable required is %.2f.\n", dp[1 << startNode][startNode]);
        }
    }
}
