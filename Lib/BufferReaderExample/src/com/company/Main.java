package com.company;

import java.io.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Main {

    static class LazySegTree {
        static int[] bucaneer, barbary;
        static Integer[] lazyVal;
        static boolean[] toUpdate;

//        static TreeNode[] tree;
        int treeSize, arrSize;

        static int maxSize = 1024009;
//        static {
//            int treeHeight = (int) ( Math.ceil(Math.log(maxSize) / Math.log(2.0)));
//            int size = (int) Math.pow(2, treeHeight + 1);
////            tree = new TreeNode[size];
//            bucaneer = new int[size];
//            barbary = new int[size];
//            lazyVal = new Integer[size];
//            toUpdate = new boolean[size];
////            for (int i = 0 ; i < size; ++i) tree[i] = new TreeNode();
//        }

        LazySegTree(int[] arr, int sizeOfArr) {
            arrSize = sizeOfArr;
            int treeHeight = (int) ( Math.ceil(Math.log(sizeOfArr) / Math.log(2.0)));
            int size = (int) Math.pow(2, treeHeight + 1);
            bucaneer = new int[size];
            barbary = new int[size];
            lazyVal = new Integer[size];
            toUpdate = new boolean[size];
            buildTree(arr, 1, 0,  sizeOfArr - 1);
        }

        void updateLeafNode (int id, int newValue) {
//            TreeNode node = tree[id];
            if (newValue == 0) {
                barbary[id] = 1; bucaneer[id] = 0;
            } else {
                bucaneer[id] = 1;
                barbary[id] = 0;
            }
            lazyVal[id] = null;
            toUpdate[id] = false;
        }

        void propogate (int id, int st, int ed, int lazyValue) {
            toUpdate[id] = true;
            // updating lazyVal maynot be this straight forward in some cases

            if (lazyValue == 'I') {
                if (lazyVal[id] == null) lazyVal[id] = lazyValue;
                else if (lazyVal[id] == 'I') {
                    lazyVal[id] = null; toUpdate[id] = false;
                } else if (lazyVal[id] == 'F') lazyVal[id] = (int)'E';
                else if (lazyVal[id] == 'E') lazyVal[id] = (int)'F';
            } else lazyVal[id] = lazyValue;
        }

        void updateNode (int id, int st, int ed, int lazyValue) {
            if (st > ed) return;

            if (lazyValue == 'F') {
                bucaneer[id] = ed - st + 1;
                barbary[id] = 0;
            }
            if (lazyValue == 'E') {
                barbary[id] = ed - st + 1;
                bucaneer[id] = 0;
            }
            if (lazyValue == 'I') {
                int x = bucaneer[id];
                bucaneer[id] = barbary[id];
                barbary[id] = x;
            }

            if (st != ed) {
                int twiceId = id << 1, mid = (st + ed) >> 1;
                propogate(twiceId, st, mid, lazyValue);
                propogate(twiceId + 1, mid + 1, ed, lazyValue);
            }
            lazyVal[id] = null;
            toUpdate[id] = false;
        }

        void buildTree (int[] arr, int id, int st, int ed) {
            if (st > ed) return;

            if (st == ed) {
                updateLeafNode(id, arr[st]);
                return;
            }

            int mid = (st + ed) >> 1, twiceId = id << 1;
            buildTree(arr, twiceId, st, mid);
            buildTree(arr, twiceId + 1, mid + 1, ed);

            barbary[id] = barbary[twiceId] + barbary[twiceId + 1];
            bucaneer[id] = bucaneer[twiceId] + bucaneer[twiceId + 1];
            toUpdate[id] = false;
            lazyVal[id] = null;
//            tree[id] = operation(tree[twiceId], tree[twiceId + 1]);
        }

        void update (int id, int st, int ed, int stIndex,int edIndex, int newVal) {
            if (toUpdate[id]) updateNode(id, st, ed, lazyVal[id]);

            if (st > ed || stIndex > edIndex || edIndex < st || stIndex > ed) return;

            if (st >= stIndex && ed <= edIndex) {
                updateNode(id, st, ed, newVal);
                return;
            }

            int mid = (st + ed) >> 1, twiceId = id << 1;
            update(twiceId, st, mid, stIndex, edIndex, newVal);
            update(twiceId + 1, mid + 1, ed, stIndex, edIndex, newVal);
            barbary[id] = barbary[twiceId] + barbary[twiceId + 1];
            bucaneer[id] = bucaneer[twiceId] + bucaneer[twiceId + 1];

        }

        int query (int id, int nodeStart, int nodeEnd, int toFindStart, int toFindEnd) {
            if (nodeStart > nodeEnd || toFindStart > toFindEnd || toFindStart > nodeEnd || toFindEnd < nodeStart) {
                return -1;
            }
            if (toUpdate[id]) {
                updateNode(id, nodeStart, nodeEnd, lazyVal[id]);
            }
            if (nodeStart >= toFindStart && nodeEnd <= toFindEnd) {
                return bucaneer[id];
            }

            int mid = (nodeStart + nodeEnd) >> 1, twiceId = id << 1;
            int left = query(twiceId, nodeStart, mid, toFindStart, toFindEnd);
            int right = query(twiceId + 1, mid + 1, nodeEnd, toFindStart, toFindEnd);
            if (left == -1) return right;
            if (right == -1) return left;
            return left + right;
        }

        int doQuery (int st, int ed) {
            return query(1, 0, arrSize - 1, st, ed);
        }

        void doUpdate(int stIndex, int edIndex, int newVal) {
            update(1, 0, arrSize - 1, stIndex, edIndex, newVal);
        }
    }

    public static void main(String[] args) throws IOException {
	// write your code here

        BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
        StringBuffer output = new StringBuffer();
        BufferedWriter stdout = new BufferedWriter(new OutputStreamWriter(System.out));

        int tc = Integer.parseInt(sc.readLine());

        int arr[] = new int[1024009];
        for (int t = 0; t < tc; ++t) {
            int sets = Integer.parseInt(sc.readLine());
            int len = 0;
            while (sets -- > 0) {
                int times = Integer.parseInt(sc.readLine());
                String inputString = sc.readLine();
                while (times -- > 0) {
                    for (int i = 0 ; i < inputString.length(); ++i) arr[len++] = inputString.charAt(i) == '1' ? 1 : 0;
                }
            }

            LazySegTree tree = new LazySegTree(arr, len);

            int query = Integer.parseInt(sc.readLine());
            output.append("Case ").append(t + 1).append(":\n");

            int ans = 0;
            for (int q = 0; q < query; ++q) {
                StringTokenizer tzer = new StringTokenizer(sc.readLine());
                int type = tzer.nextToken().charAt(0);
                int st = Integer.parseInt(tzer.nextToken()) ;
                int ed = Integer.parseInt(tzer.nextToken()) ;
//                String[] queryVal = sc.readLine().split(" ");
//                int type = queryVal[0].charAt(0), st = Integer.parseInt(queryVal[1]), ed = Integer.parseInt(queryVal[2]);
                if (type != 'S') {
                    tree.doUpdate(st, ed, type);
                } else {
                    output.append("Q").append(++ans).append(": ").append(tree.doQuery(st, ed)).append("\n");
                }
            }
        }
        try {
            stdout.write(output.toString());
            stdout.flush();
            stdout.close();
        } catch (IOException ioe) {}
    }
}
