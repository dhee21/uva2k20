package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    static int stIndex, edIndex;
    static int findMaxSumSubArrayCadens (int[] arr, int len) {
        int ans = Integer.MIN_VALUE;
        int st = 0;
        while (st < len) {
            int sum = arr[st];
            if (sum > ans) {
                ans = sum;
                stIndex = st; edIndex = st;
            }
            if (sum < 0) {
                st++; continue;
            }

            int ed;
            for (ed = st + 1; ed < len; ++ed) {
                if (sum + arr[ed] < 0) {
                    break;
                } else {
                    sum += arr[ed];
                    if (sum > ans) {
                        ans = sum;
                        stIndex = st; edIndex = ed;
                    } else if (sum == ans) {
                        int distance = ed - st;
                        if (distance > edIndex - stIndex) {
                            stIndex = st; edIndex = ed;
                        }
                    }
                }
            }
            st = ed;
        }
        return ans;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(reader.readLine().trim());
        int niceness[] = new int[20100];
        int caseVal = 1;
        while (tc -- > 0) {
            int stops = Integer.parseInt(reader.readLine().trim());
            for (int i = 0 ; i < stops - 1; ++i) {
                niceness[i] = Integer.parseInt(reader.readLine().trim());
            }
            int ans = findMaxSumSubArrayCadens(niceness, stops - 1);
            if (ans <= 0) {
                System.out.printf("Route %d has no nice parts\n", caseVal++);
            } else {
                System.out.printf("The nicest part of route %d is between stops %d and %d\n", caseVal++,
                        stIndex + 1, edIndex + 2);
            }
        }
    }
}
