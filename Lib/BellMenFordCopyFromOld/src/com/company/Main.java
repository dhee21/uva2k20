package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;

public class Main {

    static int N, M;
    static int SIZE = 1100;
    static int INFINITY = Integer.MAX_VALUE;
    static int[][][] adjList = new int[SIZE][SIZE][2];
    static int[] neighboursNumOfNodes = new int[SIZE];
    static boolean hasNegativeCycle;
    static String[] nodes = new String[SIZE];
    static int nodeNum;
    static HashMap<String, Integer> indexOfNode;

    static int[][] dist = new int[SIZE][SIZE];

    static void init () {
        Arrays.fill(neighboursNumOfNodes, 0);
        nodeNum = 0;
        if (indexOfNode != null) indexOfNode.clear();
        else indexOfNode = new HashMap<>();
    }

    static void BellMenFord (int source) {
        Arrays.fill(dist[0], INFINITY);
        dist[0][source] = 0;

        hasNegativeCycle = false;

        for (int times = 0; times < N - 1; ++times) {
            for (int i = 0 ; i < N; ++i) dist[times + 1][i] = dist[times][i];
            for (int node = 0 ; node < N; ++node) {
                if (dist[times][node] != INFINITY) {
                    for (int i = 0; i < neighboursNumOfNodes[node]; ++i) {
                        int neighbourNode = adjList[node][i][0], weight = adjList[node][i][1];
                        if (dist[times][node] + weight < dist[times + 1][neighbourNode]) {
                            dist[times + 1][neighbourNode] = dist[times][node] + weight;
                            if (times == (N - 1)) hasNegativeCycle = true;
                        }
                    }
                }
            }
        }
    }

    static void makeNode (String nodeVal) {
        if (indexOfNode.containsKey(nodeVal)) return;
        int indexOfThisNode = nodeNum++;
        nodes[indexOfThisNode] = nodeVal;
        indexOfNode.put(nodeVal, indexOfThisNode);
    }
    static void makeEdge (String node1, String node2, int weight) {
        int from = indexOfNode.get(node1);
        int to = indexOfNode.get(node2);
        adjList[from][neighboursNumOfNodes[from]++] = new int[]{to, weight};
    }
    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int tc = Integer.parseInt(rd.readLine());
        int caseVal = 1;
        while (tc -- > 0) {
            rd.readLine();
            init();
            N = Integer.parseInt(rd.readLine());
            for (int i = 0 ; i < N; ++i) {
                String nodeVal = rd.readLine();
                makeNode(nodeVal);
            }
            M = Integer.parseInt(rd.readLine());
            for (int i = 0 ; i < M; ++i) {
                String[] inp = rd.readLine().split(" ");
                String from = inp[0], to = inp[1];
                int weight = Integer.parseInt(inp[2]);
                makeEdge(from, to, weight);
            }

            String[] queries = rd.readLine().split(" ");

            if (caseVal > 1) System.out.println();
            System.out.printf("Scenario #%d\n", caseVal++);
            BellMenFord(0);

            for (int i = 1; i < queries.length; ++i) {
                int stopOvers = Integer.parseInt(queries[i]);
                int pathLength = Math.min(stopOvers + 1, N - 1);
                if (dist[pathLength][N - 1] == INFINITY) System.out.println("No satisfactory flights");
                else System.out.printf("Total cost of flight(s) is $%d\n", dist[pathLength][N - 1]);
            }
        }
    }
}
