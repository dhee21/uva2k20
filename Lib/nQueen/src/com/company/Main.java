package com.company;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

    static int[][] queens = new int[100][10];
    static int validQueensNum = 0;

    public static void swap(Integer[] arr, int from, int to) {
        int toVal = arr[to];
        arr[to] = arr[from];
        arr[from] = toVal;
    }

    public static int findMinElementGreaterThan (Integer[] arr, Integer element, int startIndex) {
        int minElement = Integer.MAX_VALUE;
        int minElementIndex = arr.length;
        for (int i = startIndex; i < arr.length; ++i) {
            if (arr[i] > element && arr[i] < minElement) {
                minElementIndex = i;
                minElement = arr[i];
            }
        }
        return minElementIndex;
    }

    public static boolean nextPermutation(Integer[] arr) {
        int index = arr.length - 2;
        while (index >= 0 && arr[index] >= arr[index + 1]) index--;
        if (index < 0) return false;

        int indexToSwapTo = findMinElementGreaterThan(arr, arr[index], index + 1);
        swap(arr, index, indexToSwapTo);
        Arrays.sort(arr, index + 1, arr.length);
        return true;
    }

    static void calculateQueens() {
        Integer[] row = {0, 1, 2, 3, 4, 5, 6, 7};
        do {
            int diagonalsGoingDown = 0;
            int diagonalsGoinUp = 0;
            boolean validQueens = true;
            for (int i = 0; i < 8; ++i) {
                int rowOfColI = row[i];
                int diagonalDownOfQueenOfColI = i - rowOfColI + 7;
                int diagonalUpOfQueenOfColI = (7 - i) - rowOfColI + 7;
                boolean isDiagOccupied = (diagonalsGoingDown & (1 << diagonalDownOfQueenOfColI)) > 0;
                isDiagOccupied |= (diagonalsGoinUp & (1 << diagonalUpOfQueenOfColI)) > 0;
                if (isDiagOccupied) {
                    validQueens = false;
                    break;
                } else {
                    diagonalsGoingDown = diagonalsGoingDown | (1 << diagonalDownOfQueenOfColI);
                    diagonalsGoinUp = diagonalsGoinUp | (1 << diagonalUpOfQueenOfColI);
                }
            }
            if (validQueens) {
                for (int i = 0; i < 8; ++i) {
                    queens[validQueensNum][i] = row[i];
                }
                validQueensNum++;
            }
        } while (nextPermutation(row));
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        calculateQueens();
        int caseVal = 1;
        int rowPos[] = new int[8];
        while (sc.hasNextLine()) {
            for (int i = 0 ; i < 8; ++i) {
                rowPos[i] = sc.nextInt();
            }
            sc.nextLine();
            int min = 20;
            for (int i = 0 ; i < validQueensNum; ++i) {
                int moves = 0;
                for (int j = 0 ; j < 8; ++j){
                    if (queens[i][j] != (rowPos[j] - 1)) moves++;
                }
                min = Math.min(min, moves);
            }
            System.out.printf("Case %d: %d\n", caseVal++, min);
        }
    }
}
