package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Stack;

public class Main {

    static int SIZE = 30;
    static HashMap<String, Integer> nodeToIndex;
    static int numOfNodes;
    static int [] arrivalTime =  new int[SIZE];
    static boolean adjMatrix[][] = new boolean[SIZE][SIZE];
    static boolean[] visited = new boolean[SIZE];
    static String[] nodes;
    static int time;

    static Stack<Integer> nodesArrived;
    static boolean[] cutFromGraph = new boolean[SIZE];

    static void initGraph() {
        nodeToIndex = new HashMap<>();
        time = 0;
        numOfNodes = 0;
        for (boolean[] arr: adjMatrix) Arrays.fill(arr, false);
        Arrays.fill(visited, false);
        Arrays.fill(arrivalTime, -1);
        nodes = new String[SIZE];
        nodesArrived = new Stack<>();
        Arrays.fill(cutFromGraph, false);
    }

    static void makeNode (String nodeVal) {
        if (nodeToIndex.containsKey(nodeVal)) return;
        int newNodeIndex = numOfNodes++;
        nodes[newNodeIndex] = nodeVal;
        nodeToIndex.put(nodeVal, newNodeIndex);
    }

    static void makeEdge(String node1, String node2) {
        int indexOfNode1 = nodeToIndex.get(node1);
        int indexOfNode2 = nodeToIndex.get(node2);
        adjMatrix[indexOfNode1][indexOfNode2] = true;
    }

    static void printStronglyConnectedComponent (int node) {
        boolean firstTime = true;
        while (true) {
            int nodeVal = nodesArrived.pop();
            cutFromGraph[nodeVal] = true;
            if (firstTime) {
                System.out.print(nodes[nodeVal]); firstTime = false;
            } else {
                System.out.print(", " + nodes[nodeVal]);
            }
            if (nodeVal == node) break;
        }
        System.out.println();
    }

    static int stronglyConnectedComponents(int node) {
        visited[node] = true;
        arrivalTime[node] = time++;
        nodesArrived.add(node);

        int dbe = arrivalTime[node];
        for (int i = 0; i < numOfNodes; ++i) if (adjMatrix[node][i]) {
           if (!visited[i]) {
               dbe = Math.min(dbe, stronglyConnectedComponents(i));
           } else {
               if (!cutFromGraph[i]) dbe = Math.min(dbe, arrivalTime[i]);
           }
        }

        if (dbe == arrivalTime[node]) {
            printStronglyConnectedComponent(node);
        }
        return dbe;
    }

    public static void main(String[] args) throws IOException {
        // write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        int caseVal = 1;
        while (true) {
            int n, m;
            String[] nM = rd.readLine().split(" ");
            n = Integer.parseInt(nM[0]);
            m = Integer.parseInt(nM[1]);
            if (n == 0 && m == 0) break;

            initGraph();

            for (int i = 0 ; i < m; ++i) {
                String[] calling = rd.readLine().split(" ");
                makeNode(calling[0]);
                makeNode(calling[1]);
                makeEdge(calling[0], calling[1]);
            }


            if (caseVal > 1) System.out.println();
            System.out.printf("Calling circles for data set %d:\n", caseVal++);
            for (int i = 0 ; i < n; ++i) {
                if (!visited[i]) stronglyConnectedComponents(i);
            }
        }
    }
}
