package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;

public class Main {

    static int SIZE = 110;
    static HashMap<String, Integer> nodeToIndex;
    static int numOfNodes;
    static int [] arrivalTime =  new int[SIZE];
    static boolean adjMatrix[][] = new boolean[SIZE][SIZE];
    static boolean[] visited = new boolean[SIZE];
    static String[] nodes;
    static int articulationVertexes, time, articulationNodes[] = new int[SIZE];

    static void initGraph() {
        articulationVertexes = 0;
        nodeToIndex = new HashMap<>();
        numOfNodes = 0;
        for (boolean[] arr: adjMatrix) Arrays.fill(arr, false);
        Arrays.fill(visited, false);
        Arrays.fill(arrivalTime, -1);
        nodes = new String[SIZE];
    }

    static void makeNode (String nodeVal) {
        int newNodeIndex = numOfNodes++;
        nodes[newNodeIndex] = nodeVal;
        nodeToIndex.put(nodeVal, newNodeIndex);
    }

    static void makeEdge(String node1, String node2) {
        int indexOfNode1 = nodeToIndex.get(node1);
        int indexOfNode2 = nodeToIndex.get(node2);
        adjMatrix[indexOfNode1][indexOfNode2] = true;
        adjMatrix[indexOfNode2][indexOfNode1] = true;
    }


    static int articulationCheck(int node, int root, int n) {
        visited[node] = true;
        arrivalTime[node] = time++;


        int maxDeepestBackEdgeFromChildren = Integer.MIN_VALUE;
        int minDeepestBackEdgeFromChildren = Integer.MAX_VALUE;
        int deepestBackEdgeFromMe = arrivalTime[node];
        int childNum = 0;
        for (int i = 0 ; i < n; ++i) if (adjMatrix[node][i]) {
            if (!visited[i]) {
                childNum++;
                int x = articulationCheck(i, root, n);
                maxDeepestBackEdgeFromChildren = Math.max(maxDeepestBackEdgeFromChildren, x);
                minDeepestBackEdgeFromChildren = Math.min(minDeepestBackEdgeFromChildren, x);
            } else {
                deepestBackEdgeFromMe = Math.min(deepestBackEdgeFromMe, arrivalTime[i]);
            }
        }

        if (node == root) {
            if (childNum > 1) {
                articulationNodes[articulationVertexes++] = node;
                return arrivalTime[node];
            }
        } else {
            if (childNum > 0 && maxDeepestBackEdgeFromChildren >= arrivalTime[node]) {
                articulationNodes[articulationVertexes++] = node;
            }
        }

        return Math.min(minDeepestBackEdgeFromChildren, deepestBackEdgeFromMe);
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        String[] ans = new String[SIZE];
        int caseVal = 1;
        while (true) {
            int n = Integer.parseInt(rd.readLine().trim());
            if (n == 0) break;

            initGraph();

            for (int i = 0 ; i < n; ++i) {
                String nodeVal = rd.readLine();
                makeNode(nodeVal);
            }
            int m = Integer.parseInt(rd.readLine());
            while (m-- > 0) {
                String[] edgeData = rd.readLine().split(" ");
                makeEdge(edgeData[0], edgeData[1]);
            }

            for (int i = 0 ; i < n ; ++i) if (!visited[i]) {
                articulationCheck(i, i, n);
            }

            if (caseVal > 1) System.out.println();
            System.out.printf("City map #%d: %d camera(s) found\n", caseVal++, articulationVertexes);
            for (int i = 0 ; i < articulationVertexes; ++i) {
                ans[i] = nodes[articulationNodes[i]];
            }
            Arrays.sort(ans, 0, articulationVertexes);
            for (int i = 0; i < articulationVertexes; ++i) {
                System.out.println(ans[i]);
            }
        }
    }
}
