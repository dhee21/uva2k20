package com.company;

import java.util.ArrayList;

public class Main {

    static class LazySegTree {
        static class TreeNode {
            int data;
            Integer lazyVal;// represents information regarding what has been updated
            boolean toUpdate;
        }
        TreeNode[] tree;
        int treeSize, arrSize;

        LazySegTree(int[] arr, int sizeOfArr) {
            arrSize = sizeOfArr;
            int treeHeight = (int) ( Math.floor(Math.log(sizeOfArr) / Math.log(2.0)) + 1);
            treeSize = (int) Math.pow(2, treeHeight + 1);

            tree = new TreeNode[treeSize];
            for (int i = 0 ; i < treeSize; ++i) tree[i] = new TreeNode();

            buildTree(arr, 1, 0,  sizeOfArr - 1);
        }

        void updateLeafNode (int id, int newValue) {
            tree[id].data = newValue;
        }

        TreeNode operation (TreeNode left, TreeNode right) {
            TreeNode combinationNode = new TreeNode();
            combinationNode.data = Math.min(left.data, right.data);
            return combinationNode;
        }

        void propogate (int id, int st, int ed, int lazyVal) {
            TreeNode node = tree[id];
            node.toUpdate = true;
            // updating lazyVal maynot be this straight forward in some cases
            node.lazyVal = lazyVal;
        }

        void updateNode (int id, int st, int ed, int lazyVal) {
            if (st > ed) return;

            TreeNode node = tree[id];
            node.data = Math.min(node.data, lazyVal);

            if (st != ed) {
                int twiceId = id << 1, mid = (st + ed) >> 1;
                propogate(twiceId, st, mid, lazyVal);
                propogate(twiceId + 1, mid + 1, ed, lazyVal);
            }
            node.lazyVal = null;
            node.toUpdate = false;
        }

        void buildTree (int[] arr, int id, int st, int ed) {
            if (st > ed) return;

            if (st == ed) {
                updateLeafNode(id, arr[st]);
                return;
            }

            int mid = (st + ed) >> 1, twiceId = id << 1;
            buildTree(arr, twiceId, st, mid);
            buildTree(arr, twiceId + 1, mid + 1, ed);
            tree[id] = operation(tree[twiceId], tree[twiceId + 1]);
        }

        void update (int id, int st, int ed, int stIndex,int edIndex, int newVal) {
            TreeNode node = tree[id];
            if (node.toUpdate) updateNode(id, st, ed, tree[id].lazyVal);

            if (st > ed || stIndex > edIndex || edIndex < st || stIndex > ed) return;

            if (st >= stIndex && ed <= edIndex) {
                updateNode(id, st, ed, newVal);
                return;
            }

            int mid = (st + ed) >> 1, twiceId = id << 1;
            update(twiceId, st, mid, stIndex, edIndex, newVal);
            update(twiceId + 1, mid + 1, ed, stIndex, edIndex, newVal);
            tree[id] =  operation(tree[twiceId], tree[twiceId + 1]);
        }

        TreeNode query (int id, int nodeStart, int nodeEnd, int toFindStart, int toFindEnd) {
            if (nodeStart > nodeEnd || toFindStart > toFindEnd || toFindStart > nodeEnd || toFindEnd < nodeStart) {
                return null;
            }
            if (tree[id].toUpdate) {
                updateNode(id, nodeStart, nodeEnd, tree[id].lazyVal);
            }
            if (nodeStart >= toFindStart && nodeEnd <= toFindEnd) {
                return tree[id];
            }

            int mid = (nodeStart + nodeEnd) >> 1, twiceId = id << 1;
            TreeNode left = query(twiceId, nodeStart, mid, toFindStart, toFindEnd);
            TreeNode right = query(twiceId + 1, mid + 1, nodeEnd, toFindStart, toFindEnd);
            if (left == null) return right;
            if (right == null) return left;
            return operation(left, right);
        }

        int doQuery (int st, int ed) {
            return query(1, 0, arrSize - 1, st, ed).data;
        }

        void doUpdate(int stIndex, int edIndex, int newVal) {
            update(1, 0, arrSize - 1, stIndex, edIndex, newVal);
        }
    }

    public static void main(String[] args) {
        // write your code here

    }
}
