package com.company;

import java.util.ArrayList;

public class Main {

    ArrayList<Integer> tree;

    class FenwickTree {
        FenwickTree (int[] arr) {
            tree = new ArrayList<>(arr.length + 1);
            for (int i = 0 ; i <= arr.length; ++i) tree.set(i, 0);
            buildTree(arr);
        }

        int lsOne (int num){
            return num & -num;
        }

        void buildTree (int[] arr) {
            for (int i = 1; i <= arr.length; ++i) updateIndex(i, arr[i]);
        }

        int sum(int index) {
            if (index < 1 || index > tree.size()) return 0;

            int sum = 0;
            for ( ; index > 0; index -= lsOne(index)) {
                sum += tree.get(index);
            }
            return sum;
        }

        // one based index
        int rangeSumQuery (int st, int ed) {
            if (st > ed || st < 1 || ed > tree.size()) return 0;
            return sum(ed) - sum(st - 1);
        }

        void updateIndex(int index, int value) {
            for (;  index <= tree.size(); index += lsOne(index)) {
                tree.set(index, tree.get(index) + value);
            }
        }
    }

    public static void main(String[] args) {
	// write your code here
    }
}
