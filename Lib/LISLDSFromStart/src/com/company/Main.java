package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {
    static int LISArray[] = new int[5000];
    static int LDSArray[] = new int[5000];

    static int calculateLISLen (int[] height, int[] width, int buildNum) {
        int ans = 0;
        for (int i = 0 ; i < buildNum; ++i) {
            int lisEndingHere = width[i];
            for (int j = 0; j < i; ++j) {
                if (height[j] < height[i]) {
                    lisEndingHere = Math.max(lisEndingHere, LISArray[j] + width[i]);
                }
            }
            LISArray[i] = lisEndingHere;
            ans = Math.max(ans, lisEndingHere);
        }
        return ans;
    }

    static int calculateLDSLen (int[] height, int[] width, int buildNum) {
        int ans = 0;
        for (int i = 0 ; i < buildNum; ++i) {
            int ldsEndingHere = width[i];
            for (int j = 0; j < i; ++j) {
                if (height[j] > height[i]) {
                    ldsEndingHere = Math.max(ldsEndingHere, LDSArray[j] + width[i]);
                }
            }
            LDSArray[i] = ldsEndingHere;
            ans = Math.max(ans, ldsEndingHere);
        }
        return ans;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int t = Integer.parseInt(reader.readLine());
        int height[] = new int[5000];
        int width[] = new int[5000];
        for (int c = 0  ; c < t; ++c) {
            int buildNum = Integer.parseInt(reader.readLine());

            String[] inp = reader.readLine().split(" ");
            for (int i = 0 ; i < inp.length; ++i) {
                height[i] = Integer.parseInt(inp[i]);
            }
            inp = reader.readLine().split(" ");
            for (int i = 0 ; i < inp.length; ++i) {
                width[i] = Integer.parseInt(inp[i]);
            }

            int lis = calculateLISLen(height, width, buildNum);
            int lds = calculateLDSLen(height, width, buildNum);
            if (lis >= lds) {
                System.out.printf("Case %d. Increasing (%d). Decreasing (%d).\n", c + 1, lis, lds);
            } else {
                System.out.printf("Case %d. Decreasing (%d). Increasing (%d).\n", c + 1, lds, lis);
            }
        }
    }
}
