package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class Main {
    static int SIZE = 45;
    static int [][] capacity = new int[SIZE][SIZE], flow = new int[SIZE][SIZE];
    static int [][] adjListResGraph = new int[SIZE][SIZE];
    static int[] neighboursNum = new int[SIZE];
    static int SOURCE = 0, SINK = 37;
    static boolean[] visited = new boolean[SIZE];
    static int[] bfsParent = new int[SIZE];

    static void makeEdge (int from, int to, int cap) {
        capacity[from][to] = cap;
        if (capacity[to][from] == 0) {
            adjListResGraph[from][neighboursNum[from]++] = to;
            adjListResGraph[to][neighboursNum[to]++] = from;
        }
    }

    static void init() {
        for (int[] arr: capacity) Arrays.fill(arr, 0);
        for (int[] arr: flow) Arrays.fill(arr, 0);
        Arrays.fill(neighboursNum, 0);

        for (int i = 0; i < 10; ++i) {
            makeEdge(27 + i, 37, 1);
        }
    }

    static int resCapacity(int from, int to) {
        return capacity[from][to] - flow[from][to];
    }

    static boolean bfs(int s, int t) {
        Arrays.fill(visited, false);
        Arrays.fill(bfsParent, -1);
        Queue<Integer> q = new LinkedList<>();

        visited[s] = true;
        q.add(s);

        while (!q.isEmpty()) {
            int node = q.poll();
            if (node == t) return true;
            for (int i = 0; i < neighboursNum[node]; ++i) {
                int neighbour = adjListResGraph[node][i];
                int resCap = resCapacity(node, neighbour);
                if (!visited[neighbour] && resCap > 0) {
                    visited[neighbour] = true;
                    bfsParent[neighbour] = node;
                    q.add(neighbour);
                }
            }
        }

        return false;
    }

    static int augmentFlow(int s, int t) {
        int node = t;
        int minResidualCap = Integer.MAX_VALUE;
        while (bfsParent[node] != -1) {
            int parent = bfsParent[node];
            minResidualCap = Math.min(minResidualCap, resCapacity(parent, node));
            node = parent;
        }
        node = t;
        while (bfsParent[node] != -1) {
            int parent = bfsParent[node];
            flow[parent][node] += minResidualCap;
            flow[node][parent] -= minResidualCap;
            node = parent;
        }
        return minResidualCap;
    }

    static int runMaxFlow(int s, int t) {
        int flow = 0;
        while (bfs(s, t)) {
            flow += augmentFlow(s, t);
        }
        return flow;
    }

    static void printAns() {
        char[] ans = new char[15];
        Arrays.fill(ans, '_');
        for (int i = 0; i < 10; ++i) {
            int toNode = 27 + i;
            for (int j = 0; j < 26; ++j) {
                int fromNode = j + 1;
                if (flow[fromNode][toNode] > 0) {
                    ans[i] = (char) (j + 'A'); break;
                }
            }
        }
        for (int i = 0 ; i < 10; ++i) System.out.print(ans[i]);
        System.out.println();
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String inp;
            int totalApplications = 0;
            init();

            while (true) {
                inp = rd.readLine();
                if (inp == null || inp.isEmpty()) break;
                int nodeCharIndex = inp.charAt(0) - 'A';
                int cap = inp.charAt(1) - '0';
                totalApplications += cap;
                int fromNode = nodeCharIndex + 1;
                makeEdge(0, fromNode, cap);
                for (int i = 3; i < inp.length() - 1; ++i) {
                    int toNode = 27 + inp.charAt(i) - '0';
                    makeEdge(fromNode, toNode, 1);
                }
            }
            if (totalApplications > 10) { System.out.println("!");}
            else {
                int maxFlow = runMaxFlow(SOURCE, SINK);
                if (maxFlow != totalApplications) { System.out.println("!");}
                else printAns();
            }

            if (inp == null) break;
        }
    }
}
