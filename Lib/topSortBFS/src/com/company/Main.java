package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        int caseVal = 1;
        while (true) {
            String x = rd.readLine();
            if (x == null) break;
            int n = Integer.parseInt(x);

            HashMap<String, Integer> alcoholToNode = new HashMap<>();
            String[] nodeToAlcohol = new String[110];
            ArrayList<ArrayList<Integer>> adjList = new ArrayList<>();

            int[] indegree = new int[110];
            for (int i = 0 ; i < n; ++i) {
                String alcohol = rd.readLine();
                alcoholToNode.put(alcohol, i);
                nodeToAlcohol[i] = alcohol;
                adjList.add(new ArrayList<>());
            }

            int m = Integer.parseInt(rd.readLine());
            for (int i = 0 ; i < m; ++i) {
                String[] edge = rd.readLine().split(" ");
                adjList.get(alcoholToNode.get(edge[0])).add(alcoholToNode.get(edge[1]));
                indegree[alcoholToNode.get(edge[1])]++;
            }

            PriorityQueue<Integer> pq = new PriorityQueue<>();

            for (int i = 0 ; i < n; ++i) {
                if (indegree[i] == 0) {
                    pq.add(i);
                }
            }

            System.out.printf("Case #%d: Dilbert should drink beverages in this order:", caseVal++);

            while (!pq.isEmpty()) {
                int node = pq.poll();
                System.out.print(" " + nodeToAlcohol[node]);
                ArrayList<Integer> adjNodes = adjList.get(node);
                for (int i = 0; i < adjNodes.size(); ++i) {
                    int adjacentNode = adjNodes.get(i);
                    indegree[adjacentNode]--;
                    if (indegree[adjacentNode] == 0) pq.add(adjacentNode);
                }
            }
            System.out.println(".\n");
            rd.readLine();
        }

    }
}
