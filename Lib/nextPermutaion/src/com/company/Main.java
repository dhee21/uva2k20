package com.company;

import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;


public class Main {

    public static void swap(Character[] arr, int from, int to) {
        char toVal = arr[to];
        arr[to] = arr[from];
        arr[from] = toVal;
    }

    public static int findMinElementGreaterThan (Character[] arr, Character element, int startIndex) {
        char minElement = 'z' + 5;
        int minElementIndex = arr.length;
        for (int i = startIndex; i < arr.length; ++i) {
            if (isGreater(arr[i] , element) && isSmaller(arr[i] , minElement)) {
                minElementIndex = i;
                minElement = arr[i];
            }
        }
        return minElementIndex;
    }

    public static boolean isSmaller(Character val, Character from) {
        return !isGreaterOrEqual(val, from);
    }

    public static boolean isGreater(Character val, Character from) {
        char valLower = Character.toLowerCase(val);
        char fromLower = Character.toLowerCase(from);

        if (valLower != fromLower) {
            return valLower > fromLower;
        }
        return val > from;
    }

    public static boolean isGreaterOrEqual (Character val, Character from) {
        if (isGreater(val, from)) return  true;
        return val == from;
    }

    public static boolean nextPermutation(Character[] arr) {
        int index = arr.length - 2;
        while (index >= 0 && isGreaterOrEqual(arr[index], arr[index + 1])) index--;
        if (index < 0) return false;

        int indexToSwapTo = findMinElementGreaterThan(arr, arr[index], index + 1);
        swap(arr, index, indexToSwapTo);
        Arrays.sort(arr, index + 1, arr.length,  new Comparator<Character>() {
            @Override
            public int compare (Character left, Character right) {
                if (isSmaller(left, right)) return  -1;
                if (left == right) return 0;
                return 1;
            }
        });
        return true;
    }

    public static void main(String[] args) {
	// write your code here
        int tc;
        Scanner sc = new Scanner(System.in);
        tc = Integer.parseInt(sc.nextLine());
        while (tc-- > 0) {
            String input = sc.nextLine();
            Character[] sorted = new Character[input.length()];
            char[] charArr = input.toCharArray();
            for (int i = 0; i < charArr.length; ++i) sorted[i] = charArr[i];

            Arrays.sort(sorted, new Comparator<Character>() {
                @Override
                public int compare (Character left, Character right) {
                    if (isSmaller(left, right)) return  -1;
                    if (left == right) return 0;
                    return 1;
                }
            });



            while (true) {
                for (Character character : sorted) {
                    System.out.print(character);
                }
                System.out.println();
                boolean isNext = nextPermutation(sorted);
                if (!isNext) break;
            }
        }
    }
}
