package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    static int [][] adjList = new int[5555][5555];
    static int [] neighbours = new int[5555];
    static int[] dfsParent = new int[5555];
    static boolean[] visited = new boolean[5555];

    static void init () {
        Arrays.fill(neighbours, 0);
        Arrays.fill(dfsParent, 0);
        Arrays.fill(visited, false);
    }

    static void makeEdge(int from, int to) {
        adjList[from][neighbours[from]++] = to;
        adjList[to][neighbours[to]++] = from;
    }

    static void dfs(int node) {
        visited[node] = true;

        int neighbour = neighbours[node];
        for (int i = 0 ; i < neighbour; ++i) {
            int ngh = adjList[node][i];
            if (!visited[ngh]) {
                dfsParent[ngh] = node;
                dfs(ngh);
            }
        }
    }

    public static void main(String[] args) throws IOException {
	// write your code

        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            int n = Integer.parseInt(rd.readLine());
            if (n == 0) break;
            init();
            for (int i = 0 ; i < n - 1; ++i) {
                String[] inp = rd.readLine().split(" ");
                int from = Integer.parseInt(inp[0]);
                int to = Integer.parseInt(inp[1]);
                makeEdge(from, to);
            }
            dfs(1);
            String inputString = rd.readLine();
            int l = Integer.parseInt(inputString);
            for (int i = 0 ; i < l; ++i) {
                String[] inp = rd.readLine().split(" ");
                int from = Integer.parseInt(inp[0]);
                int to = Integer.parseInt(inp[1]);
                ArrayList<Integer> one = new ArrayList<>() , two = new ArrayList<>();

                int node = from;
                while (node != 0) {
                    one.add(node);
                    node = dfsParent[node];
                }
                node = to;
                while (node != 0) {
                    two.add(node);
                    node = dfsParent[node];
                }

                ArrayList<Integer> comb = new ArrayList<>();
                int index1 = one.size() - 1, index2 = two.size() - 1;

                while (index1 >= 0 && index2 >= 0 && (int)one.get(index1) == (int)two.get(index2)) {
                    index1--; index2--;
                }

                for (int j = 0 ; j <= index1; ++j) comb.add(one.get(j));
                comb.add(one.get(index1 + 1));
                for (int j = index2 ; j >= 0; --j) comb.add(two.get(j));

                int sizeVal = comb.size();
                int half = sizeVal >> 1;
                if (sizeVal % 2 == 0) {
                    int x = comb.get(half), y = comb.get(half - 1);
                    System.out.printf("The fleas jump forever between %d and %d.\n", Math.min(x, y), Math.max(x, y));
                } else {
                    System.out.printf("The fleas meet at %d.\n", comb.get(half));
                }

            }
        }
    }
}
