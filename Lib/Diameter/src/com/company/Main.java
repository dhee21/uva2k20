package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

public class Main {

    static int SIZE  = 3000;
    static int[][] adjList = new int[SIZE][SIZE];
    static int[] neighbours = new int[SIZE];
    static boolean[][] isEdge = new boolean[SIZE][SIZE];
    static boolean[] visited = new boolean[SIZE];
    static int [] dfsParent = new int[SIZE];

    static void init () {
        Arrays.fill(neighbours, 0);
    }

    static void makeEdge (int a, int b) {
        adjList[a][neighbours[a]++] = b;
        adjList[b][neighbours[b]++] = a;
        isEdge[a][b] = true;
        isEdge[b][a] = true;
    }

    static void removeEdge(int a, int b) {
        isEdge[a][b] = false;
        isEdge[b][a] = false;
    }
    static void addEdge(int a, int b) {
        isEdge[a][b] = true;
        isEdge[b][a] = true;
    }

    static int bfs(int node) {
        Queue<Integer> q = new LinkedList<>();

        visited[node] = true;
        q.add(node);

        int oneNode = 0;
        while (!q.isEmpty()) {
            oneNode = q.poll();
            for (int i = 0 ; i < neighbours[oneNode]; ++i) {
                int neighbour = adjList[oneNode][i];
                if (isEdge[oneNode][neighbour] && !visited[neighbour]) {
                    dfsParent[neighbour] = oneNode;
                    visited[neighbour] = true;
                    q.add(neighbour);
                }
            }

        }
        return oneNode;
    }

    static int findDiameter(int[] arr, int root) {
        Arrays.fill(dfsParent, -1);
        Arrays.fill(visited, false);
        int newRoot = bfs(root);

        Arrays.fill(dfsParent, -1);
        Arrays.fill(visited, false);

        int node = bfs(newRoot);
        int index = 0;
        while (node != -1) {
            arr[index++] = node;
            node = dfsParent[node];
        }
        return index;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        int tc;

        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));

        tc = Integer.parseInt(rd.readLine());

        while (tc -- > 0) {
            init();
            int n = Integer.parseInt(rd.readLine());
            for (int i = 0 ; i < n - 1; ++i) {
                String [] inp = rd.readLine().split(" ");
                makeEdge(Integer.parseInt(inp[0]), Integer.parseInt(inp[1]));
            }

            int[] diameterOfTree  = new int[SIZE];
            int size = findDiameter(diameterOfTree, 1);

            int[] nodesToRemove = new int[2];
            int[] nodesToAdd = new int[2];
            int newMinimumDiameter = Integer.MAX_VALUE;
            for (int i = 0 ; i < size - 1; ++i) {
                removeEdge(diameterOfTree[i], diameterOfTree[i + 1]);
                int[] leftDiameter = new int[SIZE];
                int leftSize = findDiameter(leftDiameter, diameterOfTree[i]);
                int[] rightDiamter = new int[SIZE];
                int rightSize = findDiameter(rightDiamter, diameterOfTree[i + 1]);


                int fromLeft = ((leftSize - 1) % 2) > 0 ? ((leftSize - 1) / 2) + 1 : (leftSize - 1) / 2;
                int fromRight = ((rightSize - 1) % 2) > 0 ? ((rightSize - 1) / 2) + 1 : (rightSize - 1) / 2;


                int finalSize = fromLeft + fromRight + 1;
                int finalDia = Math.max(finalSize, Math.max(leftSize - 1, rightSize - 1));
                if (finalDia < newMinimumDiameter) {
                    newMinimumDiameter = finalDia;
                    nodesToRemove[0] = diameterOfTree[i]; nodesToRemove[1] = diameterOfTree[i + 1];
                    nodesToAdd[0] = leftDiameter[leftSize >> 1];
                    nodesToAdd[1] = rightDiamter[rightSize >> 1];
                }
                addEdge(diameterOfTree[i], diameterOfTree[i + 1]);
            }

            System.out.println(newMinimumDiameter);
            System.out.println(nodesToRemove[0] + " " +  nodesToRemove[1]);
            System.out.println(nodesToAdd[0] + " " + nodesToAdd[1]);
        }



    }
}
