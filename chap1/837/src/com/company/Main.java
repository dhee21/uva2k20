package com.company;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
	// write your code here
        while (true) {
            int days, day, month, year;
            days = sc.nextInt();
            day = sc.nextInt();
            month = sc.nextInt();
            year = sc.nextInt();

            if (day == 0 && days == 0 && month == 0 && year == 0) break;

            GregorianCalendar cal = new GregorianCalendar(year, month - 1, day);
            cal.add(Calendar.DATE, days);

            System.out.println(cal.get(Calendar.DAY_OF_MONTH) + " " + (cal.get(Calendar.MONTH) + 1) + " " + cal.get(Calendar.YEAR));
        }
    }
}
