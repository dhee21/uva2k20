package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = sc .nextInt();

        while (tc-- > 0) {
            int [] fromTo = new int[110];
            for (int i = 0; i < 110; ++i) fromTo[i] = i;
            int players, numbOfLadders, diceRolls;

            players = sc.nextInt();
            numbOfLadders = sc.nextInt();
            diceRolls = sc.nextInt();
//            sc.nextLine();

            int [] position = new int[players + 10];
            for (int i = 0 ; i < players; ++i) {
                position[i] = 1;
            }

            while (numbOfLadders -- > 0) {
                int from = sc.nextInt();
                int to = sc.nextInt();
//                sc.nextLine();
                fromTo[from] = to;
            }

            boolean gameOver = false;

            for (int i = 0 ; i < diceRolls; ++i) {
                int diceVal = sc.nextInt();
                if (gameOver) continue;
                position[i%players] = position[i%players] + diceVal;

                if (position[i%players] >= 100) {
                    position[i%players] = 100; gameOver = true;
                } else {
                  position[i%players] =  fromTo[position[i%players]];
                  if (position[i%players] == 100) gameOver = true;
                }
            }

            for (int i = 0 ; i < players; ++i) {
                System.out.println("Position of player " + (i + 1) + " is "+ position[i] + ".");
            }
        }
    }
}
