package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            int participants = sc.nextInt();
            int budget = sc.nextInt();
            int hotels = sc.nextInt();
            int weekends = sc.nextInt();
            sc.nextLine();
//            System.out.println(participants + " " + budget + " " + hotels + " " + weekends);
            int minCost = Integer.MAX_VALUE;
            while (hotels-- > 0) {
                int cost = Integer.valueOf(sc.nextLine());
//                System.out.println(cost);
                for (int i = 0 ; i < weekends; ++i) {
                    int beds = sc.nextInt();
//                    System.out.println(beds);
                    if (beds >= participants) {
                        minCost = Math.min(minCost, participants * cost);
                    }
                }
                sc.nextLine();
            }
            if (minCost > budget) {
                System.out.println("stay home");
            } else {
                System.out.println(minCost);
            }
        }
    }
}
