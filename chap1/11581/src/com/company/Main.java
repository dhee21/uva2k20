package com.company;

import java.util.HashMap;
import java.util.Scanner;

public class Main {

    public static int [][] gridCopy = new int[3][3];

    public static int convert (int[][] grid) {
        int ans = 0;
        for (int r = 0; r < 3; ++r) {
            for (int c = 0 ; c < 3; ++c) {
                ans = ans * 10 + grid[r][c];
            }
        }
        return ans;
    }

    public static boolean isOkay (int r, int c) {
        return r >= 0 && r < 3 && c >= 0 && c < 3;
    }

    public static void trasform (int[][] grid) {
        for (int r = 0; r < 3; ++r) {
            for (int c = 0 ; c < 3; ++c) {
                int sum = 0;
                if (isOkay(r - 1, c)) sum += grid[r - 1][c];
                if (isOkay(r + 1, c)) sum += grid[r + 1][c];
                if (isOkay(r, c - 1)) sum += grid[r][c - 1];
                if (isOkay(r, c + 1)) sum += grid[r][c + 1];
                gridCopy[r][c] = sum % 2;
            }
        }

        for (int r = 0 ; r < 3 ; ++r) {
            for (int c = 0 ; c < 3; ++c) {
                grid[r][c] = gridCopy[r][c];
            }
        }
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = Integer.parseInt(sc.nextLine());
        int [][] grid = new int[3][3];
        while (tc-- > 0) {
            HashMap<Integer, Integer> isPresent = new HashMap<>();
            sc.nextLine();

            for (int r = 0 ; r < 3 ; ++r) {
                String inp = sc.nextLine();
                for (int c = 0 ; c < 3; ++c) {
                    grid[r][c] = inp.charAt(c) - '0';
                }
            }

            int index =  0;
            int valFirst = convert(grid);
            isPresent.put(valFirst, index++);

             while (true) {
                 trasform(grid);
                 int val = convert(grid);
                 if (isPresent.containsKey(val) ) {
                     System.out.println(isPresent.get(val) - 1);
                     break;
                 }
                 int toPut = convert(grid);
                 isPresent.put(toPut, index++);
             }
        }
    }
}
