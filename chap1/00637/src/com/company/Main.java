package com.company;

import java.util.Scanner;

public class Main {
    public static class Face {
        public int left, right;
    }

    public static class Sheet {
        public Face front, back;
    }

    public static void main(String[] args) {
	// write your code here

        Scanner sc = new Scanner(System.in);
        while (true) {
            int pages = sc.nextInt();
            if (pages == 0) break;

            int numOfSheets = pages % 4 == 0 ? pages / 4 : (pages / 4) + 1;
            Sheet[] sheets = new Sheet[numOfSheets + 5];
            for (int i = 0 ; i < numOfSheets + 5; ++i) {
                sheets[i] = new Sheet();
                sheets[i].front = new Face();
                sheets[i].back = new Face();
            }
            int pageNo = 0;

            for (int sheet = 0 ; sheet < numOfSheets; ++sheet) {
                sheets[sheet].front.right = ++pageNo;
                if (pageNo == pages) break;
                sheets[sheet].back.left = ++pageNo;
                if (pageNo == pages) break;
            }

            for (int sheet = numOfSheets - 1; sheet >= 0; --sheet) {
                if (pageNo == pages) break;
                sheets[sheet].back.right = ++pageNo;
                if (pageNo == pages) break;
                sheets[sheet].front.left = ++pageNo;
            }

            System.out.println("Printing order for " + pages + " pages:");
            for (int i = 0; i < numOfSheets; ++i) {
               if (sheets[i].front.left > 0 || sheets[i].front.right > 0) {
                   String leftVal = sheets[i].front.left == 0 ? "Blank" : String.valueOf(sheets[i].front.left);
                   String rightVal = sheets[i].front.right == 0 ? "Blank" : String.valueOf(sheets[i].front.right);
                   System.out.println("Sheet " + (i + 1) + ", front: " + (leftVal) + ", " + (rightVal));
               }
                if (sheets[i].back.left > 0 || sheets[i].back.right > 0) {
                    String leftVal = sheets[i].back.left == 0 ? "Blank" : String.valueOf(sheets[i].back.left);
                    String rightVal = sheets[i].back.right == 0 ? "Blank" : String.valueOf(sheets[i].back.right);
                    System.out.println("Sheet " + (i + 1) + ", back : " + (leftVal) + ", " + (rightVal));
                }
            }

        }
    }
}
