package com.company;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeMap;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = Integer.valueOf(sc.nextLine());
        TreeMap<String, Integer> countryToNumber = new TreeMap<>();
        while (tc != 0) {
            String line = sc.nextLine();
            String country = line.split(" ")[0];
            int value = countryToNumber.getOrDefault(country, 0);
            countryToNumber.put(country, ++value);
            tc--;
        }
        Iterator<String> it = countryToNumber.keySet().iterator();

        while (it.hasNext()) {
            String key = it.next();
            System.out.println(key + " " + countryToNumber.get(key));
        }
    }
}
