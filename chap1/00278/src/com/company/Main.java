package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int tc;
        Scanner sc = new Scanner(System.in);
        tc = sc.nextInt();
        sc.nextLine();

        while (tc-- > 0) {
            char type = sc.next().charAt(0);
            int row = sc.nextInt();
            int col = sc.nextInt();
            sc.nextLine();
            int ans = 0;
            switch (type) {
                case 'r':
                    ans = Math.min(row, col);
                    break;
                case 'k':
                    ans = (int) Math.ceil((double) row / 2) * (int) Math.ceil((double) col / 2);
                    ans += (int) Math.floor((double) row / 2) * (int) Math.floor((double) col / 2);
                    break;
                case 'Q':
                    ans = Math.min(row, col);
                    break;
                case 'K':
                    ans = (int) Math.ceil((double) row / 2) * (int) Math.ceil((double) col / 2);;
                    break;
            }
            System.out.println(ans);
        }

    }
}
