package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {


    public static int calculateIndex (int r, int c, int k) {
        int rank = 0;
        rank += r * k;
        rank += c + 1;
        return rank - 1;
    }

    public static boolean checkIfMagic (ArrayList<Character> list, int k) {
        int index = 0;
        for (int c = 0; c < k; ++c) {
            for (int r = 0 ; r < k; ++r) {
                int indexToCheck = calculateIndex(r, c, k);
                if (list.get(index++) != list.get(indexToCheck)) return false;
            }
        }
        index = 0;
        for (int c = k - 1; c >= 0; --c) {
            for (int r = k - 1; r >= 0; --r) {
                int indexToCheck = calculateIndex(r, c, k);
                if (list.get(index++) != list.get(indexToCheck)) return false;
            }
        }
        index = 0;
        for (int r = k - 1; r >= 0; --r) {
            for (int c = k - 1; c >= 0; --c) {
                int indexToCheck = calculateIndex(r, c, k);
                if (list.get(index++) != list.get(indexToCheck)) return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = Integer.parseInt(sc.nextLine());
        HashMap<Integer, Integer> sqrt = new HashMap<>();
        for (int i = 1; i < 101; ++i) {
            sqrt.put(i*i, i);
        }

        for (int caseVal = 1; caseVal <= tc; ++caseVal) {
            String input = sc.nextLine();
            ArrayList<Character> list = new ArrayList<>();
            for (int i = 0 ; i < input.length(); ++i) {
                if (input.charAt(i) >= 'a' && input.charAt(i) <= 'z') {
                    list.add(input.charAt(i));
                }
            }

            System.out.println("Case #"+ caseVal + ":");

            if (sqrt.containsKey(list.size())) {
                int k = sqrt.get(list.size());
//                System.out.println("hete" + k);

                boolean ifMagic = checkIfMagic(list, k);
                if (ifMagic) {
                    System.out.println(k);
                } else {
                    System.out.println("No magic :(");
                }
            } else {
                System.out.println("No magic :(");
            }

        }
    }
}
