package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = sc.nextInt();
        while (tc -- > 0){
            int sum = sc.nextInt();
            int diff = sc.nextInt();

            if (((sum + diff) & 1 ) > 0) {
                System.out.println("impossible");
                continue;
            } else if (((sum - diff) & 1 ) > 0) {
                System.out.println("impossible");
                continue;
            }

            int score1 = (sum + diff) >> 1;
            int score2 = (sum - diff) >> 1;

            if (score1 >= 0 && score2 >= 0) {
                System.out.println(score1 + " " + score2);
            } else {
                System.out.println("impossible");
            }
        }
    }
}
