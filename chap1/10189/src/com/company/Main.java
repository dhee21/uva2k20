package com.company;

import java.util.Scanner;

public class Main {

    public static char [][] field = new char[110][110];
    public static int [][] fieldBombs = new int[110][110];
    public static int[] rowAdd = {-1, -1, -1, 0, 0, 1 , 1, 1};
    public static int[] colAdd = {-1, 0, 1, -1, 1 , -1, 0, 1};
    public static void  init (int row, int col) {
        for (int r = 0; r < row ; ++r) {
            for (int c = 0; c < col; ++c) {
                fieldBombs[r][c] = 0;
            }
        }
    }

    public static boolean isValid (int rows, int cols, int r, int c) {
        return r >= 0 && r < rows && c >= 0 && c < cols;
    }
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
	// write your code here
        int caseVal = 0;
        while (true) {
            int row, col;
            row = sc.nextInt();
            col = sc.nextInt(); sc.nextLine();
            if (row == 0 && col == 0) break;


            if (caseVal > 0) System.out.println();
            caseVal++;
            init(row, col);

            for (int r = 0; r < row ; ++r) {
                String rowVal = sc.nextLine();
                for (int c = 0; c < col; ++c) {
                    field[r][c] = rowVal.charAt(c);
                }
            }


            for (int r = 0; r < row ; ++r) {
                for (int c = 0; c < col; ++c) {
                   if (field[r][c] != '*') continue;

                    for (int i = 0 ; i < 8; ++i) {
                        int rowValue = r + rowAdd[i];
                        int colValue = c + colAdd[i];

                        if (isValid(row, col, rowValue, colValue)) {
                            fieldBombs[rowValue][colValue]++;
                        }
                    }
                }
            }

            System.out.println("Field #"+caseVal+":");
            for (int r = 0; r < row ; ++r) {
                for (int c = 0; c < col; ++c) {
                   if (field[r][c] == '*') System.out.print("*");
                   else System.out.print(fieldBombs[r][c]);
                }
                System.out.println();
            }
        }
    }
}
