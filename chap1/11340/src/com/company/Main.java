package com.company;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.StringTokenizer;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int tc = Integer.parseInt(br.readLine());
//        System.out.println(tc);

        while (tc-- > 0) {
            HashMap<Character, Integer> map = new HashMap<>();
            String valString = br.readLine();
//            System.out.println(valString);
            int k = Integer.parseInt(valString);
            while (k -- > 0) {
                String inp[] = br.readLine().split(" ");
                char c = inp[0].charAt(0);
                int val = Integer.parseInt(inp[1]);
                map.put(c, val);
            }
            int m = Integer.parseInt(br.readLine());
            int cents = 0;
            while (m -- > 0) {
                String essayLine = br.readLine();
                for (int i = 0 ; i < essayLine.length(); ++i) {
                    char c = essayLine.charAt(i);
                    cents += map.getOrDefault(c, 0);
                }
            }

            System.out.println((cents / 100) + "." + ((cents % 100) < 10 ? ("0" + (cents % 100)) : cents % 100) + "$");
        }
    }
}
