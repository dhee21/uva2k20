package com.company;

import javax.xml.stream.events.Characters;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Scanner;

public class Main {

    public static void swap(Character[] arr, int from, int to) {
        char toVal = arr[to];
        arr[to] = arr[from];
        arr[from] = toVal;
    }

    public static int findMinElementGreaterThan (Character[] arr, Character element, int startIndex) {
        char minElement = 'z' + 5;
        int minElementIndex = arr.length;
        for (int i = startIndex; i < arr.length; ++i) {
            if ((arr[i] > element) && (arr[i] < minElement)) {
                minElementIndex = i;
                minElement = arr[i];
            }
        }
        return minElementIndex;
    }

    public static boolean nextPermutation(Character[] arr) {
        int index = arr.length - 2;
        while (index >= 0 && arr[index] >= arr[index + 1]) index--;
        if (index < 0) return false;

        int indexToSwapTo = findMinElementGreaterThan(arr, arr[index], index + 1);
        swap(arr, index, indexToSwapTo);
        Arrays.sort(arr, index + 1, arr.length);
        return true;
    }

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader rd = new BufferedReader(new InputStreamReader(System.in));
        while (true) {
            String inp = rd.readLine();
            if (inp.length() == 1 && inp.charAt(0) == '#') break;

            Character[] array = new Character[inp.length()];
            for (int i = 0 ; i < array.length; ++i) array[i] = inp.charAt(i);

            boolean isNext = nextPermutation(array);

            if (isNext) {
                for (Character character : array) {
                    System.out.print(character);
                }
                System.out.println();
            } else {
                System.out.println("No Successor");
            }
        }
    }
}
