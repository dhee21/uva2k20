package com.company;

import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int tc ;
        Scanner sc = new Scanner(System.in);
        tc = Integer.parseInt(sc.nextLine());
        int caseVal = 0;
        while (tc-- > 0) {
            caseVal++;
            String input = sc.nextLine();
            int month, day, year;
            month = Integer.parseInt(input.substring(0, 2));
            day = Integer.parseInt(input.substring(2, 4));
            year = Integer.parseInt(input.substring(4, 8));

            GregorianCalendar c = new GregorianCalendar(year, month - 1, day);
            c.add(Calendar.WEEK_OF_YEAR, 40);

            int dateVal = c.get(Calendar.DATE);
            int monthVal = c.get(Calendar.MONTH);
            int yearVal = c.get(Calendar.YEAR);

            int [] dates = {21, 20, 21, 21,22,22, 23,22,24, 24,23,23};
            String[] zodiac = {"aquarius", "pisces", "aries", "taurus", "gemini", "cancer", "leo",
            "virgo", "libra", "scorpio", "sagittarius", "capricorn"};

            int zodiacIndex = dateVal >= dates[monthVal] ? monthVal: monthVal - 1;
            if (zodiacIndex == -1) zodiacIndex = 11;

           System.out.printf("%d %02d/%02d/%04d %s\n", caseVal, monthVal + 1, dateVal , yearVal, zodiac[zodiacIndex]);
        }
    }
}
