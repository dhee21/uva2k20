package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) throws IOException {
	// write your code here
        BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));

        while (true) {
            String inp = sc.readLine();
            if (inp == null) break;
            LinkedList<Character> list = new LinkedList<>();
            int indexToAdd = -1;
            for (int i = 0; i < inp.length(); ++i) {
               if (inp.charAt(i) == '[') {
                   indexToAdd = 0;
               } else if (inp.charAt(i) == ']') {
                   indexToAdd = -1;
               } else {
                   if (indexToAdd == -1) {
                       list.add(inp.charAt(i));
                   } else {
                       list.add(indexToAdd++, inp.charAt(i));
                   }
               }
            }

            StringBuilder sb = new StringBuilder();

            for (Character c : list) {
                sb.append(c);
            }
            System.out.println(sb.toString());
        }
    }
}
