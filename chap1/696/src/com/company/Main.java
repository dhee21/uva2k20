package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (true) {
            int row, col;
            row = sc.nextInt();
            col = sc.nextInt();
            sc.nextLine();
            int ans;
            if (row == 0 && col == 0) break;
            if (row == 1) {
                ans = col;
            } else if (col == 1) {
                ans = row;
            } else if (row == 2) {
                int val = col % 4;
                ans = (col / 4) * 2 + (val < 3  ? val : 2);
                ans <<= 1;
            } else if (col == 2){
                int val = row % 4;
                ans = (row / 4) * 2 + (val < 3  ? val : 2);
                ans <<= 1;
            } else {
                ans = (int) Math.ceil((double) row / 2) * (int) Math.ceil((double) col / 2);
                ans += (int) Math.floor((double) row / 2) * (int) Math.floor((double) col / 2);
            }
            System.out.println(ans + " knights may be placed on a " + row + " row " + col + " column board.");
        }
    }
}
