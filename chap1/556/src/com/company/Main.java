package com.company;

import javafx.geometry.Pos;

import java.util.Scanner;

public class Main {


    static int numOfRows, numOfCols;
    static char directions[] = {'N', 'E', 'S', 'W'};


    public static class Position {
        public int row, col;
        char dir;
        Position (int r, int c, char d) {
            row = r; col = c; dir = d;
        }
    }

    public static class Check {
        boolean canGoForward, isWallOnRight;
    }

    public static Position moveForward (Position p) {
        int col = p.col, row = p.row;
        char direction = p.dir;
            if (direction == 'E') {
                col++;
            }
            if (direction == 'W') {
                col--;
            }
            if (direction == 'N') {
                row--;
            }
            if (direction == 'S') {
                row++;
            }
        return new Position(row, col, direction);

    }

    public static boolean isOkay (int r, int c) {
       return  r >= 0 && r < numOfRows && c >= 0 && c < numOfCols;
    }

    public static Position getRightBoxPosition (Position p) {
        char newDir = turnRight(p.dir);
        Position rightBox = moveForward(new Position(p.row, p.col, newDir));
        return rightBox;
    }

    public static char turnRight (char d) {
        return d == 'N' ? 'E' : d == 'E' ? 'S' : d == 'S' ? 'W' : 'N';
    }
    public static char turnLeft (char d) {
        return d == 'N' ? 'W' : d == 'W' ? 'S' : d == 'S' ? 'E' : 'N';
    }

    public static Check checkThings (int [][] grid, Position pos) {
       Position p = moveForward(pos);
       Check c  = new Check();
       c.canGoForward = isOkay(p.row, p.col) &&  grid[p.row][p.col] == 0;
       if (c.canGoForward) {
           Position rightBox = getRightBoxPosition(p);
           c.isWallOnRight = !isOkay(rightBox.row, rightBox.col) || grid[rightBox.row][rightBox.col] == 1;
       }
       return c;
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (true) {
            String inp[] = sc.nextLine().split(" ");
            numOfRows = Integer.parseInt(inp[0]);
            numOfCols = Integer.parseInt(inp[1]);

            if (numOfRows == 0 && numOfRows == 0) break;

            int grid [][] = new int[numOfRows + 1][numOfCols + 1];
            int out [][] = new int[numOfRows + 1][numOfCols + 1];
            int in [][] = new int[numOfRows + 1][numOfCols + 1];
            for (int r = 0; r < numOfRows; ++r){
                String val = sc.nextLine();
                for(int c = 0; c < numOfCols; ++c) {
                    grid[r][c] = val.charAt(c) - '0';
                }
            }



            Position p = new Position(numOfRows - 1, 0, 'E');



            boolean starting = true;
            while (true) {
                if (!starting && p.row == numOfRows - 1 && p.col == 0) break;
                starting = false;

                Check c = checkThings(grid, p);
                boolean canGoForward = c.canGoForward;
                boolean isWallOnRight = c.isWallOnRight;

                if (canGoForward && isWallOnRight) {
                    out[p.row][p.col]++;
                    p = moveForward(p);
                    in[p.row][p.col]++;
                }
                if (canGoForward && !isWallOnRight) {
                    out[p.row][p.col]++;
                    p = moveForward(p);
                    in[p.row][p.col]++;
                    p.dir = turnRight(p.dir);
                }
                if (!canGoForward) {
                    p.dir = turnLeft(p.dir);
                }
            }

            int[] freq = new int[5];
            for (int r = 0; r < numOfRows; ++r) for(int c = 0; c < numOfCols; ++c) {
                if (grid[r][c] != 0) continue;
                int timesMovedOnThisPosition = Math.min(out[r][c], in[r][c]);
                freq[timesMovedOnThisPosition]++;
            }
            for (int i = 0 ; i < 5; ++i) {
                int val = freq[i];
                String toPrint = (val >= 10 && val < 100) ? (" " + val) : (val < 10) ? ("  " + val) : ("" + val);
                System.out.print(toPrint);
            }
            System.out.println();
        }
    }
}
