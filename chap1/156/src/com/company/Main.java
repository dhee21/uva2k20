package com.company;

import java.util.*;

public class Main {

    public static void main(String[] args) {
	// write your code here
        HashMap<String , ArrayList<String>> map = new HashMap<>();

        Scanner sc = new Scanner(System.in);

        while (true) {
            String wordsString = sc.nextLine();
            if (wordsString.charAt(0) == '#') break;
            String[] words = wordsString.split(" ");

            for (String word : words) {
                String lowerCase = word.toLowerCase();
                char [] arr = lowerCase.toCharArray();
                Arrays.sort(arr);
               lowerCase = String.valueOf(arr);
                if (map.containsKey(lowerCase)) {
                    map.get(lowerCase).add(word);
                } else {
                    ArrayList<String> toAdd = new ArrayList<>();
                    toAdd.add(word);
                    map.put(lowerCase, toAdd);
                }
            }
        }
        ArrayList<String> ans = new ArrayList<>();
        for (String sorted : map.keySet()) {
            ArrayList<String> list = map.get(sorted);
            if (list.size() == 1) {
                ans.add(list.get(0));
            }
        }
        Collections.sort(ans);

        for (String st : ans) {
            System.out.println(st);
        }
    }
}
