package com.company;

import java.util.Scanner;

public class Main {

    public static char[][] chessBoard = new char[8][8];

    public static boolean isValid (int r, int c) {
        return r >= 0 && r < 8 && c >= 0 && c < 8;
    }

    public static boolean isEmpty (int r, int c) {
        return chessBoard[r][c] == '1' || chessBoard[r][c] == '0';
    }

    public static void goLeftAndCheck(int r, int c, boolean isOneStep) {
        while (isValid(r, --c) && isEmpty(r, c)) {
                chessBoard[r][c] = '1';
            if (isOneStep) return;
        }
    }

    public static void goRightAndCheck (int r, int c, boolean isOneStep) {
        while (isValid(r, ++c) && isEmpty(r, c)) {
                chessBoard[r][c] = '1';
            if (isOneStep) return;
        }
    }

    public static void goAboveAndCheck (int r, int c, boolean isOneStep) {
        while (isValid(--r, c) && isEmpty(r, c)) {
                chessBoard[r][c] = '1';
            if (isOneStep) return;
        }
    }

    public static void goDownAndCheck (int r, int c, boolean isOneStep) {
        while (isValid(++r, c) && isEmpty(r, c)) {
                chessBoard[r][c] = '1';
            if (isOneStep) return;
        }
    }

    public static void goLeftUpDiagonalAndCheck (int r, int c, boolean isOneStep) {
        while (isValid(--r, --c) && isEmpty(r, c)) {
                chessBoard[r][c] = '1';
            if (isOneStep) return;
        }
    }

    public static void goRightUpDiagonalAndCheck (int r, int c, boolean isOneStep) {
        while (isValid(--r, ++c) && isEmpty(r, c)) {
                chessBoard[r][c] = '1';
            if (isOneStep) return;
        }
    }

    public static void goLeftDownDiagonalAndCheck (int r, int c, boolean isOneStep) {
        while (isValid(++r, --c) && isEmpty(r, c)) {
                chessBoard[r][c] = '1';
            if (isOneStep) return;
        }
    }

    public static void goRightDownDiagonalAndCheck (int r, int c, boolean isOneStep) {
        while (isValid(++r, ++c) && isEmpty(r, c)) {
                chessBoard[r][c] = '1';
            if (isOneStep) return;
        }
    }




    public static void makeMove(char goti, int row, int col) {
        if (goti == 'r' || goti == 'R') {
            goAboveAndCheck(row, col, false);
            goDownAndCheck(row, col, false);
            goLeftAndCheck(row, col, false);
            goRightAndCheck(row, col, false);
            return;
        }
        if (goti == 'n' || goti == 'N') {
            int rowAdd[] = {-2, -2, 2, 2, -1, 1, -1, 1};
            int colAdd[] = {-1, 1, -1, 1, 2, 2, -2, -2};

            for (int i = 0; i < 8; ++i) {
                int nightRow = row + rowAdd[i];
                int nightCol = col + colAdd[i];
                if (isValid(nightRow, nightCol) && isEmpty(nightRow, nightCol)) {
                    chessBoard[nightRow][nightCol] = '1';
                }
            }

            return;
        }
        if (goti == 'b' || goti == 'B') {
            goLeftUpDiagonalAndCheck(row, col, false);
            goLeftDownDiagonalAndCheck(row, col, false);
            goRightUpDiagonalAndCheck(row, col, false);
            goRightDownDiagonalAndCheck(row, col, false);
            return;
        }
        if (goti == 'q' || goti == 'Q') {
            goAboveAndCheck(row, col, false);
            goDownAndCheck(row, col, false);
            goLeftAndCheck(row, col, false);
            goRightAndCheck(row, col, false);
            goLeftUpDiagonalAndCheck(row, col, false);
            goLeftDownDiagonalAndCheck(row, col, false);
            goRightUpDiagonalAndCheck(row, col, false);
            goRightDownDiagonalAndCheck(row, col, false);
            return;
        }
        if (goti == 'k' || goti == 'K') {
            goAboveAndCheck(row, col, true);
            goDownAndCheck(row, col, true);
            goLeftAndCheck(row, col, true);
            goRightAndCheck(row, col, true);
            goLeftUpDiagonalAndCheck(row, col, true);
            goLeftDownDiagonalAndCheck(row, col, true);
            goRightUpDiagonalAndCheck(row, col, true);
            goRightDownDiagonalAndCheck(row, col, true);
            return;
        }
        if (goti == 'p') {
            goLeftDownDiagonalAndCheck(row, col, true);
            goRightDownDiagonalAndCheck(row, col, true);
            return;
        }
        if (goti == 'P') {
            goLeftUpDiagonalAndCheck(row, col, true);
            goRightUpDiagonalAndCheck(row, col, true);
            return;
        }
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (sc.hasNext()) {
            String fen = sc.nextLine();
            String[] chessDesc = fen.split("/");

            for (int r = 0; r < chessDesc.length; ++r) {
                String row = chessDesc[r];
                int columnToPutVal = 0;
                for (int i = 0 ; i < row.length(); ++i) {
                    char val = row.charAt(i);

                    if (val > '0' && val <= '8') {
                        int emptyCols = val - '0';
                        while (emptyCols -- > 0) chessBoard[r][columnToPutVal++] = '0';
                    } else {
                        chessBoard[r][columnToPutVal++] = val;
                    }
                }
            }

            for (int r = 0; r < 8; ++r) {
                for (int c = 0; c < 8; ++c) {
                    char val = chessBoard[r][c];
                    if (val != '0' && val != '1') {
                        makeMove(val, r, c);
                    }
                }
            }


            int notAttacked = 0;
            for (int r = 0; r < 8; ++r) {
                for (int c = 0; c < 8; ++c) {
                    if (chessBoard[r][c] == '0') notAttacked++;
                }
            }

            System.out.println(notAttacked);
        }
    }
}
