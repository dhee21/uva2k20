package com.company;

import java.util.Scanner;

public class Main {

    public static int findValueOfTwoRollsStartingAt(String[] values, int index) {
        char valueOfFirstRoll = values[index].charAt(0);
        char valueOfSecondRoll = values[index + 1].charAt(0);
        if (valueOfSecondRoll == '/') return 10;

        int valueToReturn = 0;
        if (valueOfFirstRoll == 'X') valueToReturn += 10;
        else valueToReturn += valueOfFirstRoll - '0';

        if (valueOfSecondRoll == 'X') valueToReturn += 10;
        else valueToReturn += valueOfSecondRoll - '0';

        return valueToReturn;
    }

    public static int findValueOfOneRollStartingAt(String[] values, int index) {
        int valueToReturn = 0;

        char valueOfFirstRoll = values[index].charAt(0);
        if (valueOfFirstRoll == 'X') valueToReturn += 10;
        else valueToReturn += valueOfFirstRoll - '0';

        return valueToReturn;
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (true) {
            String input = sc.nextLine();
            if (input.equals("Game Over")) break;

            String values[] = input.split(" ");

            int frameCalculated = 0;
            int frameIndex = 0, score = 0;

            while (frameCalculated < 10) {
                int frameVal = 0;
                if (values[frameIndex].charAt(0) == 'X') {
                    frameVal += 10;
                    frameVal += findValueOfTwoRollsStartingAt(values, frameIndex + 1);
                    frameIndex ++;
                } else {
                    char valueOfFirstRoll = values[frameIndex].charAt(0);
                    char valueOfSecondRoll = values[frameIndex + 1].charAt(0);
                    if (valueOfSecondRoll == '/') {
                        frameVal += 10;
                        frameVal += findValueOfOneRollStartingAt(values,frameIndex + 2);
                    } else {
                        frameVal += (valueOfFirstRoll - '0') + (valueOfSecondRoll - '0');
                    }
                    frameIndex += 2;
                }
                score += frameVal;
                frameCalculated++;
            }

            System.out.println(score);
        }
    }
}
