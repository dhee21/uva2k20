package com.company;

import java.util.Scanner;

public class Main {
    public static int [][] shuffles = new int[110][55];

    public static int[] deck = new int[55];
    public static int toCopy[] = new int[55];


    public static void initDeck () {
        for (int i = 1 ; i <= 52 ; ++i) deck[i] = i;
    }

    public static void performShuffle (int shuffleNum) {
        for (int i = 1 ; i <= 52; ++i) {
            int indexThatComeOnI = shuffles[shuffleNum][i];
            toCopy[i] = deck[indexThatComeOnI];
        }
        for (int i = 1 ; i <= 52; ++i) {
            deck[i] = toCopy[i];
        }
    }

    public static String nameOfCard (int num) {
        int cardNum = deck[num];
        String [] suits = {"Clubs", "Diamonds", "Hearts", "Spades"};
        String [] values = {"2", "3", "4", "5", "6",
                "7", "8", "9", "10", "Jack", "Queen", "King", "Ace"};
        int suitIndex = (cardNum - 1) / 13;
        int cardIndex = (cardNum - 1) % 13;
        return values[cardIndex] + " of " + suits[suitIndex];
    }

    public static void main(String[] args) {
	// write your code here
        int tc;
        Scanner sc = new Scanner(System.in);

        tc = sc.nextInt();
        sc.nextLine();
        sc.nextLine();

        while (tc -- > 0) {
            initDeck();
            int numOfShuffles = Integer.parseInt(sc.nextLine());
            for (int i = 1 ; i <= numOfShuffles; ++i) {
                int cardNum = 0;
                while (true) {
                    String values = sc.nextLine();
                    String ints[] = values.split(" ");
                    for (int k = 0 ; k < ints.length; ++k) {
                        shuffles[i][++cardNum] = Integer.parseInt(ints[k]);
                    }
                    if (cardNum == 52) break;
                }
            }

            while (true) {
                if (!sc.hasNext()) break;
                String val = sc.nextLine();
                if (val.length() == 0) break;
                int shuffleNum = Integer.parseInt(val);
                performShuffle(shuffleNum);
            }

            for (int i = 1; i <= 52; ++i) {
                System.out.println(nameOfCard(i));
            }

            if (tc > 0) System.out.println();
        }

    }
}
