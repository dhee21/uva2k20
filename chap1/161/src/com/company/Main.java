package com.company;

import java.util.Scanner;

public class Main {

    public static int lastSecond = 3600 * 5;

    public static boolean markInArray (int[] arr, int st, int end) {
        if(st >= arr.length) return false;
        arr[st]++;

        int toDec = end + 1;

        if (toDec >= arr.length) {
            return false;
        }

        arr[toDec]--;
        return true;
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (true) {
            boolean gameOver = false;
            int[] timePeriods = new int[110];
            int numOfLights = 0;
            while (true) {
                String[] line = sc.nextLine().split(" ");
                if (line[0].equals("0") && line[1].equals("0") && line[2].equals("0")) {
                    gameOver = true;
                    break;
                }
                for (int i = 0 ; i < line.length; ++i) {
                    timePeriods[numOfLights++] = Integer.parseInt(line[i]);
                }
                if (timePeriods[numOfLights - 1] == 0) {
                    numOfLights--;
                    break;
                }
            }

            if (gameOver) break;

            int [] seconds = new int[lastSecond + 10];

            for (int i = 0 ; i < numOfLights; ++i) {
                int timePeriodVal = timePeriods[i];
                int startingTime = 0;
                boolean shouldGoAhead = markInArray(seconds, startingTime, timePeriodVal - 6);
                while (shouldGoAhead) {
                    startingTime += (2 * timePeriodVal);
                    shouldGoAhead = markInArray(seconds, startingTime, startingTime + (timePeriodVal - 6));
                }
            }


            int value = 0;
            for (int i = 0; i <= lastSecond; ++i) {
                value += seconds[i];
                seconds[i] = value;
            }


            int secondVal = 0;
            for (secondVal = 0; secondVal <= lastSecond; ++secondVal) {
                if (seconds[secondVal] < numOfLights) break;
            }
            if (secondVal == lastSecond + 1) {
                System.out.println("Signals fail to synchronise in 5 hours");
                continue;
            }
            int startTimeToCheckFrom = secondVal + 1;

            for (secondVal = startTimeToCheckFrom; secondVal <= lastSecond; ++secondVal) {
                if (seconds[secondVal] == numOfLights) break;
            }

            if (secondVal == lastSecond + 1) {
                System.out.println("Signals fail to synchronise in 5 hours");
                continue;
            } else  {
                int hr = secondVal / 3600;
                int timeLeft = secondVal - (hr * 3600);
                int min = timeLeft / 60;
                int secondsLeft = timeLeft - (min * 60);

                System.out.printf("%02d:%02d:%02d\n", hr, min, secondsLeft);
            }

        }
    }
}
