package com.company;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public class Main {
    public static ArrayList<Integer> primes = new ArrayList<Integer>();

    public static void calPrimes () {
        int num = 16900;
        int toGo = 130;
        int[] array = new int[num + 10];
        primes.add(2);
        for (int i = 2; i <= toGo; ++i) {
            if (array[i] == 0 && (i % 2 == 1)) {
                primes.add(i);
                int index = i + i;
                while (index <= num) {
                    array[index] = 1;
                    index += i;
                }
            }
        }
        for (int val = toGo + 1 ; val < num; ++val) {
            if (array[val] == 0) primes.add(val);
        }


    }

    public static void main(String[] args) {
	// write your code here
        calPrimes();

        Scanner sc = new Scanner(System.in);
        int[] ans = new int[4000];

        while (true) {
            int n = sc.nextInt();
            if(n == 0) break;
            if (ans[n] != 0) {
                System.out.println(ans[n]);
                continue;
            }
            LinkedList<Integer> list = new LinkedList<>();

            for (int i = 0 ; i < n; ++i) {
                list.add(i + 1);
            }

            int primeIndex = 0;
            Iterator<Integer> it = list.iterator();

            while (list.size() > 1) {
                int hops = primes.get(primeIndex);
                while (hops -- > 0) {
                    if (it.hasNext()) it.next();
                    else {
                        it = list.iterator();
                        it.next();
                    }
                }
                it.remove();
                primeIndex++;
            }
            ans[n] = list.get(0);
            System.out.println(list.get(0));
        }
    }
}
