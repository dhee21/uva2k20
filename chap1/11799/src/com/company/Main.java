package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = sc.nextInt();
        sc.nextLine();
        int caseNo = 0;
        while (tc -- > 0) {
            caseNo++;
            int people = sc.nextInt();
            int max = -1;
            for (int i = 0 ; i < people; ++i) {
                int speed = sc.nextInt();
                max = Math.max(speed, max);
            }
            System.out.println("Case " + caseNo + ": "+ max);
        }
    }
}
