package com.company;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = sc.nextInt();
        int caseVal = 0;
        while (tc -- > 0) {
            if (caseVal++ > 0) System.out.println();
            PriorityQueue<Integer> blue = new PriorityQueue<>();
            PriorityQueue<Integer> green = new PriorityQueue<>();
            int b, sg, sb;
            b = sc.nextInt();
            sg = sc.nextInt();
            sb = sc.nextInt();

            while (sg-- > 0) green.add(-sc.nextInt());
            while (sb-- > 0) blue.add(-sc.nextInt());

            Queue<Integer> greenLeft = new LinkedList<>();
            Queue<Integer> blueLeft = new LinkedList<>();
            while (blue.size() > 0 && green.size() > 0) {

                for (int field = 0; field < b && blue.size() > 0 && green.size() > 0; ++field) {
                    int greenSoldier = -green.poll();
                    int blueSoldier = -blue.poll();
                    //System.out.println(greenSoldier + " " + blueSoldier);
                    if (greenSoldier > blueSoldier) greenLeft.add(greenSoldier - blueSoldier);
                    else if (blueSoldier > greenSoldier) blueLeft.add(blueSoldier - greenSoldier);
                }
                while (greenLeft.size() > 0) green.add(-greenLeft.poll());
                while (blueLeft.size() > 0) blue.add(-blueLeft.poll());
            }

            if (green.size() == 0 && blue.size() == 0) {
                System.out.println("green and blue died");
            } else if (blue.size() > 0) {
                System.out.println("blue wins");
                while (blue.size() > 0) {
                    System.out.println(-blue.poll());
                }
            } else {
                System.out.println("green wins");
                while (green.size() > 0) {
                    System.out.println(-green.poll());
                }
            }

        }
    }
}
