package com.company;

import java.text.DecimalFormat;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        while (true) {
            String time = sc.nextLine();
            String[] timeArr = time.split(":");
            double hr = Double.parseDouble(timeArr[0]);
            double min = Double.parseDouble(timeArr[1]);

            if (hr == 0 && min == 0) break;

            double totalMin = 60 * hr + min;

            double hrHand = totalMin / 2.0;
            double minHand = 6 * min;

            double diff = Math.abs(hrHand - minHand);

            if (diff > 180) {
                diff = 360 - diff;
            }

//            DecimalFormat df = new DecimalFormat("00.000");
//            double ans = Double.valueOf(df.format(diff));
            System.out.printf("%.3f\n", diff);
        }
    }
}
