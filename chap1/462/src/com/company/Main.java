package com.company;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static class Suit {
        public char suitOf;
        public boolean isStopped = false;
        HashMap<Character, Integer> count = new HashMap<>();

        public Suit (char val) {
            this.suitOf = val;
            count.put('K', 0);
            count.put('Q', 0);
            count.put('A', 0);
            count.put('J', 0);
            count.put('T', 0);
        }

        public int otherCardsFor(char val) {
            return count.get('T') - count.get(val) ;
        }
        public void addOneCard (char card) {
            int val = count.getOrDefault(card, 0);
            int total = count.get('T');
            count.put('T', total + 1);
            count.put(card, val + 1);
        }
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);

        while (sc.hasNext()) {
            ArrayList<Suit> suits = new ArrayList<>();
            Map<Character, Suit> charToSuit = new HashMap<>();

            Suit spade = new Suit('S');
            suits.add(spade);
            charToSuit.put('S', spade);

            Suit heart = new Suit('H');
            suits.add(heart);
            charToSuit.put('H', heart);

            Suit dia = new Suit('D');
            suits.add(dia);
            charToSuit.put('D', dia);

            Suit club = new Suit('C');
            suits.add(club);
            charToSuit.put('C', club);


            String cardsString = sc.nextLine();
            String[] cards = cardsString.split(" ");

            int points = 0, pointWithoutRules = 0;
            for (int i = 0 ; i < cards.length; ++i) {
                String oneCard = cards[i];
                char card, suit;
                card = oneCard.charAt(0);
                suit = oneCard.charAt(1);
                if (card == 'T') card = 't';
                charToSuit.get(suit).addOneCard(card);
                int pointtoAdd = card == 'A' ? 4:
                card == 'K' ? 3 : card == 'Q' ? 2 : card == 'J' ? 1 : 0;
                points += pointtoAdd;
                pointWithoutRules += pointtoAdd;
            }
            char suitWithMostCards = '0';
            int mostCardsOfaSuit = 0;
            boolean areAllSuitsStopped = true;
            for (Suit suit : suits) {
                int rule2 = 0, rule3 = 0, rule4 = 0, rule5 = 0, rule6 = 0, rule7 = 0;
                if (suit.count.get('K') > 0 && suit.otherCardsFor('K') == 0) {
                    rule2--;
                }
                if (suit.count.get('Q') > 0 && suit.otherCardsFor('Q') <= 1) {
                    rule3--;
                }
                if (suit.count.get('J') > 0 && suit.otherCardsFor('J') <= 2) {
                    rule4--;
                }
                if (suit.count.get('T') == 2) rule5++;
                if (suit.count.get('T') == 1) rule6 += 2;
                if (suit.count.get('T') == 0) rule7 += 2;

                boolean hasAce = suit.count.get('A') > 0;
                boolean king = suit.count.get('K') > 0 && suit.otherCardsFor('K') >= 1;
                boolean queen = suit.count.get('Q') > 0 && suit.otherCardsFor('Q') >= 2;
                if ( hasAce || king || queen) suit.isStopped = true;

                pointWithoutRules += rule2 + rule3 + rule4;
                points += rule2 + rule3 + rule4 + rule5 + rule6 + rule7;

                int totalCardInSuit = suit.count.get('T');
                if (totalCardInSuit > mostCardsOfaSuit) {
                    suitWithMostCards = suit.suitOf;
                    mostCardsOfaSuit = totalCardInSuit;
                }
                if (!suit.isStopped) areAllSuitsStopped = false;
            }


            if (points < 14) {
                System.out.println("PASS");
            } else {
                if (pointWithoutRules >= 16 && areAllSuitsStopped) {
                    System.out.println("BID NO-TRUMP");
                } else {
                    System.out.println("BID " + suitWithMostCards);
                }
            }
        }
    }
}
