package com.company;

import java.util.Scanner;

public class Main {

    public static String [] deck = new String[55];

    public static int valueOf(String card) {
        return card.charAt(0) >= '2' && card.charAt(0) <= '9' ? card.charAt(0) - '0' : 10;
    }

    public static void main(String[] args) {
	// write your code here
        Scanner sc = new Scanner(System.in);
        int tc = sc.nextInt(); sc.nextLine();
        for (int caseVal = 1; caseVal <= tc; ++caseVal) {
            int totalCards = 0, deckIndex = 0;

            while (true) {
                String cards = sc.nextLine();
                String[] cardsArray = cards.split(" ");
                for (String cardVal : cardsArray) {
                    deck[deckIndex++] = cardVal;
                }
                totalCards += cardsArray.length;
                if (totalCards == 52) {
                    break;
                }
            }

            int times = 3, y = 0;
            int cardToPickUp = 26;
            while (times-- > 0) {
                int x = valueOf(deck[cardToPickUp]);
                y += x;
                int cardToSkip = 10 - x;
                cardToPickUp -= (cardToSkip + 1);
            }
            int indexToFind = y - 1;
            if (indexToFind <= cardToPickUp) {
                System.out.println("Case " + caseVal + ": " + deck[indexToFind]);
            } else {
                int diff = indexToFind - cardToPickUp;
                String cardAns  = deck[26 + diff];
                System.out.println("Case " + caseVal + ": " + cardAns);
            }

        }


    }


}
