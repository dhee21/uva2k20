package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        // write your code here
        Scanner sc = new Scanner(System.in);
        while (true) {
            String[] input = sc.nextLine().split(" ");
            int duration = Integer.parseInt(input[0]);
            double downPayment = Double.parseDouble(input[1]);
            double loan = Double.parseDouble(input[2]);
            int depRecordsNum = Integer.parseInt(input[3]);
            if (duration < 0) break;

            Double[] percentages = new Double[110];
            boolean prev = false;
            double prevPercentage = 0;
            int prevMonth = 0;
            for (int i = 0 ; i < depRecordsNum ; ++i) {
                String[] depreciationData = sc.nextLine().split(" ");
                int monthNum = Integer.parseInt(depreciationData[0]);
                double percentageVal = Double.parseDouble(depreciationData[1]);

                if (prev) for (int m = prevMonth + 1 ; m < monthNum; ++m) percentages[m] = prevPercentage;

                percentages[monthNum] = percentageVal;
                prevPercentage = percentageVal;
                prev = true;
                prevMonth = monthNum;
            }
            for (int m = prevMonth + 1 ; m <= duration; ++m) percentages[m] = prevPercentage;

            double carVal = downPayment + loan;
            carVal -= percentages[0] * carVal;
            double perMonthPayment = loan / (double) duration;
            if (carVal > loan) {
                System.out.println("0 months"); continue;
            }
            for (int month = 1; month <= duration; ++month) {
                loan -= perMonthPayment;
                carVal -= percentages[month] * carVal;
                if (carVal > loan) {
                    System.out.println(month + (month == 1 ? " month" : " months")); break;
                }
            }

        }


    }
}
